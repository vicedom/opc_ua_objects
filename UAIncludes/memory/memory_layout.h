/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef MEMORY_LAYOUT_H_AMBXGVNY
#define MEMORY_LAYOUT_H_AMBXGVNY

#include <stdlib.h> /* size_t */
#include <stdbool.h>
#include <platform/platform.h>
#include <memory/memory_config.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup memory_layout memory_layout
 * @ingroup memory
 *
 * @brief Functions for managing the memory layout.
 * The UA SDK makes use of various memory pools, which need to be setup properly.
 * For this one big block of memory is allocated at startup and is then
 * devided into several memory pools.
 * - Single-Task-Mode: The memory can simply by allocated using the platform
 * function ua_malloc(), which typically is implemented using C malloc().
 * - Multi-Task-Mode: The memory is allocated in a shared memory area so that
 * it can be used from all processes. It is necessary that the SHM is mapped to
 * the same virtual base address in all processes to make IPC working.
 * @{
 */

/** Configures the size of one memory pool.
 * For the heap-pool (dynamic size) \c size = 1.
 * All other pools as object pools which manages a number of fixed sized
 * objects, here \c size = sizeof(object_type).
 */
struct memory_pool_config
{
    size_t count; /**< number of elements. set to zero to disable this pool */
    size_t size;  /**< size of one element in bytes */
    int id;       /**< unique pool id */
    bool shared;  /**< true=shared memory (for IPC), false=always private */
    const char *name;
};

/** Memory layout structure.
 * Configures the layout of the memory pools.
 */
struct memory_layout
{
    void *baseaddr; /**< base address of SHM, not used without SHM. */
    size_t size;    /**< size of complete memory area. */
    unsigned int num_pools; /**< number of memory pools */
    struct memory_pool_config *poolconfig; /**< pool configurations */
};

#define MEMORY_INVALID_INDEX (size_t)-1

unsigned int   memory_uniqueid(void);
unsigned int ipc_counter(void);
int   memory_layout_init(struct memory_layout *config);
void memory_layout_clear(struct memory_layout *config);
void  memory_layout_set(void *mem);
void *memory_layout_get(void);
void  memory_layout_set_prop(unsigned int index, void *data);
void *memory_layout_get_prop(unsigned int index);
void  memory_layout_set_prop_uint(unsigned int index, unsigned int val);
unsigned int memory_layout_get_prop_uint(unsigned int index);
void  memory_layout_set_prop_int(unsigned int index, int val);
int memory_layout_get_prop_int(unsigned int index);
struct mem_objectpool *memory_get_pool(unsigned int pool_id);
void *memory_pool_alloc(unsigned int pool_id);
void *memory_pool_calloc(unsigned int pool_id);
void  memory_pool_free(unsigned int pool_id, void *ptr);
void *memory_pool_get(unsigned int pool_id, int index);
size_t memory_pool_indexof(unsigned int pool_id, const void *ptr);

#ifdef MEMORY_ENABLE_TRACE
void *memory_malloc(size_t size, const char *file, int line);
void  memory_free(void *ptr, const char *file, int line);
void *memory_calloc(size_t nmemb, size_t size, const char *file, int line);
void *memory_realloc(void *ptr, size_t size, const char *file, int line);
/* use the macros from memory.h */
#else /* MEMORY_ENABLE_TRACE */
void *memory_malloc(size_t size);
void  memory_free(void *ptr);
void *memory_calloc(size_t nmemb, size_t size);
void *memory_realloc(void *ptr, size_t size);
#endif /* MEMORY_ENABLE_TRACE */

void memory_print_status(void);

/** @} */

UA_END_EXTERN_C

#endif /* end of include guard: MEMORY_LAYOUT_H_AMBXGVNY */

