/******************************************************************************
** pubsubserverapplicationcallback.h
**
** Copyright (c) 2006-2015 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.5
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.5, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.5/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBSERVERAPPLICATIONCALLBACK_H__
#define __PUBSUBSERVERAPPLICATIONCALLBACK_H__

#include "pubsubmodule.h"
#include "writergroup.h"
#include "datasetwriter.h"
#include "datasetreader.h"

/** Callback class for the PubSub module in the server SDK.
*/
class SERVER_PUBSUBMODULE_EXPORT PubSubServerApplicationCallback
{
    UA_DISABLE_COPY(PubSubServerApplicationCallback);
public:
    PubSubServerApplicationCallback() {}
    virtual ~PubSubServerApplicationCallback() {}

    /** Callback informing the application about the start up of the PubSub module in the server SDK.
    */
    virtual void startUpPubSub() = 0;

    /** Optional callback informing the application about the start of the shut down of the PubSub module in the server SDK.
    */
    virtual void beforeShutDownPubSub() {}

    /** Callback informing the application about the shut down of the PubSub module in the server SDK.
    *  The application is not allowed to call any methods on the PubSubModule if this method call is returned.
    */
    virtual void shutDownPubSub() = 0;

    /** Callback to inform application about creation of new PubSubConnection
    *
    *  The application can provide an own network backend by setting the PubSubNetworkBackendInterface
    *  on the PubSubConnection object.
    */
    virtual void newConnection(
        PubSubBase::PubSubConnection* pConnection //!< [in] The connection management object.
    ) = 0;

    /** Callback to inform application about creation of new writer group
    *
    *  The application can handle the processing of the group by setting the
    *  groupHandledByApplication to true.
    */
    virtual void newWriterGroup(
        PubSubBase::WriterGroup* pWriterGroup, //!< [in] The writer group management object.
        bool&       groupHandledByApplication /**< [out] Flag used to indicate that the application is responsible for group
                                              processing (true) or handling should be done by SDK (false).
                                              The PubSubWriterGroup provides methods for sampling (creating the NetworkMessage)
                                              and publishing that can be called by the application for the timing.*/
    ) = 0;

    /** Callback to inform application about creation of new DataSetWriter
    *
    *  The application can handle the processing of the DataSetMessage encoding by setting the messageHandledByApplication to true.
    *  In this case the application must set the DataSetWriterCallback to receive the message encoding callbacks.
    */
    virtual void newDataSetWriter(
        PubSubBase::DataSetWriter* pDataSetWriter, //!< [in] The DataSetWriter management object.
        bool&              messageEncodedByApplication /**< [out] Flag used to indicate that the application is responsible for encoding the
                                                       the fields of the DataSetMessage (true) or encoding should be done by SDK (false).
                                                       In the case of true, the encoding is started with writeDataSetMessageFields. */
    ) = 0;

    /** Callback to inform application about creation of new reader group
    */
    virtual void newReaderGroup(
        PubSubBase::ReaderGroup* pReaderGroup //!< [in] The reader group management object.
    );

    /** Callback to inform application about creation of new DataSetReader
    *
    *  The application can handle the processing of the DataSetMessage decoding by setting the messageHandledByApplication to true.
    *  In this case the application must set the DataSetReaderCallback to receive the message encoding callbacks.
    */
    virtual void newDataSetReader(
        PubSubBase::DataSetReader* pDataSetReader, //!< [in] The DataSetReader management object.
        bool&              messageDecodedByApplication /**< [out] Flag used to indicate that the application is responsible for decoding the
                                                       the fields of the DataSetMessage (true) or decoding should be done by SDK (false).
                                                       In the case of true, the decoding is started with newMessageReceived. */
    ) = 0;

    /** Callback to inform application about a state change of a PubSub object.
    */
    virtual void pubSubObjectStateChange(
        PubSubBase::PubSubObject* pPubSubObject, //!< [in] The affected PubSub object
        PubSubBase::PubSubObject::PubSubObjectType pubSubObjectType, //!< [in] The type of the affected PubSub object
        OpcUa_PubSubState newState  //!< [in] The new state of the affected PubSub object
    );

    /** Callback to inform application about the deletion of a PubSub object.
    */
    virtual void pubSubObjectRemoved(
        PubSubBase::PubSubObject* pPubSubObject, //!< [in] The affected PubSub object
        PubSubBase::PubSubObject::PubSubObjectType pubSubObjectType //!< [in] The type of the affected PubSub object
    ) = 0;
};


#endif // #ifndef __PUBSUBSERVERAPPLICATIONCALLBACK_H__

