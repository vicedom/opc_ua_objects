/******************************************************************************
** pubsubmanagerprivate.h
**
** Copyright (c) 2006-2015 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.5
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.5, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.5/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBMANAGERPRIVATE_H__
#define __PUBSUBMANAGERPRIVATE_H__

#include "pubsubmodule.h"
#include "writergroup.h"
#include "datasetwriter.h"
#include "datasetreader.h"
#include "pubsubcontrolcallback.h"
#include "pubsubconfiguration.h"
#include "opcua_publishsubscribetype.h"
#include "datasetcollector.h"
#include "datasetdispatcher.h"
#include "pubsubserverapplicationcallback.h"
#include "uasemaphore.h"
#include "nodemanagerbase.h"
using namespace PubSubServer;

class SERVER_PUBSUBMODULE_EXPORT PubSubVariableUserData : public UserDataBase
{
    UA_DISABLE_COPY(PubSubVariableUserData);
    PubSubVariableUserData() {}
public:
    PubSubVariableUserData(UaMutexRefCounted* pSharedMutex);
    virtual ~PubSubVariableUserData();

    void setPubSubObject(PubSubBase::PubSubObject* pPubSubObject);
    inline PubSubBase::PubSubObject* pPubSubObject() const { return m_pPubSubObject; }
    OpcUa_PubSubState getState();
    void getStateValue(UaDataValue& stateDataValue);
    void setBaseState(OpcUa_PubSubState baseState);

    bool isValid();
    virtual void setInvalid();

private:
    PubSubBase::PubSubObject* m_pPubSubObject;
    OpcUa_PubSubState         m_baseState;
    UaMutexRefCounted*        m_pSharedMutex;
    bool                      m_isValid;
};

class SERVER_PUBSUBMODULE_EXPORT NodeManagerPubSubConfig :
    public NodeManagerBase
{
    UA_DISABLE_COPY(NodeManagerPubSubConfig);
public:
    NodeManagerPubSubConfig();
    virtual ~NodeManagerPubSubConfig();

    // NodeManagerUaNode implementation
    virtual UaStatus   afterStartUp() { return OpcUa_Good; }
    virtual UaStatus   beforeShutDown() { return OpcUa_Good; }

    // IOManagerUaNode implementation
    virtual UaStatus readValues(const UaVariableArray &arrUaVariables, UaDataValueArray &arrDataValues);
    virtual UaStatus writeValues(const UaVariableArray &arrUaVariables, const PDataValueArray &arrpDataValues, UaStatusCodeArray &arrStatusCodes);
};

class SERVER_PUBSUBMODULE_EXPORT PubSubManagerPrivate :
    public PubSubBase::PubSubControlCallback,
    public DataSetDispatcherCallback,
    public DataSetCollectorCallback,
    public UaThread
{
    friend class PubSubManager;
    UA_DISABLE_COPY(PubSubManagerPrivate);

public:
    static void initPublishedDataSet(
        const UaString& Name,
        const UaPublishedVariableDataTypes& variablesToAdd,
        const UaUInt16Array&  fieldFlags,
        const UaStringArray& fieldNames,
        UaDataSetMetaDataType& dataSetMetaData,
        UaStatusCodeArray& AddResults);

    static void reloadConfig();
    UaStatus reloadConfigurationFormFile();

    void setFileWriteActive(bool isFileWriteActive);

private:
    PubSubManagerPrivate();
    virtual ~PubSubManagerPrivate();

    UaStatus startUpPubSubStack();
    UaStatus shutDownPubSubStack(bool saveConfiguration = true);

    // Interface PubSubControlCallback
    virtual void newConnection(PubSubBase::PubSubConnection* pConnection);
    virtual void newWriterGroup(PubSubBase::WriterGroup* pWriterGroup, bool& timingHandledByApplication);
    virtual void newDataSetWriter(PubSubBase::DataSetWriter* pDataSetWriter, bool&  messageEncodedByApplication);
    virtual void newReaderGroup(PubSubBase::ReaderGroup* pReaderGroup);
    virtual void newDataSetReader(PubSubBase::DataSetReader* pDataSetReader, bool& messageDecodedByApplication);
    virtual void pubSubObjectStateChange(
        PubSubBase::PubSubObject* pPubSubObject, PubSubBase::PubSubObject::PubSubObjectType pubSubObjectType, OpcUa_PubSubState newState);
    virtual void pubSubObjectRemoved(
        PubSubBase::PubSubObject* pPubSubObject, PubSubBase::PubSubObject::PubSubObjectType pubSubObjectType);
    // Interface PubSubControlCallback

    // Interface DataSetCollectorCallback
    virtual void retryMonitoring(DataSetCollector* pDataSetCollector);
    // Interface DataSetCollectorCallback

    // Interface DataSetDispatcherCallback
    virtual void newData(DataSetDispatcher* pDataSetDispatcher);
    // Interface DataSetDispatcherCallback

    // Interface UaThread
    virtual void run();
    // Interface UaThread

private:
    static PubSubManagerPrivate* s_pInstance;

    UaMutex        m_mutex;
    PubSubBase::PubSubConfiguration* m_pPubSubConfig;
    NodeManagerPubSubConfig* m_pNodeManagerPubSubConfig;
    NodeAccessInfo* m_pDefaultPermissions;

    bool           m_isStopped;
    ServerManager* m_pServerManager;
    UaString       m_sConfigFileBin;
    bool           m_isFileWriteActive;
    OpcUa::PublishSubscribeType* m_pPublishSubscribeObject;

    PubSubServerApplicationCallback* m_pPubSubServerApplicationCallback;
    std::list<DataSetCollector*> m_dataSetCollectors;
    std::list<DataSetDispatcher*> m_dataSetDispatchers;

    UaSemaphore m_semSignal;
    bool m_stopThread;
    bool m_jobActive;
    std::list<DataSetDispatcher*> m_writeJobs;
    std::map<DataSetDispatcher*, DataSetDispatcher*> m_writeJobsFilter;
    std::list<DataSetCollector*> m_retryJobs;
};

#endif // #ifndef __PUBSUBMANAGERPRIVATE_H__

