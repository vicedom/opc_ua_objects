/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
#ifndef UAPUBSUB_SAMPLINGQUEUE_H
#define UAPUBSUB_SAMPLINGQUEUE_H

#include <platform/atomic.h>
#include <uabase/datavalue.h>

UA_BEGIN_EXTERN_C

enum pubsub_element_state {
    ELEMENT_AQUIRED = -1,
    ELEMENT_RELEASED = 0,
    ELEMENT_UPDATED = 1,
};

struct pubsub_datavalue_element {
    struct ua_datavalue value;
    int state;
};

struct pubsub_datavalue_samplingitem {
    unsigned int writeIndex;
    unsigned int readIndex;
    unsigned int handle;
    struct pubsub_datavalue_element elem[2];
    struct ua_datavalue cache;
};

int pubsub_datavalue_samplingitem_init(struct pubsub_datavalue_samplingitem *item);

struct ua_datavalue *pubsub_samplingitem_read(struct pubsub_datavalue_samplingitem *item);

void pubsub_samplingitem_write(struct pubsub_datavalue_samplingitem *item, struct ua_datavalue *val);

struct ua_datavalue *pubsub_samplingitem_beginWrite(struct pubsub_datavalue_samplingitem *item);

void pubsub_samplingitem_endWrite(struct pubsub_datavalue_samplingitem *item);

UA_END_EXTERN_C

#endif // UAPUBSUB_SAMPLINGQUEUE_H
