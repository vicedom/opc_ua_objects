/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UAPUBSUB_FILTER_H
#define UAPUBSUB_FILTER_H

#include <uapubsub_types.h>

#include <platform/platform.h>

#include <stdbool.h>
#include <stdint.h>

UA_BEGIN_EXTERN_C

/* size in bits of the type pubsub_filterset */
#define PUBSUB_FILTERSET_SIZE 64

/* bitfield to indicate matching filters; each bit applies to one registered filter */
typedef uint64_t pubsub_filterset;

struct pubsub_filter {
    uint8_t *bytes;         ///< bytestring to compare
    uint8_t *mask;          ///< mask to apply before compare
    unsigned int size;      ///< length of bytes and mask in bytes
    unsigned int filter_id; ///< id (bitindex) for this filter in its associated filterset
    pubsub_handle reader;   ///< reader object for this filter
};
#define pubsub_filterset_init_from(pfset, orig) (pfset = orig)

static inline void pubsub_filterset_init(pubsub_filterset *pfset)
{
    *pfset = 0;
    return;
}

static inline bool pubsub_filterset_isnull(pubsub_filterset pfset) { return (pfset == 0) ? true : false; }

static inline void pubsub_filterset_add_filter(pubsub_filterset *pfset, struct pubsub_filter *filter)
{
    uint64_t bit = 1;
    if (!filter || !pfset) return;
    UA_ASSERT(filter->filter_id < PUBSUB_FILTERSET_SIZE);
    *pfset |= (bit << filter->filter_id);
}

static inline int pubsub_filterset_ctz(pubsub_filterset pfset)
{
    int count = 0;
    if (pfset == 0) return 64;
    if ((pfset & 0xFFFFFFFF) == 0) {
        count += 32;
        pfset >>= 32;
    }
    if ((pfset & 0x0000FFFF) == 0) {
        count += 16;
        pfset >>= 16;
    }
    if ((pfset & 0x000000FF) == 0) {
        count += 8;
        pfset >>= 8;
    }
    if ((pfset & 0x0000000F) == 0) {
        count += 4;
        pfset >>= 4;
    }
    if ((pfset & 0x00000003) == 0) {
        count += 2;
        pfset >>= 2;
    }
    if ((pfset & 0x00000001) == 0) { count += 1; }
    return count;
}

static inline int pubsub_filterset_count(pubsub_filterset pfset)
{
    int count;
    for (count = 0; pfset != 0; count++) { pfset &= pfset - 1; }
    return count;
}

UA_END_EXTERN_C

#endif // UAPUBSUB_FILTER_H
