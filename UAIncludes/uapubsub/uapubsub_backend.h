/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
#ifndef UAPUBSUB_BACKEND_H
#define UAPUBSUB_BACKEND_H

#include <uapubsub_filter.h>
#include <uapubsub_types.h>

#include <stdbool.h>

UA_BEGIN_EXTERN_C

/* forward declarations */
struct pubsub_backend_context;

/* function pointer definitions */
typedef int (*pubsub_backend_init_fn)(struct pubsub_backend_context *);
typedef int (*pubsub_backend_clear_fn)(struct pubsub_backend_context *);
typedef int (*pubsub_backend_alloc_fn)(struct pubsub_backend_context *);
typedef int (*pubsub_backend_free_fn)(struct pubsub_backend_context *);
typedef int (*pubsub_backend_send_fn)(struct pubsub_backend_context *, unsigned int len);
typedef void (*pubsub_backend_send_complete_cb)(int, struct pubsub_backend_context *);
typedef void (*pubsub_backend_begin_send_fn)(struct pubsub_backend_context *, pubsub_backend_send_complete_cb);
typedef int (*pubsub_backend_recv_fn)(struct pubsub_backend_context *ctx);
typedef void (*pubsub_backend_recv_complete_cb)(int, struct pubsub_backend_context *);
typedef void (*pubsub_backend_begin_recv_fn)(struct pubsub_backend_context *, pubsub_backend_send_complete_cb);

enum pubsub_buffer_mode {
    PUBSUB_BUFFER_MODE_MODULE = 0,  ///< buffers get allocated within pubsub module
    PUBSUB_BUFFER_MODE_BACKEND = 1, ///< buffers get allocated by backend
    PUBSUB_BUFFER_MODE_DYNAMIC = 2, ///< buffers get passed explicitly in send/recv calls
};

enum pubsub_exec_mode {
    PUBSUB_EXEC_MODE_SYNC = 0,  ///< send and receive operations are blocking/polling
    PUBSUB_EXEC_MODE_ASYNC = 1, ///< send and receive operations are fully async
    PUBSUB_EXEC_MODE_MIXED = 2, ///< mixed, e.g send sync and receive async
};

enum pubsub_context_mode {
    PUBSUB_CONTEXT_MODE_LAYER = 0,  ///< pubsub module is called within an already existing context , e.g. a comm task
    PUBSUB_CONTEXT_MODE_MODULE = 1, ///< pubsub module com loop is running in a separate context
    PUBSUB_CONTEXT_MODE_APPLICATION = 2, ///< pubsub module is called from application and running in (multiple) different contexts
};

enum pubsub_backend_class {
    PUBSUB_BACKEND_CLASS_USER = 0, ///< user backend implementation, highest priority
    PUBSUB_BACKEND_CLASS_SDK_ETH,  ///< sdk backend for raw ethernet connections
    PUBSUB_BACKEND_CLASS_SDK_UDP,  ///< sdk backend for udp connections
    PUBSUB_BACKEND_CLASS_SDK_MQTT, ///< sdk backend for mqtt connections
    PUBSUB_BACKEND_CLASS_SDK_AMQP, ///< sdk backend for amqp connections
    PUBSUB_BACKEND_CLASS_FALLBACK, ///< fallback backend, lowest priority
    PUBSUB_BACKEND_NUM_CLASSES,
};
struct pubsub_backend_context {
    void *arg;              ///< pointer for userdata
    unsigned char *data;    ///< data to send / received
    unsigned int len;       ///< length of data
    pubsub_handle peer;     ///< handle to sending / receiving object (writergroup/connection)
    pubsub_handle iface;    ///< handle to associated connection
    pubsub_filterset pfset; ///< filter set to indicate matching filters/readers
    bool new_data;
};

struct pubsub_backend_vt {
    pubsub_backend_init_fn init;              ///< initialize connection/interface/context
    pubsub_backend_clear_fn clear;            ///< clear connection/interface/context
    pubsub_backend_alloc_fn alloc;            ///< allocate buffer for pre-encoded header
    pubsub_backend_free_fn free;              ///< free previously allocated buffer
    pubsub_backend_send_fn send;              ///< synchronous send, may block
    pubsub_backend_begin_send_fn begin_send;  ///< asynchronous send with completion callback
    pubsub_backend_send_complete_cb end_send; ///< completion callback for async send
    pubsub_backend_recv_fn recv;              ///< synchronous recv, may block
    pubsub_backend_begin_recv_fn begin_recv;  ///< asynchronous recv with completion callback
    pubsub_backend_recv_complete_cb end_recv; ///< completion callback for async recv
};

struct pubsub_backend {
    /* Global start function called at pubsub module initialization */
    int (*start)(void);
    /* Global stop function called at pubsub module deinitialization */
    int (*stop)(void);
    /* Global assign function called at module startup / object initialization */
    bool (*assign)(pubsub_handle connection);
    /* vtable of a backend */
    struct pubsub_backend_vt vt;
    /* runtime behaviour of the backend */
    enum pubsub_buffer_mode buffer_mode;
    enum pubsub_exec_mode exec_mode;
    enum pubsub_context_mode context_mode;
    /* registration priority of the backend */
    enum pubsub_backend_class backend_class;
};

UA_END_EXTERN_C
#endif // UAPUBSUB_BACKEND_H
