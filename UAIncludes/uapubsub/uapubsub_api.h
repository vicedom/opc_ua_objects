/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
#ifndef UAPUBSUB_API_H
#define UAPUBSUB_API_H

/* include for extern "C" Makro */
#include <platform/platform.h>

/* include for handle typedef */
#include <uapubsub_types.h>

/* include for callback api function implementations */
#include <uapubsub_callbacks.h>

UA_BEGIN_EXTERN_C

/* TODO: mask this if embeddedstack types should be hidden */
struct ua_uabinaryfiledatatype;

/* forward declarations */
struct pubsub_backend;

/* publicly exposed api functions of the pubsub communication module */
struct pubsub_api {
    struct {
        int (*init)(void);

        int (*cleanup)(void);

        pubsub_handle (*set_config_blob)(unsigned int len, void *data);

        pubsub_handle (*set_config_struct)(const struct ua_uabinaryfiledatatype *conf);

        int (*apply_config)(pubsub_handle conf);

        int (*startup)(pubsub_handle conf);

        int (*shutdown)(pubsub_handle conf);

        int (*do_com)(pubsub_handle conf);

        int (*do_com_cycle)(pubsub_handle conf);

        int (*sample)(pubsub_handle writer_group);

        int (*publish)(pubsub_handle writer_group);

        int (*subscribe)(pubsub_handle reader);

        int (*desample)(pubsub_handle reader);
    } ctrl;

    struct {
        int (*num_connections)(pubsub_handle conf);

        int (*num_datasets)(pubsub_handle conf);

        int (*num_writergroups)(pubsub_handle connection);

        int (*num_writers)(pubsub_handle writer_group);

        int (*num_readergroups)(pubsub_handle connection);

        int (*num_readers)(pubsub_handle reader_group);

        pubsub_handle (*first_connection)(pubsub_handle conf);

        pubsub_handle (*next_connection)(pubsub_handle connection);

        pubsub_handle (*first_dataset)(pubsub_handle conf);

        pubsub_handle (*next_dataset)(pubsub_handle dataset);

        pubsub_handle (*first_writergroup)(pubsub_handle connection);

        pubsub_handle (*next_writergroup)(pubsub_handle writer_group);

        pubsub_handle (*first_writer)(pubsub_handle writer_group);

        pubsub_handle (*next_writer)(pubsub_handle writer);

        pubsub_handle (*first_readergroup)(pubsub_handle connection);

        pubsub_handle (*next_readergroup)(pubsub_handle reader_group);

        pubsub_handle (*first_reader)(pubsub_handle reader_group);

        pubsub_handle (*next_reader)(pubsub_handle reader);

        void (*handle_set_userdata)(pubsub_handle handle, void *userdata);

        void *(*handle_get_userdata)(pubsub_handle handle);
    } cfg;

    struct {
        const struct ua_pubsubconnectiondatatype *(*connection)(pubsub_handle connection);

        const struct ua_publisheddatasetdatatype *(*dataset)(pubsub_handle writer);

        const struct ua_writergroupdatatype *(*writergroup)(pubsub_handle writer_group);

        const struct ua_datasetwriterdatatype *(*writer)(pubsub_handle writer);

        const struct ua_readergroupdatatype *(*readergroup)(pubsub_handle reader_group);

        const struct ua_datasetreaderdatatype *(*reader)(pubsub_handle reader);
    } prm;

    struct {
        int (*enable_dispatcher)(pubsub_dispatch_fn dispatch);
        int (*register_backend)(struct pubsub_backend *backend);
    } dsp;
};

int pubsub_load_api(struct pubsub_api **api);

int pubsub_unload_api(struct pubsub_api *api);

UA_END_EXTERN_C

#endif // UAPUBSUB_API_H
