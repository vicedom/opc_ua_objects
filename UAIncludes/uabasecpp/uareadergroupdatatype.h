/******************************************************************************
** uareadergroupdatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaReaderGroupDataType class.
**
******************************************************************************/
#ifndef UAREADERGROUPDATATYPE_H
#define UAREADERGROUPDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uadatasetreaderdatatype.h"
#include "uaendpointdescription.h"
#include "uaextensionobject.h"
#include "uakeyvaluepair.h"
#include "uastring.h"
#include "uaarraytemplates.h"

class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaReaderGroupDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_ReaderGroupDataType.
 *
 *  This class encapsulates the native OpcUa_ReaderGroupDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaReaderGroupDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared ReaderGroupDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaReaderGroupDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaReaderGroupDataType
{
    UA_DECLARE_PRIVATE(UaReaderGroupDataType)
public:
    UaReaderGroupDataType();
    UaReaderGroupDataType(const UaReaderGroupDataType &other);
    UaReaderGroupDataType(const OpcUa_ReaderGroupDataType &other);
    UaReaderGroupDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        OpcUa_MessageSecurityMode securityMode,
        const UaString& securityGroupId,
        const UaEndpointDescriptions &securityKeyServices,
        OpcUa_UInt32 maxNetworkMessageSize,
        const UaKeyValuePairs &groupProperties,
        UaExtensionObject& transportSettings,
        UaExtensionObject& messageSettings,
        const UaDataSetReaderDataTypes &dataSetReaders
        );
    UaReaderGroupDataType(const UaExtensionObject &extensionObject);
    UaReaderGroupDataType(const OpcUa_ExtensionObject &extensionObject);
    UaReaderGroupDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaReaderGroupDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaReaderGroupDataType();

    void clear();

    bool operator==(const UaReaderGroupDataType &other) const;
    bool operator!=(const UaReaderGroupDataType &other) const;

    UaReaderGroupDataType& operator=(const UaReaderGroupDataType &other);

    OpcUa_ReaderGroupDataType* copy() const;
    void copyTo(OpcUa_ReaderGroupDataType *pDst) const;

    static OpcUa_ReaderGroupDataType* clone(const OpcUa_ReaderGroupDataType& source);
    static void cloneTo(const OpcUa_ReaderGroupDataType& source, OpcUa_ReaderGroupDataType& copy);

    void attach(OpcUa_ReaderGroupDataType *pValue);
    OpcUa_ReaderGroupDataType* detach(OpcUa_ReaderGroupDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setReaderGroupDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setReaderGroupDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setReaderGroupDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setReaderGroupDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setReaderGroupDataType(
        const UaString& name,
        OpcUa_Boolean enabled,
        OpcUa_MessageSecurityMode securityMode,
        const UaString& securityGroupId,
        const UaEndpointDescriptions &securityKeyServices,
        OpcUa_UInt32 maxNetworkMessageSize,
        const UaKeyValuePairs &groupProperties,
        UaExtensionObject& transportSettings,
        UaExtensionObject& messageSettings,
        const UaDataSetReaderDataTypes &dataSetReaders
        );

    UaString getName() const;
    OpcUa_Boolean getEnabled() const;
    OpcUa_MessageSecurityMode getSecurityMode() const;
    UaString getSecurityGroupId() const;
    void getSecurityKeyServices(UaEndpointDescriptions& securityKeyServices) const;
    OpcUa_UInt32 getMaxNetworkMessageSize() const;
    void getGroupProperties(UaKeyValuePairs& groupProperties) const;
    UaExtensionObject getTransportSettings() const;
    UaExtensionObject getMessageSettings() const;
    void getDataSetReaders(UaDataSetReaderDataTypes& dataSetReaders) const;

    void setName(const UaString& name);
    void setEnabled(OpcUa_Boolean enabled);
    void setSecurityMode(OpcUa_MessageSecurityMode securityMode);
    void setSecurityGroupId(const UaString& securityGroupId);
    void setSecurityKeyServices(const UaEndpointDescriptions &securityKeyServices);
    void setMaxNetworkMessageSize(OpcUa_UInt32 maxNetworkMessageSize);
    void setGroupProperties(const UaKeyValuePairs &groupProperties);
    void setTransportSettings(UaExtensionObject& transportSettings);
    void setMessageSettings(UaExtensionObject& messageSettings);
    void setDataSetReaders(const UaDataSetReaderDataTypes &dataSetReaders);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_ReaderGroupDataType.
 *
 *  This class encapsulates an array of the native OpcUa_ReaderGroupDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaReaderGroupDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaReaderGroupDataTypes
{
public:
    UaReaderGroupDataTypes();
    UaReaderGroupDataTypes(const UaReaderGroupDataTypes &other);
    UaReaderGroupDataTypes(OpcUa_Int32 length, OpcUa_ReaderGroupDataType* data);
    virtual ~UaReaderGroupDataTypes();

    UaReaderGroupDataTypes& operator=(const UaReaderGroupDataTypes &other);
    const OpcUa_ReaderGroupDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_ReaderGroupDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaReaderGroupDataTypes &other) const;
    bool operator!=(const UaReaderGroupDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_ReaderGroupDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_ReaderGroupDataType* data);
    OpcUa_ReaderGroupDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_ReaderGroupDataType* rawData() const {return m_data;}
    inline OpcUa_ReaderGroupDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setReaderGroupDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setReaderGroupDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setReaderGroupDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setReaderGroupDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setReaderGroupDataTypes(OpcUa_Int32 length, OpcUa_ReaderGroupDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_ReaderGroupDataType* m_data;
};

#endif // UAREADERGROUPDATATYPE_H

