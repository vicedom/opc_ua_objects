/******************************************************************************
** uauadpwritergroupmessagedatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaUadpWriterGroupMessageDataType class.
**
******************************************************************************/
#ifndef UAUADPWRITERGROUPMESSAGEDATATYPE_H
#define UAUADPWRITERGROUPMESSAGEDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaUadpWriterGroupMessageDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_UadpWriterGroupMessageDataType.
 *
 *  This class encapsulates the native OpcUa_UadpWriterGroupMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaUadpWriterGroupMessageDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared UadpWriterGroupMessageDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaUadpWriterGroupMessageDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaUadpWriterGroupMessageDataType
{
    UA_DECLARE_PRIVATE(UaUadpWriterGroupMessageDataType)
public:
    UaUadpWriterGroupMessageDataType();
    UaUadpWriterGroupMessageDataType(const UaUadpWriterGroupMessageDataType &other);
    UaUadpWriterGroupMessageDataType(const OpcUa_UadpWriterGroupMessageDataType &other);
    UaUadpWriterGroupMessageDataType(
        OpcUa_UInt32 groupVersion,
        OpcUa_DataSetOrderingType dataSetOrdering,
        OpcUa_UadpNetworkMessageContentMask networkMessageContentMask,
        OpcUa_Double samplingOffset,
        const UaDoubleArray &publishingOffset
        );
    UaUadpWriterGroupMessageDataType(const UaExtensionObject &extensionObject);
    UaUadpWriterGroupMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    UaUadpWriterGroupMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaUadpWriterGroupMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaUadpWriterGroupMessageDataType();

    void clear();

    bool operator==(const UaUadpWriterGroupMessageDataType &other) const;
    bool operator!=(const UaUadpWriterGroupMessageDataType &other) const;

    UaUadpWriterGroupMessageDataType& operator=(const UaUadpWriterGroupMessageDataType &other);

    OpcUa_UadpWriterGroupMessageDataType* copy() const;
    void copyTo(OpcUa_UadpWriterGroupMessageDataType *pDst) const;

    static OpcUa_UadpWriterGroupMessageDataType* clone(const OpcUa_UadpWriterGroupMessageDataType& source);
    static void cloneTo(const OpcUa_UadpWriterGroupMessageDataType& source, OpcUa_UadpWriterGroupMessageDataType& copy);

    void attach(OpcUa_UadpWriterGroupMessageDataType *pValue);
    OpcUa_UadpWriterGroupMessageDataType* detach(OpcUa_UadpWriterGroupMessageDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setUadpWriterGroupMessageDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpWriterGroupMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpWriterGroupMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpWriterGroupMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setUadpWriterGroupMessageDataType(
        OpcUa_UInt32 groupVersion,
        OpcUa_DataSetOrderingType dataSetOrdering,
        OpcUa_UadpNetworkMessageContentMask networkMessageContentMask,
        OpcUa_Double samplingOffset,
        const UaDoubleArray &publishingOffset
        );

    OpcUa_UInt32 getGroupVersion() const;
    OpcUa_DataSetOrderingType getDataSetOrdering() const;
    OpcUa_UadpNetworkMessageContentMask getNetworkMessageContentMask() const;
    OpcUa_Double getSamplingOffset() const;
    void getPublishingOffset(UaDoubleArray& publishingOffset) const;

    void setGroupVersion(OpcUa_UInt32 groupVersion);
    void setDataSetOrdering(OpcUa_DataSetOrderingType dataSetOrdering);
    void setNetworkMessageContentMask(OpcUa_UadpNetworkMessageContentMask networkMessageContentMask);
    void setSamplingOffset(OpcUa_Double samplingOffset);
    void setPublishingOffset(const UaDoubleArray &publishingOffset);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_UadpWriterGroupMessageDataType.
 *
 *  This class encapsulates an array of the native OpcUa_UadpWriterGroupMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaUadpWriterGroupMessageDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaUadpWriterGroupMessageDataTypes
{
public:
    UaUadpWriterGroupMessageDataTypes();
    UaUadpWriterGroupMessageDataTypes(const UaUadpWriterGroupMessageDataTypes &other);
    UaUadpWriterGroupMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpWriterGroupMessageDataType* data);
    virtual ~UaUadpWriterGroupMessageDataTypes();

    UaUadpWriterGroupMessageDataTypes& operator=(const UaUadpWriterGroupMessageDataTypes &other);
    const OpcUa_UadpWriterGroupMessageDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_UadpWriterGroupMessageDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaUadpWriterGroupMessageDataTypes &other) const;
    bool operator!=(const UaUadpWriterGroupMessageDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_UadpWriterGroupMessageDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_UadpWriterGroupMessageDataType* data);
    OpcUa_UadpWriterGroupMessageDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_UadpWriterGroupMessageDataType* rawData() const {return m_data;}
    inline OpcUa_UadpWriterGroupMessageDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setUadpWriterGroupMessageDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setUadpWriterGroupMessageDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setUadpWriterGroupMessageDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpWriterGroupMessageDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpWriterGroupMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpWriterGroupMessageDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_UadpWriterGroupMessageDataType* m_data;
};

#endif // UAUADPWRITERGROUPMESSAGEDATATYPE_H

