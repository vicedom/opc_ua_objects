/******************************************************************************
** uauadpdatasetreadermessagedatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaUadpDataSetReaderMessageDataType class.
**
******************************************************************************/
#ifndef UAUADPDATASETREADERMESSAGEDATATYPE_H
#define UAUADPDATASETREADERMESSAGEDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uaguid.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaUadpDataSetReaderMessageDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_UadpDataSetReaderMessageDataType.
 *
 *  This class encapsulates the native OpcUa_UadpDataSetReaderMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaUadpDataSetReaderMessageDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared UadpDataSetReaderMessageDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaUadpDataSetReaderMessageDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaUadpDataSetReaderMessageDataType
{
    UA_DECLARE_PRIVATE(UaUadpDataSetReaderMessageDataType)
public:
    UaUadpDataSetReaderMessageDataType();
    UaUadpDataSetReaderMessageDataType(const UaUadpDataSetReaderMessageDataType &other);
    UaUadpDataSetReaderMessageDataType(const OpcUa_UadpDataSetReaderMessageDataType &other);
    UaUadpDataSetReaderMessageDataType(
        OpcUa_UInt32 groupVersion,
        OpcUa_UInt16 networkMessageNumber,
        OpcUa_UInt16 dataSetOffset,
        const UaGuid& dataSetClassId,
        OpcUa_UadpNetworkMessageContentMask networkMessageContentMask,
        OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask,
        OpcUa_Double publishingInterval,
        OpcUa_Double receiveOffset,
        OpcUa_Double processingOffset
        );
    UaUadpDataSetReaderMessageDataType(const UaExtensionObject &extensionObject);
    UaUadpDataSetReaderMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    UaUadpDataSetReaderMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaUadpDataSetReaderMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaUadpDataSetReaderMessageDataType();

    void clear();

    bool operator==(const UaUadpDataSetReaderMessageDataType &other) const;
    bool operator!=(const UaUadpDataSetReaderMessageDataType &other) const;

    UaUadpDataSetReaderMessageDataType& operator=(const UaUadpDataSetReaderMessageDataType &other);

    OpcUa_UadpDataSetReaderMessageDataType* copy() const;
    void copyTo(OpcUa_UadpDataSetReaderMessageDataType *pDst) const;

    static OpcUa_UadpDataSetReaderMessageDataType* clone(const OpcUa_UadpDataSetReaderMessageDataType& source);
    static void cloneTo(const OpcUa_UadpDataSetReaderMessageDataType& source, OpcUa_UadpDataSetReaderMessageDataType& copy);

    void attach(OpcUa_UadpDataSetReaderMessageDataType *pValue);
    OpcUa_UadpDataSetReaderMessageDataType* detach(OpcUa_UadpDataSetReaderMessageDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setUadpDataSetReaderMessageDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setUadpDataSetReaderMessageDataType(
        OpcUa_UInt32 groupVersion,
        OpcUa_UInt16 networkMessageNumber,
        OpcUa_UInt16 dataSetOffset,
        const UaGuid& dataSetClassId,
        OpcUa_UadpNetworkMessageContentMask networkMessageContentMask,
        OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask,
        OpcUa_Double publishingInterval,
        OpcUa_Double receiveOffset,
        OpcUa_Double processingOffset
        );

    OpcUa_UInt32 getGroupVersion() const;
    OpcUa_UInt16 getNetworkMessageNumber() const;
    OpcUa_UInt16 getDataSetOffset() const;
    UaGuid getDataSetClassId() const;
    OpcUa_UadpNetworkMessageContentMask getNetworkMessageContentMask() const;
    OpcUa_UadpDataSetMessageContentMask getDataSetMessageContentMask() const;
    OpcUa_Double getPublishingInterval() const;
    OpcUa_Double getReceiveOffset() const;
    OpcUa_Double getProcessingOffset() const;

    void setGroupVersion(OpcUa_UInt32 groupVersion);
    void setNetworkMessageNumber(OpcUa_UInt16 networkMessageNumber);
    void setDataSetOffset(OpcUa_UInt16 dataSetOffset);
    void setDataSetClassId(const UaGuid& dataSetClassId);
    void setNetworkMessageContentMask(OpcUa_UadpNetworkMessageContentMask networkMessageContentMask);
    void setDataSetMessageContentMask(OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask);
    void setPublishingInterval(OpcUa_Double publishingInterval);
    void setReceiveOffset(OpcUa_Double receiveOffset);
    void setProcessingOffset(OpcUa_Double processingOffset);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_UadpDataSetReaderMessageDataType.
 *
 *  This class encapsulates an array of the native OpcUa_UadpDataSetReaderMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaUadpDataSetReaderMessageDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaUadpDataSetReaderMessageDataTypes
{
public:
    UaUadpDataSetReaderMessageDataTypes();
    UaUadpDataSetReaderMessageDataTypes(const UaUadpDataSetReaderMessageDataTypes &other);
    UaUadpDataSetReaderMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpDataSetReaderMessageDataType* data);
    virtual ~UaUadpDataSetReaderMessageDataTypes();

    UaUadpDataSetReaderMessageDataTypes& operator=(const UaUadpDataSetReaderMessageDataTypes &other);
    const OpcUa_UadpDataSetReaderMessageDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_UadpDataSetReaderMessageDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaUadpDataSetReaderMessageDataTypes &other) const;
    bool operator!=(const UaUadpDataSetReaderMessageDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_UadpDataSetReaderMessageDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_UadpDataSetReaderMessageDataType* data);
    OpcUa_UadpDataSetReaderMessageDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_UadpDataSetReaderMessageDataType* rawData() const {return m_data;}
    inline OpcUa_UadpDataSetReaderMessageDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setUadpDataSetReaderMessageDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetReaderMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpDataSetReaderMessageDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_UadpDataSetReaderMessageDataType* m_data;
};

#endif // UAUADPDATASETREADERMESSAGEDATATYPE_H

