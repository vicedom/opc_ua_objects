/******************************************************************************
** uapublishedvariabledatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaPublishedVariableDataType class.
**
******************************************************************************/
#ifndef UAPUBLISHEDVARIABLEDATATYPE_H
#define UAPUBLISHEDVARIABLEDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uanodeid.h"
#include "uaqualifiedname.h"
#include "uastring.h"
#include "uavariant.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaDataValue;

class UABASE_EXPORT UaPublishedVariableDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_PublishedVariableDataType.
 *
 *  This class encapsulates the native OpcUa_PublishedVariableDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaPublishedVariableDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared PublishedVariableDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaPublishedVariableDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaPublishedVariableDataType
{
    UA_DECLARE_PRIVATE(UaPublishedVariableDataType)
public:
    UaPublishedVariableDataType();
    UaPublishedVariableDataType(const UaPublishedVariableDataType &other);
    UaPublishedVariableDataType(const OpcUa_PublishedVariableDataType &other);
    UaPublishedVariableDataType(
        const UaNodeId& publishedVariable,
        OpcUa_UInt32 attributeId,
        OpcUa_Double samplingIntervalHint,
        OpcUa_UInt32 deadbandType,
        OpcUa_Double deadbandValue,
        const UaString& indexRange,
        const UaVariant&substituteValue,
        const UaQualifiedNameArray &metaDataProperties
        );
    UaPublishedVariableDataType(const UaExtensionObject &extensionObject);
    UaPublishedVariableDataType(const OpcUa_ExtensionObject &extensionObject);
    UaPublishedVariableDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaPublishedVariableDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaPublishedVariableDataType();

    void clear();

    bool operator==(const UaPublishedVariableDataType &other) const;
    bool operator!=(const UaPublishedVariableDataType &other) const;

    UaPublishedVariableDataType& operator=(const UaPublishedVariableDataType &other);

    OpcUa_PublishedVariableDataType* copy() const;
    void copyTo(OpcUa_PublishedVariableDataType *pDst) const;

    static OpcUa_PublishedVariableDataType* clone(const OpcUa_PublishedVariableDataType& source);
    static void cloneTo(const OpcUa_PublishedVariableDataType& source, OpcUa_PublishedVariableDataType& copy);

    void attach(OpcUa_PublishedVariableDataType *pValue);
    OpcUa_PublishedVariableDataType* detach(OpcUa_PublishedVariableDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setPublishedVariableDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setPublishedVariableDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setPublishedVariableDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPublishedVariableDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setPublishedVariableDataType(
        const UaNodeId& publishedVariable,
        OpcUa_UInt32 attributeId,
        OpcUa_Double samplingIntervalHint,
        OpcUa_UInt32 deadbandType,
        OpcUa_Double deadbandValue,
        const UaString& indexRange,
        const UaVariant&substituteValue,
        const UaQualifiedNameArray &metaDataProperties
        );

    UaNodeId getPublishedVariable() const;
    OpcUa_UInt32 getAttributeId() const;
    OpcUa_Double getSamplingIntervalHint() const;
    OpcUa_UInt32 getDeadbandType() const;
    OpcUa_Double getDeadbandValue() const;
    UaString getIndexRange() const;
    UaVariant getSubstituteValue() const;
    void getMetaDataProperties(UaQualifiedNameArray& metaDataProperties) const;

    void setPublishedVariable(const UaNodeId& publishedVariable);
    void setAttributeId(OpcUa_UInt32 attributeId);
    void setSamplingIntervalHint(OpcUa_Double samplingIntervalHint);
    void setDeadbandType(OpcUa_UInt32 deadbandType);
    void setDeadbandValue(OpcUa_Double deadbandValue);
    void setIndexRange(const UaString& indexRange);
    void setSubstituteValue(const UaVariant&substituteValue);
    void setMetaDataProperties(const UaQualifiedNameArray &metaDataProperties);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_PublishedVariableDataType.
 *
 *  This class encapsulates an array of the native OpcUa_PublishedVariableDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaPublishedVariableDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaPublishedVariableDataTypes
{
public:
    UaPublishedVariableDataTypes();
    UaPublishedVariableDataTypes(const UaPublishedVariableDataTypes &other);
    UaPublishedVariableDataTypes(OpcUa_Int32 length, OpcUa_PublishedVariableDataType* data);
    virtual ~UaPublishedVariableDataTypes();

    UaPublishedVariableDataTypes& operator=(const UaPublishedVariableDataTypes &other);
    const OpcUa_PublishedVariableDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_PublishedVariableDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaPublishedVariableDataTypes &other) const;
    bool operator!=(const UaPublishedVariableDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_PublishedVariableDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_PublishedVariableDataType* data);
    OpcUa_PublishedVariableDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_PublishedVariableDataType* rawData() const {return m_data;}
    inline OpcUa_PublishedVariableDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setPublishedVariableDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setPublishedVariableDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setPublishedVariableDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPublishedVariableDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setPublishedVariableDataTypes(OpcUa_Int32 length, OpcUa_PublishedVariableDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_PublishedVariableDataType* m_data;
};

#endif // UAPUBLISHEDVARIABLEDATATYPE_H

