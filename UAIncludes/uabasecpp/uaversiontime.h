/******************************************************************************
** uaversiontime.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __UAVERSIONTIME_H__
#define __UAVERSIONTIME_H__

#include "uabase.h"
#include "uadatetime.h"

/** Class UaVersionTime.
*/
class UABASE_EXPORT UaVersionTime
{
public:
    UaVersionTime();
    UaVersionTime(const UaVersionTime &other);
    UaVersionTime(OpcUa_UInt32 versionTime);
    ~UaVersionTime();
    void clear();

    UaVersionTime& operator=(const UaVersionTime &other);

    static UaVersionTime now();

    inline OpcUa_UInt32 versionTime() const { return m_versionTime; }
    UaDateTime toDateTime() const;

private:
    OpcUa_UInt32 m_versionTime;
};


#endif // #ifndef __UAVERSIONTIME_H__

