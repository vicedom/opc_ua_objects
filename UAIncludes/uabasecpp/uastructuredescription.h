/******************************************************************************
** uastructuredescription.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaStructureDescription class.
**
******************************************************************************/
#ifndef UASTRUCTUREDESCRIPTION_H
#define UASTRUCTUREDESCRIPTION_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uanodeid.h"
#include "uaqualifiedname.h"
#include "uastructuredefinitiondatatype.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaStructureDescriptionPrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_StructureDescription.
 *
 *  This class encapsulates the native OpcUa_StructureDescription structure
 *  and handles memory allocation and cleanup for you.
 *  UaStructureDescription uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared StructureDescription it creates a copy for that (copy-on-write).
 *  So assigning another UaStructureDescription or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaStructureDescription
{
    UA_DECLARE_PRIVATE(UaStructureDescription)
public:
    UaStructureDescription();
    UaStructureDescription(const UaStructureDescription &other);
    UaStructureDescription(const OpcUa_StructureDescription &other);
    UaStructureDescription(
        const UaNodeId& dataTypeId,
        const UaQualifiedName& name,
        const UaStructureDefinitionDataType& structureDefinition
        );
    UaStructureDescription(const UaExtensionObject &extensionObject);
    UaStructureDescription(const OpcUa_ExtensionObject &extensionObject);
    UaStructureDescription(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaStructureDescription(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaStructureDescription();

    void clear();

    bool operator==(const UaStructureDescription &other) const;
    bool operator!=(const UaStructureDescription &other) const;

    UaStructureDescription& operator=(const UaStructureDescription &other);

    OpcUa_StructureDescription* copy() const;
    void copyTo(OpcUa_StructureDescription *pDst) const;

    static OpcUa_StructureDescription* clone(const OpcUa_StructureDescription& source);
    static void cloneTo(const OpcUa_StructureDescription& source, OpcUa_StructureDescription& copy);

    void attach(OpcUa_StructureDescription *pValue);
    OpcUa_StructureDescription* detach(OpcUa_StructureDescription* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setStructureDescription(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setStructureDescription(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setStructureDescription(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setStructureDescription(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setStructureDescription(
        const UaNodeId& dataTypeId,
        const UaQualifiedName& name,
        const UaStructureDefinitionDataType& structureDefinition
        );

    UaNodeId getDataTypeId() const;
    UaQualifiedName getName() const;
    UaStructureDefinitionDataType getStructureDefinition() const;

    void setDataTypeId(const UaNodeId& dataTypeId);
    void setName(const UaQualifiedName& name);
    void setStructureDefinition(const UaStructureDefinitionDataType& structureDefinition);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_StructureDescription.
 *
 *  This class encapsulates an array of the native OpcUa_StructureDescription structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaStructureDescription for information about the encapsulated structure.
 */
class UABASE_EXPORT UaStructureDescriptions
{
public:
    UaStructureDescriptions();
    UaStructureDescriptions(const UaStructureDescriptions &other);
    UaStructureDescriptions(OpcUa_Int32 length, OpcUa_StructureDescription* data);
    virtual ~UaStructureDescriptions();

    UaStructureDescriptions& operator=(const UaStructureDescriptions &other);
    const OpcUa_StructureDescription& operator[](OpcUa_UInt32 index) const;
    OpcUa_StructureDescription& operator[](OpcUa_UInt32 index);

    bool operator==(const UaStructureDescriptions &other) const;
    bool operator!=(const UaStructureDescriptions &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_StructureDescription* data);
    void attach(OpcUa_Int32 length, OpcUa_StructureDescription* data);
    OpcUa_StructureDescription* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_StructureDescription* rawData() const {return m_data;}
    inline OpcUa_StructureDescription* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setStructureDescriptions(const UaVariant &variant);
    OpcUa_StatusCode setStructureDescriptions(const OpcUa_Variant &variant);
    OpcUa_StatusCode setStructureDescriptions(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setStructureDescriptions(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setStructureDescriptions(OpcUa_Int32 length, OpcUa_StructureDescription* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_StructureDescription* m_data;
};

#endif // UASTRUCTUREDESCRIPTION_H

