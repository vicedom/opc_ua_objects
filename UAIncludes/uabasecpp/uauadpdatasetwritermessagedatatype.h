/******************************************************************************
** uauadpdatasetwritermessagedatatype.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaUadpDataSetWriterMessageDataType class.
**
******************************************************************************/
#ifndef UAUADPDATASETWRITERMESSAGEDATATYPE_H
#define UAUADPDATASETWRITERMESSAGEDATATYPE_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaUadpDataSetWriterMessageDataTypePrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_UadpDataSetWriterMessageDataType.
 *
 *  This class encapsulates the native OpcUa_UadpDataSetWriterMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  UaUadpDataSetWriterMessageDataType uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared UadpDataSetWriterMessageDataType it creates a copy for that (copy-on-write).
 *  So assigning another UaUadpDataSetWriterMessageDataType or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaUadpDataSetWriterMessageDataType
{
    UA_DECLARE_PRIVATE(UaUadpDataSetWriterMessageDataType)
public:
    UaUadpDataSetWriterMessageDataType();
    UaUadpDataSetWriterMessageDataType(const UaUadpDataSetWriterMessageDataType &other);
    UaUadpDataSetWriterMessageDataType(const OpcUa_UadpDataSetWriterMessageDataType &other);
    UaUadpDataSetWriterMessageDataType(
        OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask,
        OpcUa_UInt16 configuredSize,
        OpcUa_UInt16 networkMessageNumber,
        OpcUa_UInt16 dataSetOffset
        );
    UaUadpDataSetWriterMessageDataType(const UaExtensionObject &extensionObject);
    UaUadpDataSetWriterMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    UaUadpDataSetWriterMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaUadpDataSetWriterMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaUadpDataSetWriterMessageDataType();

    void clear();

    bool operator==(const UaUadpDataSetWriterMessageDataType &other) const;
    bool operator!=(const UaUadpDataSetWriterMessageDataType &other) const;

    UaUadpDataSetWriterMessageDataType& operator=(const UaUadpDataSetWriterMessageDataType &other);

    OpcUa_UadpDataSetWriterMessageDataType* copy() const;
    void copyTo(OpcUa_UadpDataSetWriterMessageDataType *pDst) const;

    static OpcUa_UadpDataSetWriterMessageDataType* clone(const OpcUa_UadpDataSetWriterMessageDataType& source);
    static void cloneTo(const OpcUa_UadpDataSetWriterMessageDataType& source, OpcUa_UadpDataSetWriterMessageDataType& copy);

    void attach(OpcUa_UadpDataSetWriterMessageDataType *pValue);
    OpcUa_UadpDataSetWriterMessageDataType* detach(OpcUa_UadpDataSetWriterMessageDataType* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setUadpDataSetWriterMessageDataType(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataType(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataType(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataType(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setUadpDataSetWriterMessageDataType(
        OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask,
        OpcUa_UInt16 configuredSize,
        OpcUa_UInt16 networkMessageNumber,
        OpcUa_UInt16 dataSetOffset
        );

    OpcUa_UadpDataSetMessageContentMask getDataSetMessageContentMask() const;
    OpcUa_UInt16 getConfiguredSize() const;
    OpcUa_UInt16 getNetworkMessageNumber() const;
    OpcUa_UInt16 getDataSetOffset() const;

    void setDataSetMessageContentMask(OpcUa_UadpDataSetMessageContentMask dataSetMessageContentMask);
    void setConfiguredSize(OpcUa_UInt16 configuredSize);
    void setNetworkMessageNumber(OpcUa_UInt16 networkMessageNumber);
    void setDataSetOffset(OpcUa_UInt16 dataSetOffset);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_UadpDataSetWriterMessageDataType.
 *
 *  This class encapsulates an array of the native OpcUa_UadpDataSetWriterMessageDataType structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaUadpDataSetWriterMessageDataType for information about the encapsulated structure.
 */
class UABASE_EXPORT UaUadpDataSetWriterMessageDataTypes
{
public:
    UaUadpDataSetWriterMessageDataTypes();
    UaUadpDataSetWriterMessageDataTypes(const UaUadpDataSetWriterMessageDataTypes &other);
    UaUadpDataSetWriterMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpDataSetWriterMessageDataType* data);
    virtual ~UaUadpDataSetWriterMessageDataTypes();

    UaUadpDataSetWriterMessageDataTypes& operator=(const UaUadpDataSetWriterMessageDataTypes &other);
    const OpcUa_UadpDataSetWriterMessageDataType& operator[](OpcUa_UInt32 index) const;
    OpcUa_UadpDataSetWriterMessageDataType& operator[](OpcUa_UInt32 index);

    bool operator==(const UaUadpDataSetWriterMessageDataTypes &other) const;
    bool operator!=(const UaUadpDataSetWriterMessageDataTypes &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_UadpDataSetWriterMessageDataType* data);
    void attach(OpcUa_Int32 length, OpcUa_UadpDataSetWriterMessageDataType* data);
    OpcUa_UadpDataSetWriterMessageDataType* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_UadpDataSetWriterMessageDataType* rawData() const {return m_data;}
    inline OpcUa_UadpDataSetWriterMessageDataType* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setUadpDataSetWriterMessageDataTypes(const UaVariant &variant);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataTypes(const OpcUa_Variant &variant);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataTypes(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataTypes(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setUadpDataSetWriterMessageDataTypes(OpcUa_Int32 length, OpcUa_UadpDataSetWriterMessageDataType* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_UadpDataSetWriterMessageDataType* m_data;
};

#endif // UAUADPDATASETWRITERMESSAGEDATATYPE_H

