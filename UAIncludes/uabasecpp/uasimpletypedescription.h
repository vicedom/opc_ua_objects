/******************************************************************************
** uasimpletypedescription.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Project: C++ OPC SDK base module
**
** Portable UaSimpleTypeDescription class.
**
******************************************************************************/
#ifndef UASIMPLETYPEDESCRIPTION_H
#define UASIMPLETYPEDESCRIPTION_H

#include <opcua_proxystub.h>

#include "uabase.h"
#include "uanodeid.h"
#include "uaqualifiedname.h"
#include "uaarraytemplates.h"

class UaExtensionObject;
class UaVariant;
class UaDataValue;

class UABASE_EXPORT UaSimpleTypeDescriptionPrivate;

/** @ingroup CppBaseLibraryClass
 *  @brief Wrapper class for the UA stack structure OpcUa_SimpleTypeDescription.
 *
 *  This class encapsulates the native OpcUa_SimpleTypeDescription structure
 *  and handles memory allocation and cleanup for you.
 *  UaSimpleTypeDescription uses implicit sharing to avoid needless copying and to boost the performance.
 *  Only if you modify a shared SimpleTypeDescription it creates a copy for that (copy-on-write).
 *  So assigning another UaSimpleTypeDescription or passing it as parameter needs constant time and is nearly as fast as assigning a pointer.
 */
class UABASE_EXPORT UaSimpleTypeDescription
{
    UA_DECLARE_PRIVATE(UaSimpleTypeDescription)
public:
    UaSimpleTypeDescription();
    UaSimpleTypeDescription(const UaSimpleTypeDescription &other);
    UaSimpleTypeDescription(const OpcUa_SimpleTypeDescription &other);
    UaSimpleTypeDescription(
        const UaNodeId& dataTypeId,
        const UaQualifiedName& name,
        const UaNodeId& baseDataType,
        OpcUa_Byte builtInType
        );
    UaSimpleTypeDescription(const UaExtensionObject &extensionObject);
    UaSimpleTypeDescription(const OpcUa_ExtensionObject &extensionObject);
    UaSimpleTypeDescription(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    UaSimpleTypeDescription(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    ~UaSimpleTypeDescription();

    void clear();

    bool operator==(const UaSimpleTypeDescription &other) const;
    bool operator!=(const UaSimpleTypeDescription &other) const;

    UaSimpleTypeDescription& operator=(const UaSimpleTypeDescription &other);

    OpcUa_SimpleTypeDescription* copy() const;
    void copyTo(OpcUa_SimpleTypeDescription *pDst) const;

    static OpcUa_SimpleTypeDescription* clone(const OpcUa_SimpleTypeDescription& source);
    static void cloneTo(const OpcUa_SimpleTypeDescription& source, OpcUa_SimpleTypeDescription& copy);

    void attach(OpcUa_SimpleTypeDescription *pValue);
    OpcUa_SimpleTypeDescription* detach(OpcUa_SimpleTypeDescription* pDst);

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    void toExtensionObject(UaExtensionObject &extensionObject) const;
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject) const;
    void toExtensionObject(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    void toExtensionObject(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    OpcUa_StatusCode setSimpleTypeDescription(const UaExtensionObject &extensionObject);
    OpcUa_StatusCode setSimpleTypeDescription(const OpcUa_ExtensionObject &extensionObject);
    OpcUa_StatusCode setSimpleTypeDescription(UaExtensionObject &extensionObject, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setSimpleTypeDescription(OpcUa_ExtensionObject &extensionObject, OpcUa_Boolean bDetach);

    void setSimpleTypeDescription(
        const UaNodeId& dataTypeId,
        const UaQualifiedName& name,
        const UaNodeId& baseDataType,
        OpcUa_Byte builtInType
        );

    UaNodeId getDataTypeId() const;
    UaQualifiedName getName() const;
    UaNodeId getBaseDataType() const;
    OpcUa_Byte getBuiltInType() const;

    void setDataTypeId(const UaNodeId& dataTypeId);
    void setName(const UaQualifiedName& name);
    void setBaseDataType(const UaNodeId& baseDataType);
    void setBuiltInType(OpcUa_Byte builtInType);
};

/** @ingroup CppBaseLibraryClass
 *  @brief Array class for the UA stack structure OpcUa_SimpleTypeDescription.
 *
 *  This class encapsulates an array of the native OpcUa_SimpleTypeDescription structure
 *  and handles memory allocation and cleanup for you.
 *  @see UaSimpleTypeDescription for information about the encapsulated structure.
 */
class UABASE_EXPORT UaSimpleTypeDescriptions
{
public:
    UaSimpleTypeDescriptions();
    UaSimpleTypeDescriptions(const UaSimpleTypeDescriptions &other);
    UaSimpleTypeDescriptions(OpcUa_Int32 length, OpcUa_SimpleTypeDescription* data);
    virtual ~UaSimpleTypeDescriptions();

    UaSimpleTypeDescriptions& operator=(const UaSimpleTypeDescriptions &other);
    const OpcUa_SimpleTypeDescription& operator[](OpcUa_UInt32 index) const;
    OpcUa_SimpleTypeDescription& operator[](OpcUa_UInt32 index);

    bool operator==(const UaSimpleTypeDescriptions &other) const;
    bool operator!=(const UaSimpleTypeDescriptions &other) const;

    void attach(OpcUa_UInt32 length, OpcUa_SimpleTypeDescription* data);
    void attach(OpcUa_Int32 length, OpcUa_SimpleTypeDescription* data);
    OpcUa_SimpleTypeDescription* detach();

    void create(OpcUa_UInt32 length);
    void resize(OpcUa_UInt32 length);
    void clear();

    inline OpcUa_UInt32 length() const {return m_noOfElements;}
    inline const OpcUa_SimpleTypeDescription* rawData() const {return m_data;}
    inline OpcUa_SimpleTypeDescription* rawData() {return m_data;}

    void toVariant(UaVariant &variant) const;
    void toVariant(OpcUa_Variant &variant) const;
    void toVariant(UaVariant &variant, OpcUa_Boolean bDetach);
    void toVariant(OpcUa_Variant &variant, OpcUa_Boolean bDetach);

    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean updateTimeStamps) const;
    void toDataValue(UaDataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);
    void toDataValue(OpcUa_DataValue &dataValue, OpcUa_Boolean bDetach, OpcUa_Boolean updateTimeStamps);

    OpcUa_StatusCode setSimpleTypeDescriptions(const UaVariant &variant);
    OpcUa_StatusCode setSimpleTypeDescriptions(const OpcUa_Variant &variant);
    OpcUa_StatusCode setSimpleTypeDescriptions(UaVariant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setSimpleTypeDescriptions(OpcUa_Variant &variant, OpcUa_Boolean bDetach);
    OpcUa_StatusCode setSimpleTypeDescriptions(OpcUa_Int32 length, OpcUa_SimpleTypeDescription* data);

private:
    OpcUa_UInt32 m_noOfElements;
    OpcUa_SimpleTypeDescription* m_data;
};

#endif // UASIMPLETYPEDESCRIPTION_H

