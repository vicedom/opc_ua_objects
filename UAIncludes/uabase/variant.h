/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_VARIANT_H_
#define _UABASE_VARIANT_H_

#include <platform/platform.h>
#include "variant_types.h"

/* includes for convenience and to not break existing includes */
#include "variant_setter.h"
#include "variant_getter.h"
#include "variant_utility.h"

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_variant ua_variant
 *  @brief A union of all built-in data types including an
 * OpcUa_ExtensionObject.
 *
 * There are further functons available:
 * * Getter: @ref ua_base_variant_getter
 * * Setter: @ref ua_base_variant_setter
 * * Utility: @ref ua_base_variant_utility
 *
 * Variants can also contain arrays of any of these built-in
 * types. Variants are used to store any value or parameter with a
 * data type of BaseDataType or one of its subtypes.
 *
 * Variants can be empty. An empty Variant is described as having a
 * null value and should be treated like a null column in a SQL
 * database. A null value in a Variant may not be the same as a null
 * value for data types that support nulls such as Strings. Some
 * development platforms may not be able to preserve the distinction
 * between a null for a DataType and a null for a Variant. Therefore
 * applications shall not rely on this distinction.
 *
 * Variants can contain arrays of Variants but they cannot directly
 * contain another Variant.
 *
 * DataValue and DiagnosticInfo types only have meaning when returned
 * in a response message with an associated StatusCode. As a result,
 * Variants cannot contain instances of DataValue or
 * DiagnosticInfo. This requirement means that if an attribute
 * supports the writing of a null value, it shall also support writing
 * of an empty Variant and vice versa.
 *
 * Variables with a DataType of BaseDataType are mapped to a Variant,
 * however, the ValueRank and ArrayDimensions attributes place
 * restrictions on what is allowed in the Variant. For example, if the
 * ValueRank is Scalar, the Variant may only contain scalar values.
 *
 * ExtensionObjects and Variants allow unlimited nesting which could
 * result in stack overflow errors even if the message size is less
 * than the maximum allowed. Decoders shall support at least 100
 * nesting levels. Decoders shall report an error if the number of
 * nesting levels exceeds what it supports.
 *
 * @ingroup ua_base
 * @{
 */

void ua_variant_init(struct ua_variant *v);
void ua_variant_clear(struct ua_variant *v);

int ua_variant_compare(const struct ua_variant *a, const struct ua_variant *b) UA_PURE_FUNCTION;
int ua_variant_copy(struct ua_variant *dst, const struct ua_variant *src);

/** Test if the ua_variant \c v is a null variant */
static inline bool ua_variant_is_null(const struct ua_variant *v)
{
    return (v->type == UA_VT_NULL);
}

/** Returns the built-in datatype of the given variant.
 * This unmasks any array or matrix flags so that the return
 * value contains the pure datatype information.
 */
static inline enum ua_variant_type ua_variant_get_type(const struct ua_variant *v)
{
    return (enum ua_variant_type)(v->type & UA_VT_TYPEMASK);
}

/** Returns true if the given variant is an array.
 * This is also true for matrix values.
 */
static inline bool ua_variant_is_array(const struct ua_variant *v)
{
    return ((v->type & UA_VT_IS_ARRAY) == UA_VT_IS_ARRAY) ? true : false;
}

/** Returns true if the given variant is a matrix.
 * A matrix is any form for multi-dimensional array, which
 * has more than one dimension.
 */
static inline bool ua_variant_is_matrix(const struct ua_variant *v)
{
    return ((v->type & UA_VT_ARRAYMASK) == UA_VT_IS_MATRIX) ? true : false;
}

/** Returns true if the given variant is a scalar value.  */
static inline bool ua_variant_is_scalar(const struct ua_variant *v)
{
    return ((v->type & UA_VT_ARRAYMASK) == 0) ? true : false;
}

/**
 * @internal
 * Return the properties for a certain variant type.
 * This is used SDK internally for implementing certain variant functionality
 */
const struct ua_variant_base_properties *ua_variant_get_type_properties(uint8_t type);

/** @} */

UA_END_EXTERN_C

#endif /* _UABASE_VARIANT_H_ */

