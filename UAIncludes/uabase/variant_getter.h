/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_VARIANT_GETTER_H_
#define _UABASE_VARIANT_GETTER_H_

#include <platform/platform.h>
#include "variant_types.h"

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_variant_getter ua_variant_getter
 * @brief Getter and detach functions for the @ref ua_base_variant structure.
 *
 * Getter functions usually return a copy of the data which is independent of
 * the orginal data and must be cleared separately. There are some exceptions regarding
 * the array and matrix handling which will return const data still attach to the
 * original value.
 *
 * Detach functions return the original data but remove the data from the original structure.
 * Thus the detached data must be cleared separately and the original structure is usually no
 * longer of use.
 *
 * @ingroup ua_base_variant
 * @{
 */

/* scalar get */
int ua_variant_get_bool(const struct ua_variant *v, bool *val) UA_NONNULL(1, 2);
int ua_variant_get_byte(const struct ua_variant *v, uint8_t *val) UA_NONNULL(1, 2);
int ua_variant_get_sbyte(const struct ua_variant *v, int8_t *val) UA_NONNULL(1, 2);
int ua_variant_get_uint16(const struct ua_variant *v, uint16_t *val) UA_NONNULL(1, 2);
int ua_variant_get_int16(const struct ua_variant *v, int16_t *val) UA_NONNULL(1, 2);
int ua_variant_get_uint32(const struct ua_variant *v, uint32_t *val) UA_NONNULL(1, 2);
int ua_variant_get_int32(const struct ua_variant *v, int32_t *val) UA_NONNULL(1, 2);
int ua_variant_get_uint64(const struct ua_variant *v, uint64_t *val) UA_NONNULL(1, 2);
int ua_variant_get_int64(const struct ua_variant *v, int64_t *val) UA_NONNULL(1, 2);
int ua_variant_get_float(const struct ua_variant *v, float *val) UA_NONNULL(1, 2);
int ua_variant_get_double(const struct ua_variant *v, double *val) UA_NONNULL(1, 2);
int ua_variant_get_datetime(const struct ua_variant *v, ua_datetime *val) UA_NONNULL(1, 2);
int ua_variant_get_string(const struct ua_variant *v, struct ua_string *val) UA_NONNULL(1, 2);
int ua_variant_get_bytestring(const struct ua_variant *v, struct ua_bytestring *val) UA_NONNULL(1, 2);
int ua_variant_get_xmlelement(const struct ua_variant *v, struct ua_xmlelement *val) UA_NONNULL(1, 2);
int ua_variant_get_nodeid(const struct ua_variant *v, struct ua_nodeid *val) UA_NONNULL(1, 2);
int ua_variant_get_expandednodeid(const struct ua_variant *v, struct ua_expandednodeid *val) UA_NONNULL(1, 2);
int ua_variant_get_guid(const struct ua_variant *v, struct ua_guid *val) UA_NONNULL(1, 2);
int ua_variant_get_statuscode(const struct ua_variant *v, ua_statuscode *val) UA_NONNULL(1, 2);
int ua_variant_get_qualifiedname(const struct ua_variant *v, struct ua_qualifiedname *val) UA_NONNULL(1, 2);
int ua_variant_get_localizedtext(const struct ua_variant *v, struct ua_localizedtext *val) UA_NONNULL(1, 2);
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
int ua_variant_get_datavalue(const struct ua_variant *v, struct ua_datavalue *val) UA_NONNULL(1, 2);
#endif
int ua_variant_get_const_extensionobject(const struct ua_variant *v, const struct ua_extensionobject **val);

/* scalar detach */
int ua_variant_detach_extensionobject(struct ua_variant *v, void **obj, struct ua_nodeid *type_id);
int ua_variant_detach_extensionobject_of_type(struct ua_variant *v, void **obj, const struct ua_nodeid *type_id);

/* array get */
int ua_variant_get_array(const struct ua_variant *v, struct ua_variant_array const **array);
int ua_variant_get_array_data_of_type(const struct ua_variant *v, void const **data, int32_t *length, enum ua_variant_type type);
int ua_variant_array_get_data(const struct ua_variant_array *array, void const **data, int32_t *length);

/* matrix get */
int ua_variant_get_matrix(const struct ua_variant *v, struct ua_variant_matrix const **matrix);
int ua_variant_get_matrix_data_of_type(const struct ua_variant *v, void const **data, int32_t *length, enum ua_variant_type type);
int ua_variant_matrix_get_data(const struct ua_variant_matrix *matrix, void const **data, int32_t *length);
int ua_variant_matrix_get_dimensions(const struct ua_variant_matrix *matrix, int32_t const **dimensions, int32_t *num_dimensions);


/** @} */

UA_END_EXTERN_C

#endif /* inlcude guard */
