/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef ATTRIBUTEID_H_PBEHICFB
#define ATTRIBUTEID_H_PBEHICFB

#include <platform/platform.h>
#include <uabase/base_config.h>

enum ua_attributeid {
    UA_ATTRIBUTEID_NODEID = 1,
    UA_ATTRIBUTEID_NODECLASS = 2,
    UA_ATTRIBUTEID_BROWSENAME = 3,
    UA_ATTRIBUTEID_DISPLAYNAME = 4,
    UA_ATTRIBUTEID_DESCRIPTION = 5,
    UA_ATTRIBUTEID_WRITEMASK = 6,
    UA_ATTRIBUTEID_USERWRITEMASK = 7,
    UA_ATTRIBUTEID_ISABSTRACT = 8,
    UA_ATTRIBUTEID_SYMMETRIC = 9,
    UA_ATTRIBUTEID_INVERSENAME = 10,
    UA_ATTRIBUTEID_CONTAINSLOOPS = 11,
    UA_ATTRIBUTEID_EVENTNOTIFIER = 12,
    UA_ATTRIBUTEID_VALUE = 13,
    UA_ATTRIBUTEID_DATATYPE = 14,
    UA_ATTRIBUTEID_VALUERANK = 15,
    UA_ATTRIBUTEID_ARRAYDIMENSIONS = 16,
    UA_ATTRIBUTEID_ACCESSLEVEL = 17,
    UA_ATTRIBUTEID_USERACCESSLEVEL = 18,
    UA_ATTRIBUTEID_MINIMUMSAMPLINGINTERVAL = 19,
    UA_ATTRIBUTEID_HISTORIZING = 20,
    UA_ATTRIBUTEID_EXECUTABLE = 21,
    UA_ATTRIBUTEID_USEREXECUTABLE = 22
};

UA_BEGIN_EXTERN_C

#ifdef ENABLE_TO_STRING
const char* ua_attributeid_to_string(enum ua_attributeid attr);
#endif /* ENABLE_TO_STRING */

UA_END_EXTERN_C

#endif /* end of include guard: ATTRIBUTEID_H_PBEHICFB */

