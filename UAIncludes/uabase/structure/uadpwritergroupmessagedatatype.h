/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/
/****************************************************************************
** OPC UA Information Model Code generated from 'Opc.Ua.NodeSet2.xml'.
**
** Generated on: 2019-02-07
** Generated by: Unified Automation Perl Generator 1.0.0
**
** This code may soley be used with Unified Automation SDKs to create
** commercial OPC UA applications. It is not allowed to copy, distribute or
** modify the source code, nor any part of it.
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#ifndef _UABASE_UADPWRITERGROUPMESSAGEDATATYPE_H_
#define _UABASE_UADPWRITERGROUPMESSAGEDATATYPE_H_

#include <platform/platform.h>
#include <uabase/base_config.h>
#include <uabase/dict.h>
#include <stdint.h>
#include <uabase/structure/datasetorderingtype.h>
#include <uabase/structure/uadpnetworkmessagecontentmask.h>
#include <uabase/type_table.h>

UA_BEGIN_EXTERN_C

/**
 *  \ingroup ua_base_structure
 *  \struct ua_uadpwritergroupmessagedatatype

 */
struct ua_uadpwritergroupmessagedatatype {
    uint32_t group_version;
    enum ua_datasetorderingtype data_set_ordering;
    ua_uadpnetworkmessagecontentmask_t network_message_content_mask;
    double sampling_offset;
    double *publishing_offset;
    int32_t num_publishing_offset;
};

extern const struct ua_dict_field g_ua_uadpwritergroupmessagedatatype_fields[5];

void ua_uadpwritergroupmessagedatatype_init(struct ua_uadpwritergroupmessagedatatype *t);
void ua_uadpwritergroupmessagedatatype_clear(struct ua_uadpwritergroupmessagedatatype *t);
int ua_uadpwritergroupmessagedatatype_compare(const struct ua_uadpwritergroupmessagedatatype *a, const struct ua_uadpwritergroupmessagedatatype *b) UA_PURE_FUNCTION;
int ua_uadpwritergroupmessagedatatype_copy(struct ua_uadpwritergroupmessagedatatype *dst, const struct ua_uadpwritergroupmessagedatatype *src);

/**
 * @relates ua_uadpwritergroupmessagedatatype
 * @brief Resize the PublishingOffset array of a ua_uadpwritergroupmessagedatatype struct.
 *
 * Depending on \c new_len and the current size of the array, the array will be
 * created, increased, decreased or deleted. In case array members are removed
 * these are cleared properly, new array members are initialized with zero.
 *
 * @param new_len New length of the PublishingOffset array in number of members.
 * @return Zero on success or @ref ua_base_statuscode on failure.
 */
static inline int ua_uadpwritergroupmessagedatatype_resize_publishing_offset_array(struct ua_uadpwritergroupmessagedatatype *val, int32_t new_len)
{
    return ua_dict_resize_array(&g_type_table_ns0[UA_TYPE_UADPWRITERGROUPMESSAGEDATATYPE - countof(g_base_fcts_ns0)], val, 4, new_len);
}

UA_END_EXTERN_C

#endif /* _UABASE_UADPWRITERGROUPMESSAGEDATATYPE_H_ */

