/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UABASE_TYPE_TABLE_H
#define UABASE_TYPE_TABLE_H

#include "type_identifier.h"
#include "dict.h"
#include "base.h"

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_type_table ua_type_table
 * @ingroup ua_base
 *
 * Management for builtin and custom structures registered at the
 * encoder. These are automatically decoded when found inside an
 * extensionobject.
 *
 * @{
 */

/* forward declarations */
struct ua_nodeid;

/**
 * Base functions for builtin types. Also contains the type id and size.
 * The copy and clear functions may be omitted for fixed size types, in
 * this case the field must have the @ref UA_DICT_FLAG_FIXED_SIZE flag set.
 * All functions and the size may be omitted in case the @ref UA_DICT_FLAG_OMITTED
 * flag is set.
 * The type id must always be set.
 */
struct ua_type_table_base_fcts {
    uint16_t  type_id;
    uint16_t  size;
    compare_t compare;
    copy_t    copy;
    clear_t   clear;
};

/**
 * Encode and decode functions for builtin types.
 * These functions must be set for each type, in case the @ref UA_DICT_FLAG_OMITTED
 * flag is set these functions receive NULL instead of a pointer to the value. Then
 * the encode function must encode a dummy value and the decode function must skip
 * over the encoded value and discard the result.
 */
struct ua_type_table_encoder_fcts {
    encode_t encode;
    decode_t decode;
};

/**
 * Possible sortings for custom type tables to be registered.
 * Based on the sorting the lookup function is chosen.
 */
enum ua_type_table_sorting {
    UA_TYPE_TABLE_SORTING_NONE, /**< The table is not sorted at all. */
    UA_TYPE_TABLE_SORTING_ASC,  /**< The type ids and encoding ids are each sorted numerical ascending. */
    UA_TYPE_TABLE_SORTING_IDX_TABLE /**< The table is sorted ascending by type id and an additional index table is used for lookup by encoding id. */
};

/**
 * Index table for a type table to enable an efficient lookup using
 * the encoding id.
 */
struct ua_type_table_idx_table {
    uint32_t encoding_id; /**< Encoding id of a structure */
    uint32_t idx;         /**< Index in the structure table with this encoding */
};

/**
 * Struct for one entry in the type table.
 * There must be an entry for each namespace with custom structures.
 */
struct ua_type_table {
    uint16_t ns;
    ua_dict_type_t num_fcts;
    ua_dict_type_t num_types;
    ua_dict_type_t max_types;
    uint8_t sorting;
    bool is_const;
    const struct ua_type_table_base_fcts *base_fcts;
    const struct ua_type_table_encoder_fcts *encoder_fcts;
    const struct ua_dict_structure *types;
    const void *idx; /* some kind of index, depending on the 'sorting' */
};

/** Array with namespace zero builtin base functions (copy, compare, clear) */
extern const struct ua_type_table_base_fcts g_base_fcts_ns0[UA_TYPE_TABLE_NUM_NS0_FCTS];

/** Array with namespace zero structures */
extern const struct ua_dict_structure g_type_table_ns0[UA_TYPE_NUM_VALUES - countof(g_base_fcts_ns0)];
/** Index table for @ref g_type_table_ns0 */
extern const struct ua_type_table_idx_table g_type_table_ns0_idx_table[UA_TYPE_NUM_VALUES - countof(g_base_fcts_ns0)];

const struct ua_type_table *ua_type_table_find(uint16_t ns);

int ua_type_table_register_table(uint16_t nsidx, struct ua_dict_structure *structures, uint16_t num_structures, uint16_t max_structures, enum ua_type_table_sorting sorting);
int ua_type_table_register_const_table(uint16_t nsidx, const struct ua_dict_structure *structures, uint16_t num_structures, enum ua_type_table_sorting sorting);
int ua_type_table_register_const_table_with_idx_table(uint16_t nsidx, const struct ua_dict_structure *structures, uint16_t num_structures, const struct ua_type_table_idx_table *idx_table, enum ua_type_table_sorting sorting);
int ua_type_table_register_structure(uint16_t nsidx, const struct ua_dict_structure *structure);

int ua_type_table_init(const struct ua_type_table_encoder_fcts *ns0_encoder_fcts);
void ua_type_table_clear(void);

const struct ua_dict_structure *ua_type_table_lookup_local_type(uint16_t local_ns, uint32_t local_type);
const struct ua_dict_structure *ua_type_table_lookup_binary_encoding(const struct ua_nodeid *binary_encoding_id, uint32_t *local_type);
const struct ua_dict_structure *ua_type_table_lookup_type_id(const struct ua_nodeid *type_id, uint32_t *local_type);

/** @} */

UA_END_EXTERN_C

#endif /* UABASE_TYPE_TABLE_H */
