/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef VALUERANK_H_9DMYWOBJ
#define VALUERANK_H_9DMYWOBJ

/*============================================================================
 * Constants defined for the ValueRank attribute.
 *===========================================================================*/

/**
 * @defgroup ua_base_valuerank ua_valuerank
 * @ingroup ua_base
 * Possible values for the valuerank attribute.
 * @{
 */

/** The variable may be a scalar or a one dimensional array. */
#define UA_VALUERANK_SCALARORONEDIMENSION -3

/** The variable may be a scalar or an array of any dimension. */
#define UA_VALUERANK_ANY -2

/** The variable is always a scalar. */
#define UA_VALUERANK_SCALAR -1

/** The variable is always an array with one or more dimensions. */
#define UA_VALUERANK_ONEORMOREDIMENSIONS 0

/** The variable is always a one dimensional array. */
#define UA_VALUERANK_ONEDIMENSION 1

/** The variable is always a two dimensional array. */
#define UA_VALUERANK_TWODIMENSIONS 2

/** @} */

#endif /* end of include guard: VALUERANK_H_9DMYWOBJ */

