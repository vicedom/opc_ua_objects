/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_INDEXRANGE_H_
#define _UABASE_INDEXRANGE_H_

#include <uabase/statuscode.h>
#include <uabase/string.h>

UA_BEGIN_EXTERN_C

/**
 * @ingroup ua_base
 */
struct ua_indexrange {
    uint32_t first; /**< Index of the first element */
    uint32_t last;  /**< Index of the last element or zero if only one element is specified */
};

ua_statuscode ua_indexrange_parse(
    const struct ua_string *index_range,
    struct ua_indexrange *indexrange_array,
    unsigned int *max_dimensions);

UA_END_EXTERN_C

#endif /* _UABASE_INDEXRANGE_H_ */
