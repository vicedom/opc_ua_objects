/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_NODEID_H_
#define _UABASE_NODEID_H_

#include <uabase/base_config.h>
#include <stdint.h>
#include <stdlib.h> /* for size_t */
#include <stdbool.h>
#include <platform/platform.h>

UA_BEGIN_EXTERN_C

struct ua_guid;
struct ua_string;
struct ua_bytestring;

/**
 * @defgroup ua_base_nodeid ua_nodeid
 * @ingroup ua_base
 *
 * @brief Identifier for UA nodes.
 *
 * Identifier for a unique node in the addressspace of an OPC UA Server.
 * The nodeid consists of the index for the namespace it belongs to and the actual
 * identifier. The identifier can be one of the four types defined by the
 * @ref ua_identifiertype.
 *
 * A Server shall persist the NodeId of a Node, that is, it is not
 * allowed to generate new NodeIds when rebooting. The server is not
 * allowed to change the namespace index for a namespace URI as long as
 * the server is not restarted, but new namespaces can be added.  The
 * namespace index for a namespace URI may change after a server
 * restart.
 *
 * Clients must be prepared for changes of the namespace index in a
 * NodeId and must persist their own namespace table together with the
 * stored NodeIds for a server.
 *
 * The following three elements identify a Node within a Server:
 *
 * Name           | Type   | Description
 * ---------------|--------|------------------------------------------------------------------
 * namespaceIndex | UInt16 | The index for a namespace URI used in an OPC UA server
 * identifierType | enum   | The format and data type of the identifier
 * identifier     | *      | The identifier for a node in the address space of an OPC UA server
 *
 * <b>Namespace Index</b>
 *
 * The namespace is a URI that identifies the naming authority
 * responsible for assigning the identifier element of the
 * NodeId. Naming authorities include the local server, the underlying
 * system, standards bodies and consortia. It is expected that most
 * nodes will use the URI of the server or of the underlying system.
 *
 * Using a namespace URI allows multiple OPC UA servers attached to
 * the same underlying system to use the same identifier to identify
 * the same object. This enables clients that connect to those Servers
 * to recognise Objects that they have in common.
 *
 * Namespace URIs are identified by numeric values in OPC UA services
 * to permit more efficient transfer and processing (e.g. table
 * lookups). The numeric values used to identify namespaces correspond
 * to the index into the NamespaceArray. The NamespaceArray is a
 * variable that is part of the Server object in the address space.
 *
 * The URI for the OPC UA namespace is
 * “http://opcfoundation.org/UA/”. Its corresponding index in the
 * namespace table is 0.
 *
 * The Namespace URI is case sensitive.
 *
 * <b>Identifier Type</b>
 *
 * The IdentifierType element identifies the type of the NodeId, its
 * format and its scope. The following IdentifierTypes are available:
 *
 * Value                 | Description
 * ----------------------|--------------------------------------------
 * UA_IDENTIFIER_NUMERIC | Numeric value
 * UA_IDENTIFIER_STRING  | String value
 * UA_IDENTIFIER_GUID    | Globally Unique Identifier
 * UA_IDENTIFIER_OPAQUE  | Namespace specific format in a ByteString
 *
 * <b>Identifier value</b>
 *
 * The identifier value element is used within the context of the
 * first two elements to identify a node. Its data type and format is
 * defined by the identifierType.
 *
 * Numeric identifiers are typically used for static namespaces or for
 * NodeIds in generated code.  Such identifiers in generated code have
 * typically defines generated which makes them easy to use and read
 * in code.
 *
 * String identifiers are typically used for nodes that are
 * dynamically generated during system configuration. The string is
 * either a path of nodes with a server specific delimiters or a
 * server specific addressing schema contained in the string.
 *
 * String identifiers are case sensitive. That is, clients shall
 * consider them case sensitive.  Servers are allowed to provide
 * alternative NodeIds and using this mechanism severs can handle
 * NodeIds as case insensitive.
 *
 * Normally the scope of NodeIds is the server in which they are
 * defined. For certain types of NodeIds, NodeIds can uniquely
 * identify a Node within a system, or across systems (e.g. GUIDs).
 * System-wide and globally-unique identifiers allow clients to track
 * Nodes, such as work orders, as they move between OPC UA servers as
 * they progress through the system.
 *
 * Opaque identifiers are identifiers that are free-format byte
 * strings that might or might not be human interpretable.
 *
 * Identifier values of IdentifierType STRING are restricted to 4096
 * characters. Identifier values of IdentifierType OPAQUE are
 * restricted to 4096 bytes.
 *
 * A null NodeId has special meaning. For example, many services
 * define special behaviour if a null NodeId is passed as a
 * parameter. A null NodeId always has a NamespaceIndex equal to 0. A
 * node in the address space cannot have a null NodeId.  Each
 * identifier type has a set of identifier values that represent a
 * null NodeId:
 *
 * Identifier Type  | Identifier
 * -----------------|-------------------------------------------------
 * NUMERIC          | 0
 * STRING           | A null or Empty String (“”)
 * GUID             | A Guid initialised with zeros (e.g. 00000000-0000-0000-0000-000000)
 * OPAQUE           | A ByteString with Length=0
 *
 * @{
 */

/**
 * @brief The set of known node identifier types
 */
enum ua_identifiertype {
    UA_IDENTIFIER_NUMERIC = 0, /**< The identifier is a 32bit unsigned integer */
    UA_IDENTIFIER_STRING,      /**< The identifier is a @ref ua_string */
    UA_IDENTIFIER_GUID,        /**< The identifier is a @ref ua_guid */
    UA_IDENTIFIER_OPAQUE       /**< The identifier is a @ref ua_bytestring */
};

/**
 * @brief Structure for an UA Nodeid, see also @ref ua_base_nodeid.
 * @see ua_base_nodeid
 */
struct ua_nodeid {
    union {
        uint32_t              numeric; /**< Numeric value */
        struct ua_string     *string;  /**< String value */
        struct ua_guid       *guid;    /**< Globally Unique Identifier */
        struct ua_bytestring *opaque;  /**< Namespace specific format in a ByteString */
    } identifier;                      /**< The identifier for a Node in the AddressSpace of an OPC UA Server. */
    enum ua_identifiertype type;       /**< The format and data type of the identifier. */
    uint16_t nsindex;                  /**< The index for a namespace URI. */
};

/** Initializes a nodeid struct as numeric null nodeid.
 */
#define UA_NODEID_INITIALIZER { { 0 }, UA_IDENTIFIER_NUMERIC, 0 }

/** Initializes a nodeid struct as numeric nodeid with identifier and namespace index.
 * @param id The numeric identifier.
 * @param ns The namespace index.
 */
#define UA_NODEID_NUMERIC_INITIALIZER(id, ns) { { id }, UA_IDENTIFIER_NUMERIC, ns }

void ua_nodeid_init(struct ua_nodeid *id);
void ua_nodeid_clear(struct ua_nodeid *id);

void ua_nodeid_set_numeric(struct ua_nodeid *id, uint16_t nsindex, uint32_t value);
int ua_nodeid_set_string(struct ua_nodeid *id, uint16_t nsindex, const char *value);
int ua_nodeid_set_guid(struct ua_nodeid *id, uint16_t nsindex, const struct ua_guid *value);
int ua_nodeid_set_bytestring(struct ua_nodeid *id, uint16_t nsindex, const char *value, int32_t len);

int ua_nodeid_attach_string(struct ua_nodeid *id, uint16_t nsindex, char *value);
int ua_nodeid_attach_string_const(struct ua_nodeid *id, uint16_t nsindex, const char *value);
void ua_nodeid_attach_guid(struct ua_nodeid *id, uint16_t nsindex, struct ua_guid *value);
int ua_nodeid_attach_bytestring(struct ua_nodeid *id, uint16_t nsindex, char *value, int32_t len);

int ua_nodeid_compare(const struct ua_nodeid *a, const struct ua_nodeid *b) UA_PURE_FUNCTION;
int ua_nodeid_compare_numeric(const struct ua_nodeid *a, uint16_t nsidx, uint32_t value) UA_PURE_FUNCTION;
int ua_nodeid_compare_string(const struct ua_nodeid *a, uint16_t nsidx, const char *value) UA_PURE_FUNCTION;
int ua_nodeid_copy(struct ua_nodeid *dst, const struct ua_nodeid *src);

bool ua_nodeid_is_null(const struct ua_nodeid *id);

#ifdef ENABLE_TO_STRING
int ua_nodeid_to_string(const struct ua_nodeid *id, struct ua_string *dst);
int ua_nodeid_snprintf(char *dst, size_t size, const struct ua_nodeid *id);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_nodeid_from_string(struct ua_nodeid *id, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

const char *ua_nodeid_printable(const struct ua_nodeid *id);

/** @} */

UA_END_EXTERN_C

#endif /* _UABASE_NODEID_H_ */
