/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef __UABASE_BASETYPES_H__
#define __UABASE_BASETYPES_H__

/* convenience include file to get all base datatypes */

#include <uabase/accesslevel.h>
#include <uabase/attributeid.h>
#include <uabase/buffer.h>
#include <uabase/bytestring.h>
#include <uabase/datavalue.h>
#include <uabase/datetime.h>
#include <uabase/diagnosticinfo.h>
#include <uabase/eventnotifier.h>
#include <uabase/expandednodeid.h>
#include <uabase/extensionobject.h>
#include <uabase/guid.h>
#include <uabase/identifiers.h>
#include <uabase/indexrange.h>
#include <uabase/localizedtext.h>
#include <uabase/minimumsamplinginterval.h>
#include <uabase/nodeid.h>
#include <uabase/qualifiedname.h>
#include <uabase/requestheader.h>
#include <uabase/responseheader.h>
#include <uabase/statuscode.h>
#include <uabase/statuscodes.h>
#include <uabase/string.h>
#include <uabase/valuerank.h>
#include <uabase/variant.h>
#include <uabase/xmlelement.h>

#endif /* __UABASE_BASETYPES_H__ */

