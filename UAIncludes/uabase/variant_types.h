/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_VARIANT_TYPES_H_
#define _UABASE_VARIANT_TYPES_H_


#include <stdint.h>
#include <stdbool.h>
#include <uabase/datetime.h>
#include <uabase/statuscode.h>
#include <uabase/string.h>
#include <uabase/bytestring.h>

/* forward declarations */
struct ua_guid;
struct ua_nodeid;
struct ua_datavalue;

/**
 * @addtogroup ua_base_variant
 * @{
 */

/**
 * @internal
 * Properties for a variant of specific type.
 * May change at any time and is for SDK internal use only.
 */
struct ua_variant_base_properties {
    bool ptr_type;
    size_t size;
    compare_t compare;
    copy_t copy;
    clear_t clear;
};

/**
 * @brief Type identifier for a variant.
 */
enum ua_variant_type {
    UA_VT_NULL = 0,
    UA_VT_BOOLEAN = 1,
    UA_VT_SBYTE = 2,
    UA_VT_BYTE = 3,
    UA_VT_INT16 = 4,
    UA_VT_UINT16 = 5,
    UA_VT_INT32 = 6,
    UA_VT_UINT32 = 7,
    UA_VT_INT64 = 8,
    UA_VT_UINT64 = 9,
    UA_VT_FLOAT = 10,
    UA_VT_DOUBLE = 11,
    UA_VT_STRING = 12,
    UA_VT_DATETIME = 13,
    UA_VT_GUID = 14,
    UA_VT_BYTESTRING = 15,
    UA_VT_XMLELEMENT = 16,
    UA_VT_NODEID = 17,
    UA_VT_EXPANDEDNODEID = 18,
    UA_VT_STATUSCODE = 19,
    UA_VT_QUALIFIEDNAME = 20,
    UA_VT_LOCALIZEDTEXT = 21,
    UA_VT_EXTENSIONOBJECT = 22,
    UA_VT_DATAVALUE = 23,
    UA_VT_VARIANT = 24,
    UA_VT_DIAGNOSTICINFO = 25,
    UA_VT_ENCODED_BINARY = 26,
    UA_VT_IS_MATRIX = 0xc0,
    UA_VT_IS_ARRAY = 0x80,
    UA_VT_ARRAYMASK = 0xc0,
    UA_VT_TYPEMASK = 0x3f
};

struct ua_variant_array {
    int len;
    union {
        bool *b;
        uint8_t *ui8;
        int8_t *i8;
        uint16_t *ui16;
        int16_t *i16;
        uint32_t *ui32;
        int32_t *i32;
        uint64_t *ui64;
        int64_t *i64;
        float *f;
        double *d;
        struct ua_string *s;
        ua_datetime *dt;
        struct ua_guid *g;
        struct ua_bytestring *bs;
        struct ua_xmlelement *xml;
        struct ua_nodeid *nodeid;
        struct ua_expandednodeid *enodeid;
        ua_statuscode *status;
        struct ua_qualifiedname *qn;
        struct ua_localizedtext *lt;
        struct ua_extensionobject *eo;
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
        struct ua_datavalue *dv;
#endif
        struct ua_variant *var;
    } value;
};

struct ua_variant_matrix {
    /* the array must be allocated before usage! */
    struct ua_variant_array *array;
    /* the first element of this array is the number of following elements */
    int32_t *dim;
};

/**
 * @brief Structure for an UA Variant, see also @ref ua_base_variant.
 * @see ua_base_variant
 */
struct ua_variant {
    uint8_t type; /**< Type of the variant @ref ua_variant_type. */
    /* here we have some padding bytes, that we cannot avoid */
    union {
        bool b;
        uint8_t ui8;
        int8_t i8;
        uint16_t ui16;
        int16_t i16;
        uint32_t ui32;
        int32_t i32;
        uint64_t ui64;
        int64_t i64;
        float f;
        double d;
        struct ua_string s;
        ua_datetime dt;
        struct ua_guid *g;
        struct ua_bytestring bs;
        struct ua_xmlelement *xml;
        struct ua_nodeid *nodeid;
        struct ua_expandednodeid *enodeid;
        ua_statuscode status;
        struct ua_qualifiedname *qn;
        struct ua_localizedtext *lt;
        struct ua_extensionobject *eo;
#ifdef UA_SUPPORT_DATAVALUE_IN_VARIANT
        struct ua_datavalue *dv;
#endif
        struct ua_variant_array array;
        struct ua_variant_matrix matrix;
    } value;
};

/** @} */

#endif /* inlcude guard */
