/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_STATUSCODE_H_
#define _UABASE_STATUSCODE_H_

#include "base.h"
#include <uabase/base_config.h>
#include <platform/platform.h>
#include <stdint.h>
#include <stdbool.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup ua_base_statuscode ua_statuscode
 * A numerical value that is used to report the outcome of an operation
 * performed by an OPC UA Server.
 *
 * This code may have associated diagnostic information that describes
 * the status in more detail; however, the code by itself is intended to
 * provide Client applications with enough information to make decisions
 * on how to process the results of an OPC UA Service.
 *
 * The StatusCode is a 32-bit unsigned integer. The top 16 bits represent
 * the numeric value of the code that shall be used for detecting
 * specific errors or conditions. The bottom 16 bits are bit flags that
 * contain additional information but do not affect the meaning of the
 * StatusCode.
 *
 * All OPC UA Clients shall always check the StatusCode associated with a
 * result before using it.  Results that have an uncertain/warning status
 * associated with them shall be used with care since these results might
 * not be valid in all situations. Results with a bad/failed status shall
 * never be used.
 *
 * OPC UA Servers should return good/success StatusCodes if the operation
 * completed normally and the result is always valid. Different
 * StatusCode values can provide additional information to the Client.
 *
 * OPC UA Servers should use uncertain/warning StatusCodes if they could
 * not complete the operation in the manner requested by the Client,
 * however, the operation did not fail entirely.
 *
 * The list of StatusCodes is managed by OPC UA. The complete list of
 * StatusCodes is defined in Part 6 of the OPC UA Specification. Servers
 * shall not define their own StatusCodes. OPC UA companion working
 * groups may request additional StatusCodes from the OPC Foundation to
 * be added to the list in Part 6.
 *
 * @ingroup ua_base
 * @{
 */

#include <uabase/statuscodes.h>

#define UA_SCGOOD 0x00000000        /**< Generic good status code. */
#define UA_SCUNCERTAIN 0x40000000   /**< Generic uncertain status code. */
#define UA_SEVERITY_MASK 0xC0000000 /**< for marking out the severity */
#define UA_SUBCODE_MASK  0x0FFF0000 /**< for marking out the sub code */

/**
 * @brief A numerical value indicating outcome of an UA server
 * operation, possible values are listed in @ref ua_statuscodes.
 */
typedef uint32_t ua_statuscode;

/**
 * @brief Initialize statuscode with good.
 */
static inline void ua_statuscode_init(ua_statuscode *dt)
{
    *dt = 0;
}

/**
 * @brief Clear statuscode and write good.
 */
static inline void ua_statuscode_clear(ua_statuscode *dt)
{
    *dt = 0;
}

/**
 * @brief Compare two statuscodes.
 * @return Zero if both statuscodes are the same.
 */
static inline int ua_statuscode_compare(const ua_statuscode *a, const ua_statuscode *b)
{
    return ua_uint32_compare(a, b);
}

/**
 * @brief Copy statuscode from \c src to \c dst.
 * @return Zero on success.
 */
static inline int ua_statuscode_copy(ua_statuscode *dst, const ua_statuscode *src)
{
    *dst = *src;
    return 0;
}

/** Test if a statuscode is good */
static inline bool ua_statuscode_is_good(ua_statuscode status)
{
    return ((status & UA_SEVERITY_MASK) == UA_SCGOOD);
}

/** Test if a statuscode is uncertain */
static inline bool ua_statuscode_is_uncertain(ua_statuscode status)
{
    return ((status & UA_SEVERITY_MASK) == UA_SCUNCERTAIN);
}

/** Test if a statuscode is bad */
static inline bool ua_statuscode_is_bad(ua_statuscode status)
{
    return ((status & UA_SCBAD) == UA_SCBAD);
}

/** Get severity and sub code portion of a status code */
static inline ua_statuscode ua_statuscode_get_code(ua_statuscode status)
{
    return ((status) & (UA_SEVERITY_MASK | UA_SUBCODE_MASK));
}

/** Get severity and sub code portion of a status code as unit16 value.
 * This is required to store in statuscode in UA PubSub messages.
 * @see ua_statuscode_from_uint16_code
 */
static inline uint16_t ua_statuscode_get_code_to_uint16(ua_statuscode status)
{
    return ((uint16_t)((ua_statuscode_get_code(status)) >> 16));
}

/** Creates a 32bit status code with severity from the 16bit sub code.
 * This is the reverse operation of \ref ua_statuscode_get_code_to_uint16.
 */
static inline ua_statuscode ua_statuscode_from_uint16_code(uint16_t code)
{
    return (((ua_statuscode)code) << 16);
}

ua_statuscode ua_statuscode_from_errorcode(int error_code);

/** @}*/

UA_END_EXTERN_C

#endif /* _UABASE_DATETIME_H_ */

