/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UABASE_QUALIFIEDNAME_H_
#define _UABASE_QUALIFIEDNAME_H_

#include <uabase/string.h>
#include <stdlib.h> /* for size_t */
#include <platform/platform.h>

UA_BEGIN_EXTERN_C

/**
 * @ingroup ua_base
 * @brief Contains a qualified name.
 *
 * It is, for example, used as BrowseName. The name part of the
 * QualifiedName is restricted to 512 characters.
 */
struct ua_qualifiedname {
    struct ua_string name; /**< The text portion of the QualifiedName. */
    uint16_t nsindex;      /**< Index that identifies the namespace that
                            * defines the name.
                            *
                            * This index is the index of that namespace in the
                            * local Server’s NamespaceArray.
                            *
                            * The Client may read the NamespaceArray Variable
                            * to access the string value of the namespace.*/
};

/**
 * Initializes ua_qualifiedname with namespace index zero and null name.
 * @relates ua_qualifiedname
 */
#define UA_QUALIFIEDNAME_INITIALIZER { UA_STRING_INITIALIZER, 0 }

void ua_qualifiedname_init(struct ua_qualifiedname *qn);
void ua_qualifiedname_clear(struct ua_qualifiedname *qn);

bool ua_qualifiedname_is_null(const struct ua_qualifiedname *qn);

int ua_qualifiedname_set(struct ua_qualifiedname *qn, uint16_t nsindex, const char *name);
void ua_qualifiedname_attach_const(struct ua_qualifiedname *qn, uint16_t nsindex, const char *name);
void ua_qualifiedname_attachn_const(struct ua_qualifiedname *qn, uint16_t nsindex, const char *name, size_t len);

int ua_qualifiedname_compare(const struct ua_qualifiedname *a, const struct ua_qualifiedname *b) UA_PURE_FUNCTION;
int ua_qualifiedname_copy(struct ua_qualifiedname *dst, const struct ua_qualifiedname *src);

#ifdef ENABLE_TO_STRING
int ua_qualifiedname_to_string(const struct ua_qualifiedname *qn, struct ua_string *dst);
int ua_qualifiedname_snprintf(char *dst, size_t size, const struct ua_qualifiedname *qn);
#endif /* ENABLE_TO_STRING */
#ifdef ENABLE_FROM_STRING
int ua_qualifiedname_from_string(struct ua_qualifiedname *qn, const struct ua_string *src);
#endif /* ENABLE_FROM_STRING */

UA_END_EXTERN_C

#endif /* _UABASE_QUALIFIEDNAME_H_ */
