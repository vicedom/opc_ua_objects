/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UA_CRYPTO_H_
#define _UA_CRYPTO_H_

/**
 * @defgroup crypto crypto
 * @{
 */

#include <common/errors.h>
#include <crypto/key.h>
#include <stdlib.h> /* for size_t */
#include <stdint.h>

/** Null operation for all algorithms. */
#define crypto_alg_null 0

/** Enumeration of symmetric encryption algorithms. */
enum crypto_sym_alg {
    crypto_sym_alg_invalid          = 0, /**< invalid value */
    crypto_sym_alg_aes_cbc          = 1, /**< AES encryption with cipher block chaining */
};

/** Enumeration of asymmetric encryption algorithms. */
enum crypto_asym_alg {
    crypto_asym_alg_invalid         = 0, /**< invalid enumeration value */
    crypto_asym_alg_rsa             = 1, /**< RSA encryption with SHA1 */
    crypto_asym_alg_rsa_sha256      = 2  /**< RSA encryption with SHA256 */
};

/** Enumeration of message signing algorithms (algorithm, hash and scheme combined). */
enum crypto_sign_alg {
    crypto_sign_alg_invalid         = 0, /**< invalid enumeration value */
    crypto_sign_alg_hmac_sha1       = 1, /**< hash-base message authentication using 160 bit SHA1 */
    crypto_sign_alg_hmac_sha224     = 2, /**< hash-base message authentication using 224 bit SHA2 */
    crypto_sign_alg_hmac_sha256     = 3, /**< hash-base message authentication using 256 bit SHA2 */
    crypto_sign_alg_hmac_sha384     = 4, /**< hash-base message authentication using 384 bit SHA2 */
    crypto_sign_alg_hmac_sha512     = 5, /**< hash-base message authentication using 512 bit SHA2 */
    crypto_sign_alg_rsa_sha1        = 6, /**< RSA based signature using 160 bit SHA1 */
    crypto_sign_alg_rsa_sha256      = 7, /**< RSA based signature using 256 bit SHA2 */
    crypto_sign_alg_rsa_sha256_pss  = 8  /**< RSA based signature using 256 bit SHA2 and PSS padding */
};

/** Destination buffer lengths for hash algorithms. */
#define CRYPTO_SHA1_LEN     20
#define CRYPTO_SHA224_LEN   28
#define CRYPTO_SHA256_LEN   32
#define CRYPTO_SHA384_LEN   48
#define CRYPTO_SHA512_LEN   64

/** Enumeration of hash algorithms. */
enum crypto_hash_alg {
    crypto_hash_alg_invalid         = 0, /**< invalid enumeration value */
    crypto_hash_alg_sha1            = 1, /**< 160 bit secure hash algorithm (SHA1) */
    crypto_hash_alg_sha224          = 2, /**< 224 bit secure hash algorithm (SHA2) */
    crypto_hash_alg_sha256          = 3, /**< 256 bit secure hash algorithm (SHA2)*/
    crypto_hash_alg_sha384          = 4, /**< 384 bit secure hash algorithm (SHA2)*/
    crypto_hash_alg_sha512          = 5, /**< 512 bit secure hash algorithm (SHA2)*/
};

/** Enumeration of padding algorithms. */
enum crypto_pad_alg {
    crypto_pad_invalid              = 0, /**< invalid enumeration value */
    crypto_pad_pkcs1_v15            = 1, /**< PKCS1 padding v1.5 */
    crypto_pad_pkcs1_oaep           = 2, /**< PKCS1 OAEP */
};

/** Size of AES cipher and plain text blocks in bytes. */
#define CRYPTO_AES_BLOCK_SIZE_BYTES 16

#ifdef HAVE_CRYPTO

/**
 * En-/decrypt buffer content using the requested algorithm.
 * @param alg_id Identifier of the crypto algorithm.
 * @param key    Encrytpion key.
 * @param iv     Initialization vector of size 16 bytes. (updated after use)
 * @param len    Length in bytes of the text to encrypt/decrypt (multiple of 16).
 * @param in     Plain text of "len" bytes".
 * @param out    Cipher text of "len" bytes".
 * @return Error Code
 */
int crypt_sym_encrypt(
    enum crypto_sym_alg  alg_id,
    struct crypto_key   *key,
    unsigned char       *iv,
    size_t               len,
    const unsigned char *in,
    unsigned char       *out
);

/**
 * En-/decrypt buffer content using the requested algorithm.
 * @param alg_id Identifier of the crypto algorithm.
 * @param key    Encrytpion key.
 * @param iv     Initialization vector of size 16 bytes. (updated after use)
 * @param len    Length in bytes of the text to encrypt/decrypt (multiple of 16).
 * @param out    Cipher text of "len" bytes".
 * @param in     Plain text of "len" bytes".
 * @return Error Code
 */
int crypt_sym_decrypt(
    enum crypto_sym_alg  alg_id,
    struct crypto_key   *key,
    unsigned char       *iv,
    size_t               len,
    const unsigned char *in,
    unsigned char       *out
);

/**
 * Sign the given data using HMAC and the given algorithm.
 * @param alg_id  HMAC algorithm to use.
 * @param signkey Key for hashing.
 * @param data    Data to sign.
 * @param datalen Length of the data to sign.
 * @param mac     Destination buffer of size maclen.
 * @param maclen  Size of the destination buffer.
 * @return Error Code
 */
int crypt_sym_sign(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *signkey,
    const unsigned char *data,
    size_t               datalen,
    unsigned char       *mac,
    size_t               maclen);

/**
 * Verify the correctness of the given signature.
 * @param alg_id  HMAC algorithm to use.
 * @param signkey Key for hashing.
 * @param data    Data to sign.
 * @param datalen Length of the data to sign.
 * @param mac     Buffer containing the signature to verify.
 * @param maclen  Length of the signature data to be verified.
 * @return Error Code; UA_EBADSIGNATURE if signature cannot be verified.
 */
int crypt_sym_verify(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *signkey,
    const unsigned char *data,
    size_t               datalen,
    unsigned char       *mac,
    size_t               maclen);

/**
 * Encrypt buffer content using the requested algorithm.
 * The plain text can be of any size, independently of alg_id and pad_id.
 * ctext can contain more than one block.
 * @param alg_id Identifier of the encryption algorithm.
 * @param pad_id Identifier of the padding algorithm.
 * @param pubkey The public key.
 * @param ctext  The plain text.
 * @param plen    Length of the plain text.
 * @param ptext  The cipher text.
 * @param clen   Length of the cipher text buffer. Used space on return.
 * @return Error Code
 */
int crypt_encrypt(
    enum crypto_asym_alg    alg_id,
    enum crypto_pad_alg     pad_id,
    struct crypto_key      *pubkey,
    const unsigned char    *ptext,
    size_t                  plen,
    unsigned char          *ctext,
    size_t                 *clen
);

/**
 * Decrypt buffer content using the requested algorithm.
 * @param alg_id  Identifier of the requested algorithm.
 * @param pad_id  Identifier of the padding algorithm.
 * @param privkey The private key.
 * @param ctext   The cipher text. Can contain more than one encrypted block.
 * @param clen    Length of the cipher text. Must be a multiple of the key length.
 * @param ptext   The plain text.
 * @param plen    Length of the plain text buffer. Used length on return.
 * @return Error Code
 */
int crypt_decrypt(
    enum crypto_asym_alg    alg_id,
    enum crypto_pad_alg     pad_id,
    struct crypto_key      *privkey,
    const unsigned char    *ctext,
    size_t                  clen,
    unsigned char          *ptext,
    size_t                 *plen
);

/**
 * Create cryptographic signature (e.g. RSA SHA1).
 * @param alg_id  Identifier of the requested algorithm.
 * @param privkey The private signing key.
 * @param data    Data to sign.
 * @param datalen Length in bytes of the data to sign.
 * @param sig     The resulting signature.
 * @param siglen  Length of the buffer to store the signature.
 * @return Error Code
 */
int crypt_sign(
    enum crypto_sign_alg  alg_id,
    struct crypto_key    *privkey,
    const unsigned char  *data,
    size_t                datalen,
    unsigned char        *sig,
    size_t                siglen
);

    /**
 * Verify cryptographic signature (e.g. RSA SHA1).
 * @param alg_id  Identifier of the requested algorithm.
 * @param verkey  Key containing the public key component.
 * @param data    Signed data.
 * @param len     Length in bytes of the signed data.
 * @param sig     The message digest.
 * @param siglen  Length of the buffer with the message digest.
 * @return Error Code; UA_EBADSIGNATURE if signature cannot be verified.
 */
int crypt_verify(
    enum crypto_sign_alg alg_id,
    struct crypto_key   *verkey,
    const unsigned char *data,
    size_t               len,
    const unsigned char *sig,
    size_t               siglen
);

/**
 * Generate a hash value (e.g. SHA1).
 * @param alg_id Identifier of the requested algorithm.
 * @param data   Data to hash.
 * @param len    Length in bytes of the data to hash.
 * @param md     The resulting message digest.
 * @return Error Code
 */
int crypt_hash(
    enum crypto_hash_alg  alg_id,
    const unsigned char  *data,
    size_t                len,
    unsigned char        *md
);

/**
 * Generate P-SHA hash value.
 * @param alg_id    Identifier of the requested algorithm (only sha1 and sha256 mandatory).
 * @param secret    Shared secret data.
 * @param secretlen Length in bytes of the shared secret.
 * @param seed      Label and seed data.
 * @param seedlen   Length in bytes of the label and seed. The max. supported size is 32 bytes.
 * @param out       Buffer of size outlen for storing the created hashes.
 * @param outlen    Length in bytes of the hash value to create. Must be a multiple of the digest length.
 * @return Error Code
 */
int crypt_psha(
    enum crypto_hash_alg  alg_id,
    const unsigned char  *secret,
    size_t                secretlen,
    const unsigned char  *seed,
    size_t                seedlen,
    unsigned char        *out,
    size_t                outlen
);

/**
 * Fill the given buffer with random bytes.
 * @param data  Buffer to fill with random bytes.
 * @param len   Size of the buffer in bytes.
 * @return Error Code
 */
int crypt_random(
    unsigned char *data,
    size_t         len
);

/**
 * Initializes the RNG with the provided seed data.
 * @param seed    Data for seeding the random number generator.
 * @param seedlen Length of seed in bytes.
 * @return Error Code
 */
int crypt_random_seed(
    const unsigned char *seed,
    size_t               seedlen);

/**
 * Initialize the crypto library.
 * Must be called before any other call to this API.
 * @param seed    Data for seeding the entropy generator.
 * @param seedlen Length of seed.
 * @return Error Code
 */
int crypt_init(
    const unsigned char *seed,
    size_t               seedlen);

/**
 * Clean up resources of the crypto library.
 */
void crypt_clear(void);

#endif /* HAVE_CRYPTO */

/** @} */

#endif /* _UA_CRYPTO_H_ */
