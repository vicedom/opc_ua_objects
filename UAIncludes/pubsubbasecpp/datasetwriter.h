/******************************************************************************
** datasetwriter.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __DATASETWRITER_H__
#define __DATASETWRITER_H__

#include "pubsubobject.h"
#include "writerqueueitem.h"
#include "uakeyvaluepair.h"
#include "uauadpdatasetwritermessagedatatype.h"
#include "uajsondatasetwritermessagedatatype.h"
#include <vector>

namespace PubSubBase
{

class WriterGroup;
class PubSubStackQueue;
class PublishedDataSet;
class DataSetWriterCallback;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for DataSetWriters.
*/
class PUBSUBBASE_EXPORT DataSetWriter : public PubSubObject
{
    friend class WriterGroup;
    UA_DISABLE_COPY(DataSetWriter);

protected:
    DataSetWriter();
    DataSetWriter(
        WriterGroup*                 pParent,
        OpcUa_DataSetWriterDataType* pConfigData,
        OpcUa_UInt32                 configIndex,
        OpcUa_Int32                  stackHandle,
        PublishedDataSet*            pPublishedDataSet);
    DataSetWriter(
        WriterGroup*                 pParent,
        OpcUa_DataSetWriterDataType* pConfigData,
        PublishedDataSet*            pPublishedDataSet);
    virtual ~DataSetWriter();

public:
    UaStatus startUp();
    UaStatus shutDown();

    // #################################################
    // DataSetWriter settings
    UaString name() const;
    void setName(const UaString& name);
    bool enabled() const;
    void setEnabled(bool bEnabled);
    OpcUa_UInt16 dataSetWriterId() const;
    void setDataSetWriterId(OpcUa_UInt16 dataSetWriterId);
    OpcUa_DataSetFieldContentMask dataSetFieldContentMask() const;
    UaStatus setDataSetFieldContentMask(OpcUa_DataSetFieldContentMask dataSetFieldContentMask);
    OpcUa_UInt32 keyFrameCount() const;
    void setKeyFrameCount(OpcUa_UInt32 keyFrameCount);
    PublishedDataSet* pPublishedDataSet() const;
    void setPublishedDataSet(PublishedDataSet* pPublishedDataSet);
    UaKeyValuePairs dataSetWriterProperties() const;
    void setDataSetWriterProperties(const UaKeyValuePairs& dataSetWriterProperties);
    // UADP Message Settings
    UaStatus messageSettingsUadp(UaUadpDataSetWriterMessageDataType& uadpMessageSettings) const;
    UaStatus setMessageSettingsUadp(const UaUadpDataSetWriterMessageDataType& uadpMessageSettings);
    // JSON Message Settings
    UaStatus messageSettingsJson(UaJsonDataSetWriterMessageDataType& jsonMessageSettings) const;
    UaStatus setMessageSettingsJson(const UaJsonDataSetWriterMessageDataType& jsonMessageSettings);
    // #################################################

    // #################################################
    // Default DataSetMessage processing through queues
    void connectStackQueue(PubSubStackQueue* pStackQueue);
    OpcUa_PublishedDataItemsDataType* pPublishedDataItems();
    OpcUa_UInt32 getQueueSize() const;
    WriterQueueItem* getQueueItem(OpcUa_UInt32 index);
    // #################################################

    // #################################################
    // Custom DataSetMessage processing through callback
    inline bool messageEncodedByApplication() const { return m_messageEncodedByApplication; }
    inline void setMessageEncodedByApplication(bool messageEncodedByApplication) { m_messageEncodedByApplication = messageEncodedByApplication; }
    void setDataSetWriterCallback(DataSetWriterCallback* pDataSetWriterCallback);
    inline DataSetWriterCallback* pDataSetWriterCallback() const { return m_pDataSetWriterCallback; }
    // #################################################

    UaStatus calculateDataSetSize(OpcUa_UInt16& size, bool& isFixedSize);

    inline WriterGroup* pParent() const { return m_pParent; }
    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_DataSetWriterDataType* pConfigData() const { return m_pConfigData; }

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_DataSetWriter; }

private:
    void setConfigRelation(OpcUa_DataSetWriterDataType* pConfigData, OpcUa_UInt32 configIndex);
    static OpcUa_DataSetWriterDataType* createStructure();
    static UaStatus createDataSetWriter_UADP(
        OpcUa_DataSetWriterDataType* dataSetWriter,
        const UaString&              name,
        bool                         enabled,
        OpcUa_UInt16                 dataSetWriterId,
        const UaString&              dataSetName,
        PubSubHeaderLayout           headerLayout,
        TransportFacet               transportFacet = PubSubObject::TransportFacet_PubSub_UDP_UADP);

private:
    WriterGroup*                 m_pParent;
    OpcUa_DataSetWriterDataType* m_pConfigData;
    OpcUa_UInt32                 m_configIndex;
    PublishedDataSet*            m_pPublishedDataSet;
    DataSetWriterCallback*       m_pDataSetWriterCallback;
    std::vector<WriterQueueItem*> m_queueItems;
    bool                         m_messageEncodedByApplication;
};
/*! @} */

}

#endif // #ifndef __DATASETWRITER_H__

