/******************************************************************************
** publisheddataset.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBLISHEDDATASET_H__
#define __PUBLISHEDDATASET_H__

#include "pubsubobject.h"
#include "uapublishedvariabledatatype.h"
#include "uaversiontime.h"
#include <list>

namespace PubSubBase
{

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for PublishedDataSets.
*/
class PUBSUBBASE_EXPORT PublishedDataSet : public PubSubObject
{
    friend class PubSubConfiguration;
    UA_DISABLE_COPY(PublishedDataSet);

protected:
    PublishedDataSet(
        PubSubResources*                pPubSubResources,
        OpcUa_PublishedDataSetDataType* pConfigData,
        OpcUa_UInt32                    configIndex);
    PublishedDataSet(
        PubSubResources*                pPubSubResources,
        OpcUa_PublishedDataSetDataType* pConfigData);
    virtual ~PublishedDataSet();

public:
    UaStatus startUp();
    UaStatus shutDown();

    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_PublishedDataSetDataType* pConfigData() const { return m_pConfigData; }

    // #################################################
    // PublishedDataSet settings
    UaString name() const;
    void setName(const UaString& name);
    UaStringArray dataSetFolder() const;
    OpcUa_DataSetMetaDataType* pDataSetMetaData() const;
    // Published Variables
    OpcUa_PublishedDataItemsDataType* pPublishedDataItems() const;
    UaStatus publishedVariables(UaPublishedVariableDataTypes& publishedVariables);
    void setPublishedVariable(UaPublishedVariableDataTypes& publishedVariables, bool detach = true);
    // Published Events
    OpcUa_PublishedEventsDataType* pPublishedEvents() const;
    // #################################################

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_PublishedDataSet; }

private:
    static OpcUa_PublishedDataSetDataType* createStructure();
    static UaStatus createPublishedDataSet_DataItems(
        OpcUa_PublishedDataSetDataType* pPublishedDataSet,
        const UaString&      name,
        const UaVersionTime& minorVersion,
        const UaVersionTime& majorVersion,
        const UaStringArray& fieldNames,
        const UaGuidArray&   fieldIds,
        const UaNodeIdArray& publishedVariableNodeIds,
        const UaByteArray&   publishedVariableBuiltInTypes,
        const UaNodeIdArray& publishedVariableDataTypes,
        const UaInt32Array&  publishedVariableValueRanks);

    void setConfigRelation(OpcUa_PublishedDataSetDataType* pConfigData, OpcUa_UInt32 configIndex);

private:
    OpcUa_PublishedDataSetDataType* m_pConfigData;
    OpcUa_UInt32                    m_configIndex;
};
/*! @} */

}

#endif // #ifndef __PUBLISHEDDATASET_H__

