/******************************************************************************
** pubsubconfiguration.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBCONFIGURATION_H__
#define __PUBSUBCONFIGURATION_H__

#include "pubsubobject.h"
#include "pubsubcontrolcallback.h"
#include "pubsubconnection.h"
#include "publisheddataset.h"
#include <list>

namespace PubSubBase
{

class PUBSUBBASE_EXPORT PubSubResources;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for PubSubConfig.
*/
class PUBSUBBASE_EXPORT PubSubConfiguration : public PubSubObject
{
    UA_DISABLE_COPY(PubSubConfiguration);
protected:
    virtual ~PubSubConfiguration();
public:
    PubSubConfiguration();

    UaStatus loadConfiguration(const UaString& sConfigurationFile, bool initializeStack);
    UaStatus loadConfiguration(const UaByteString& bsConfiguration, bool initializeStack);
    UaStatus saveConfiguration(const UaString& sConfigurationFile);
    UaStatus saveConfiguration(UaByteString& bsConfiguration);
    static UaStatus saveConfiguration(const UaString& sConfigurationFile, OpcUa_PubSubConfigurationDataType* pConfiguration, const UaStringArray& namespaces);
    static UaStatus saveConfiguration(UaByteString& bsConfiguration, OpcUa_PubSubConfigurationDataType* pConfiguration, const UaStringArray& namespaces);
    UaStatus applyChanges();
    void clear();

    UaStatus startUp();
    UaStatus shutDown();
    UaStatus startUpPubSubStack();
    UaStatus shutDownPubSubStack();

    // #################################################
    UaStringArray namespaceTable() const;
    void setNamespaceTable(const UaStringArray& namespaces);
    UaStatus mapNamespaceTable(const UaStringArray& newNamespaceTable);
    // PubSubConfiguration settings
    bool enabled() const;
    void setEnabled(bool bEnabled);
    // #################################################

    // #################################################
    // Children handling
    // Connections
    PubSubConnection* addConnection(TransportFacet transportFacet);
    PubSubConnection* addConnection_Datagram(
        const UaString&  name,
        bool             enabled,
        const UaVariant& publisherId,
        const UaString&  address,
        const UaString&  networkInterface,
        const UaString&  discoveryAddress,
        TransportFacet   transportFacet = PubSubObject::TransportFacet_PubSub_UDP_UADP);
    PubSubConnection* addConnection_Broker(
        const UaString&  name,
        bool             enabled,
        const UaVariant& publisherId,
        const UaString&  address,
        TransportFacet   transportFacet);
    OpcUa_UInt32 connectionCount() const;
    PubSubConnection* getConnection(OpcUa_UInt32 index);
    UaStatus removeConnection(PubSubConnection* pConnection);

    // DataSets
    PublishedDataSet* addDataSet(
        const UaString&      name,
        const UaStringArray& fieldNames,
        const UaNodeIdArray& publishedVariableNodeIds,
        const UaByteArray&   publishedVariableBuiltInTypes,
        const UaInt32Array&  publishedVariableValueRanks);
    PublishedDataSet* addDataSet(
        const UaString&      name,
        const UaStringArray& fieldNames,
        const UaNodeIdArray& publishedVariableNodeIds,
        const UaByteArray&   publishedVariableBuiltInTypes,
        const UaNodeIdArray& publishedVariableDataTypes,
        const UaInt32Array&  publishedVariableValueRanks);
    PublishedDataSet* addDataSet(
        const UaString&      name,
        const UaVersionTime& minorVersion,
        const UaVersionTime& majorVersion,
        const UaStringArray& fieldNames,
        const UaGuidArray&   fieldIds,
        const UaNodeIdArray& publishedVariableNodeIds,
        const UaByteArray&   publishedVariableBuiltInTypes,
        const UaNodeIdArray& publishedVariableDataTypes,
        const UaInt32Array&  publishedVariableValueRanks);
    OpcUa_UInt32 dataSetCount() const;
    PublishedDataSet* getDataSet(OpcUa_UInt32 index);
    UaStatus removeDataSet(PublishedDataSet* pDataSet);
    // #################################################

    OpcUa_PubSubConfigurationDataType* pPubSubConfigurationData() const;
    PublishedDataSet* getPublishedDataSet(const UaString& dataSetName) const;

    void setPubSubControlCallback(PubSubControlCallback* pPubSubControlCallback);

    bool isConfigAvailable() const;
    bool isStackInitialized() const;
    bool isStackStarted() const;

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_Configuration; }

private:
    UaStatus updateConfigStructure();
    void loadConfigurationFinish(const UaStatus &ret);

private:
    std::list<PublishedDataSet*> m_publishedDataSets;
    std::list<PubSubConnection*> m_pubsubConnections;
};
/*! @} */

}

#endif // #ifndef __PUBSUBCONFIGURATION_H__

