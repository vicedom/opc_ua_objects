/******************************************************************************
** pubsubnetworkbackendinterface.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBNETWORKBACKENDINTERFACE_H__
#define __PUBSUBNETWORKBACKENDINTERFACE_H__

#include "pubsubbase.h"
#include "writergroup.h"
#include "userdatabase.h"
#include "uapubsub_api.h"
#include "uapubsub_backend.h"

namespace PubSubBase
{

/** Base interface for a PubSub network back-end
*/
class PUBSUBBASE_EXPORT PubSubNetworkBackendInterface
{
    UA_DISABLE_COPY(PubSubNetworkBackendInterface);
public:
    PubSubNetworkBackendInterface() {}
    virtual ~PubSubNetworkBackendInterface() {}

    /** PubSub transport facet reflecting the defined TransportProfileUris */
    enum PubSubNetworkBackendInterfaceType
    {
        PubSubNetworkBackendInterfaceType_UDP,
        PubSubNetworkBackendInterfaceType_ETH,
        PubSubNetworkBackendInterfaceType_MQTT,
        PubSubNetworkBackendInterfaceType_AMQP,
        PubSubNetworkBackendInterfaceType_SDK,
        PubSubNetworkBackendInterfaceType_User
    };

    virtual PubSubNetworkBackendInterfaceType interfaceType() const = 0;
    virtual pubsub_backend* getNetworkBackend() const = 0;
};

/** SDK interface for a PubSub network back-end
*/
class PUBSUBBASE_EXPORT PubSubNetworkBackendInterfaceSdk : public PubSubNetworkBackendInterface
{
public:
    PubSubNetworkBackendInterfaceSdk() {}
    virtual ~PubSubNetworkBackendInterfaceSdk() {}

    /** Start up the network backend */
    virtual int startUp() = 0;

    /** Shut down the network backend */
    virtual int shutDown() = 0;

    /** Initializes the send context for a WriterGroup
    *
    * This call can be used to setup the related connection if necessary, to prepare the user data
    * and other resources in the send context for further calls related to the send context
    */
    virtual int initSend(
        WriterGroup* pWriterGroup,  //!< [in] The writer group management object.
        pubsub_backend_context *ctx //!< [in/out] The send context for the WriterGroup.
    ) = 0;

    /** Initializes the receive context for a PubSubConnection
    *
    * This call can be used to setup the related connection if necessary, to prepare the user data
    * and other resources in the receive context for further calls related to the receive context
    */
    virtual int initReceive(
        PubSubConnection* pConnection, //!< [in] The connection management object.
        pubsub_backend_context *ctx    //!< [in/out] The receive context for the PubSubConnection.
    ) = 0;

    /** Clear resources for the context
    */
    virtual int clear(
        pubsub_backend_context *ctx //!< [in/out] The context
    ) = 0;

    /** Allocate the buffer if the buffer is managed by the backend and reused.
    */
    virtual int alloc(
        pubsub_backend_context *ctx //!< [in/out] The context
    ) = 0;

    /** Allocate the buffer if the buffer is managed by the backend and reused.
    */
    virtual int free(
        pubsub_backend_context *ctx //!< [in/out] The context
    ) = 0;

    /** Send a network message for a WriterGroup
    */
    virtual int send(
        pubsub_backend_context *tx, //!< [in] The send context for the WriterGroup.
        unsigned int size           //!< [in] The size of the data to send.
    ) = 0;

    /** Begin receive of a network message for a PubSubConnection
    */
    virtual void beginReceive(
        pubsub_backend_context *rx //!< [in/out] The receive context for the PubSubConnection.
    ) = 0;
};

}

#endif // #ifndef __PUBSUBNETWORKBACKENDINTERFACE_H__

