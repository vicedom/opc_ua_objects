/******************************************************************************
** pubsubcontrolcallback.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __PUBSUBCONTROLCALLBACK_H__
#define __PUBSUBCONTROLCALLBACK_H__

#include "pubsubbase.h"
#include "writergroup.h"
#include "datasetwriter.h"
#include "datasetreader.h"

namespace PubSubBase
{

/** Class PubSubControlCallback.
*/
class PUBSUBBASE_EXPORT PubSubControlCallback
{
    UA_DISABLE_COPY(PubSubControlCallback);
public:
    PubSubControlCallback() {}
    virtual ~PubSubControlCallback() {}

    /** Callback to inform other modules about creation of new PubSubConnection
    */
    virtual void newConnection(
        PubSubConnection* pConnection //!< [in] The connection management object.
    ) = 0;

    /** Callback to inform other modules about creation of new WriterGroup
    */
    virtual void newWriterGroup(
        WriterGroup* pGroup, //!< [in] The writer group management object.
        bool&        timingHandledByApplication /**< [out] Flag used to indicate that the application is responsible for
                                                           handling of cyclic sampling and publishing. */
    ) = 0;

    /** Callback to inform other modules about creation of new DataSetWriter
    *  The application can handle the encoding of the DataSetMessage by setting the
    *  messageEncodedByApplication to true.
    */
    virtual void newDataSetWriter(
        DataSetWriter* pDataSetWriter, //!< [in] The DataSetWriter management object.
        bool&          messageEncodedByApplication /**< [out] Flag used to indicate that the application is responsible for encoding
                                                       the fields of the DataSetMessage (true) or encoding should be done by the PubSub stack (false).
                                                       In the case of true, the encoding is started with a callback to the application. */
    ) = 0;

    /** Callback to inform application about creation of new reader group
    */
    virtual void newReaderGroup(
        PubSubBase::ReaderGroup* pReaderGroup //!< [in] The reader group management object.
    );

    /** Callback to inform other modules about creation of new DataSetReader
    *  The application can handle the decoding of the DataSetMessage by setting the
    *  messageHandledByApplication to true.
    */
    virtual void newDataSetReader(
        DataSetReader* pReader, //!< [in] The DataSetReader management object.
        bool&          messageDecodedByApplication /**< [out] Flag used to indicate that the application is responsible for decoding
                                                       the fields of the DataSetMessage (true) or decoding should be done by the PubSub stack (false).
                                                       In the case of true, the encoding is started with a callback to the application. */
    ) = 0;

    /** Callback to inform application about a state change of a PubSub object.
    */
    virtual void pubSubObjectStateChange(
        PubSubObject* pPubSubObject, //!< [in] The affected PubSub object
        PubSubObject::PubSubObjectType pubSubObjectType, //!< [in] The type of the affected PubSub object
        OpcUa_PubSubState newState  //!< [in] The new state of the affected PubSub object
    );

    /** Callback to inform application about the deletion of a PubSub object.
    */
    virtual void pubSubObjectRemoved(
        PubSubObject* pPubSubObject, //!< [in] The affected PubSub object
        PubSubObject::PubSubObjectType pubSubObjectType //!< [in] The type of the affected PubSub object
    ) = 0;

private:
};

}

#endif // #ifndef __READERQUEUEITEM_H__

