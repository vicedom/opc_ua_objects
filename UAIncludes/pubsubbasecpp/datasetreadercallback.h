/******************************************************************************
** datasetreadercallback.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __DATASETREADERCALLBACK_H__
#define __DATASETREADERCALLBACK_H__

#include "pubsubbase.h"
#include "datasetreader.h"
#include <uaencoder/encoder.h>
#include <uabase/datetime.h>

namespace PubSubBase
{

/** Callback class for the DataSetReader.
*/
class PUBSUBBASE_EXPORT DataSetReaderCallback
{
    UA_DISABLE_COPY(DataSetReaderCallback);
public:
    DataSetReaderCallback() {}
    virtual ~DataSetReaderCallback() {}

    /** Callback informing the application about the start up of the PubSub module in the server SDK.
    */
    virtual OpcUa_StatusCode startUpReader(
        PubSubBase::DataSetReader* pDataSetReader //!< [in] The affected DataSetReader
    ) = 0;

    /** Callback informing the application about a state change of the DataSetReader.
    */
    virtual void stateChange(
        DataSetReader* pDataSetReader, //!< [in] The affected DataSetReader
        OpcUa_PubSubState newState  //!< [in] The new state of the affected DataSetReader
    );

    /** Callback informing the application about the shut down of the PubSub module in the server SDK.
    *  The application is not allowed to call any methods on the PubSubModule if this method call is returned.
    */
    virtual OpcUa_StatusCode shutDownReader(
        PubSubBase::DataSetReader* pDataSetReader //!< [in] The affected DataSetReader
    ) = 0;

    /** Callback informing about a new message received for a DataSetReader
    *
    * This callback ist used if the application decodes the received message.
    *
    * The method should not block
    */
    virtual OpcUa_StatusCode newMessageReceived(
        PubSubBase::DataSetReader* pDataSetReader, //!< [in] The affected DataSetReader
        struct ua_decoder_context* pDec, //!< [in] The decoder object used to read the DataSetMessage fields.
        OpcUa_Byte dataSetMessageType,  /**< [in] Bit mask indicating the type of the written DataSetMessage.<br>
                                         The bit range 0-3 indicates the DataSetMessage type listed in the following table<br>
                                         Bit Values |  Description
                                         -----------|-------------------
                                         0000       | Data Key Frame
                                         0001       | Data Delta Frame
                                         0010       | Event
                                         0011       | Keep Alive*/
        OpcUa_UInt16 status,            //!< [in] The overall status of the DataSetMessage
        ua_datetime timestamp           //!< [in] The timestamp of the DataSetMessage
    ) = 0;

    /** Callback informing about new data in the queue
    *
    * This callback is used if the application takes the values from a value queue filled
    * by the PubSubstack when a received message is decoded by the PubSub stack
    *
    * The method should not block
    */
    virtual void newDataInQueue(
        PubSubBase::DataSetReader* pDataSetReader, //!< [in] The affected DataSetReader
        OpcUa_UInt16 status,  //!< [in] The overall status of the DataSetMessage
        ua_datetime timestamp //!< [in] The timestamp of the DataSetMessage.
    );
};

}

#endif // #ifndef __DATASETREADERCALLBACK_H__

