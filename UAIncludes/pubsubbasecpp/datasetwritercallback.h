/******************************************************************************
** datasetwritercallback.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __DATASETWRITERCALLBACK_H__
#define __DATASETWRITERCALLBACK_H__

#include "pubsubbase.h"
#include "datasetwriter.h"
#include <uaencoder/encoder.h>

namespace PubSubBase
{

/** Callback class for the DataSetWriter.
*/
class PUBSUBBASE_EXPORT DataSetWriterCallback
{
    UA_DISABLE_COPY(DataSetWriterCallback);
public:
    DataSetWriterCallback() {}
    virtual ~DataSetWriterCallback() {}

    /** Callback informing the application about the start up of the DataSetWriter.
    */
    virtual OpcUa_StatusCode startUpWriter(
        DataSetWriter* pDataSetWriter //!< [in] The affected DataSetWriter
    ) = 0;

    /** Callback informing the application about a state change of the DataSetWriter.
    */
    virtual void stateChange(
        DataSetWriter* pDataSetWriter, //!< [in] The affected DataSetWriter
        OpcUa_PubSubState newState  //!< [in] The new state of the affected DataSetWriter
    );

    /** Callback informing the application about the shut down of the DataSetWriter.
    *
    *  The application is not allowed to call any methods on the PubSubModule if this method call is returned.
    */
    virtual OpcUa_StatusCode shutDownWriter(
        DataSetWriter* pDataSetWriter //!< [in] The affected DataSetWriter
    ) = 0;

    /** Callback for encoding the fields of a new DataSetMessage before sending
    *
    *  The application is responsible for writing the field count and the field data.
    *  The DataSetMessage header is already filled by the SDK.
    *
    *  The out parameters allow to influence the header in the case of an error or
    *  if the message type changes between messages.
    */
    virtual OpcUa_StatusCode writeDataSetMessageFields(
        DataSetWriter* pDataSetWriter,   //!< [in] The affected DataSetWriter
        struct ua_encoder_context* pEnc, //!< [in] The encoder object used to write the DataSetMessage fields.
        bool& dataSetMessageValid,       //!< [out] Flag indicating if the DataSetMessage is valid
        OpcUa_Byte& dataSetMessageType,  /**< [out] Bit mask indicating the type of the written DataSetMessage.<br>
                                                    The bit range 0-3 indicates the DataSetMessage type listed in the following table<br>
                                                        Bit Values |  Description
                                                        -----------|-------------------
                                                        0000       | Data Key Frame
                                                        0001       | Data Delta Frame
                                                        0010       | Event
                                                        0011       | Keep Alive
                                                    Bit 4 indicates if more events are available if the type is Event.*/
        OpcUa_UInt16& status             //!< [out] The overall status of the DataSetMessage
    ) = 0;
};

}

#endif // #ifndef __DATASETWRITERCALLBACK_H__

