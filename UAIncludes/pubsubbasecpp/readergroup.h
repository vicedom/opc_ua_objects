/******************************************************************************
** readergroup.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __READERGROUP_H__
#define __READERGROUP_H__

#include "pubsubgroup.h"
#include "uakeyvaluepair.h"
#include <list>

namespace PubSubBase
{

class PUBSUBBASE_EXPORT PubSubConnection;
class PUBSUBBASE_EXPORT DataSetReader;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for ReaderGroups.
*/
class PUBSUBBASE_EXPORT ReaderGroup : public PubSubGroup
{
    friend class PubSubConnection;
    UA_DISABLE_COPY(ReaderGroup);

protected:
    ReaderGroup();
    ReaderGroup(
        PubSubConnection*          pParent,
        OpcUa_ReaderGroupDataType* pConfigData,
        OpcUa_UInt32               configIndex,
        OpcUa_Int32                stackHandle);
    ReaderGroup(
        PubSubConnection*          pParent,
        OpcUa_ReaderGroupDataType* pConfigData);
    virtual ~ReaderGroup();

public:
    UaStatus startUp();
    UaStatus shutDown();

    // #################################################
    // ReaderGroup settings
    UaString name() const;
    void setName(const UaString& name);
    bool enabled() const;
    void setEnabled(bool bEnabled);
    OpcUa_UInt32 maxNetworkMessageSize() const;
    void setMaxNetworkMessageSize(OpcUa_UInt32 maxNetworkMessageSize);
    UaKeyValuePairs groupProperties() const;
    void setGroupProperties(const UaKeyValuePairs& groupProperties);
    // #################################################

    // #################################################
    // Children handling
    DataSetReader* addDataSetReader(
        OpcUa_WriterGroupDataType*   pWriterGroup,
        OpcUa_DataSetWriterDataType* pDataSetWriter,
        OpcUa_DataSetMetaDataType*   pDataSetMetaData);
    DataSetReader* addDataSetReader(
        const UaString&              name,
        bool                         enabled,
        const UaVariant&             publisherId,
        OpcUa_Double                 messageReceiveTimeout,
        OpcUa_Double                 receiveOffset,
        OpcUa_Double                 processingOffset,
        OpcUa_WriterGroupDataType*   pWriterGroup,
        OpcUa_DataSetWriterDataType* pDataSetWriter,
        OpcUa_DataSetMetaDataType*   pDataSetMetaData);
    OpcUa_UInt32 dataSetReaderCount() const;
    DataSetReader* getDataSetReader(OpcUa_UInt32 index);
    UaStatus removeDataSetReader(DataSetReader* pDataSetReader);
    // #################################################

    inline PubSubConnection* pParent() const { return m_pParent; }
    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_ReaderGroupDataType* pConfigData() const { return m_pConfigData; }

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_ReaderGroup; }

private:
    UaStatus createChildren();
    void initChildren();
    void setConfigRelation(OpcUa_ReaderGroupDataType* pConfigData, OpcUa_UInt32 configIndex);
    UaStatus updateReaderGroupStructure();
    UaStatus reallocStructures(OpcUa_UInt32 dataSetReaderCount, const std::list<OpcUa_UInt32>& deleteIndexList);
    static OpcUa_ReaderGroupDataType* createStructure();
    static UaStatus createReaderGroup(
        OpcUa_ReaderGroupDataType* pReaderGroup,
        const UaString&            name,
        bool                       enabled,
        OpcUa_UInt32               maxNetworkMessageSize = 1400);

private:
    PubSubConnection*          m_pParent;
    std::list<DataSetReader*>  m_dataSetReaders;
    OpcUa_ReaderGroupDataType* m_pConfigData;
    OpcUa_UInt32               m_configIndex;
};
/*! @} */

}

#endif // #ifndef __READERGROUP_H__

