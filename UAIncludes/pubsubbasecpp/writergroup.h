/******************************************************************************
** writergroup.h
**
** Copyright (c) 2006-2019 Unified Automation GmbH All rights reserved.
**
** Software License Agreement ("SLA") Version 2.7
**
** Unless explicitly acquired and licensed from Licensor under another
** license, the contents of this file are subject to the Software License
** Agreement ("SLA") Version 2.7, or subsequent versions
** as allowed by the SLA, and You may not copy or use this file in either
** source code or executable form, except in compliance with the terms and
** conditions of the SLA.
**
** All software distributed under the SLA is provided strictly on an
** "AS IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
** AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT
** LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
** PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific
** language governing rights and limitations under the SLA.
**
** The complete license agreement can be found here:
** http://unifiedautomation.com/License/SLA/2.7/
**
** Description: OPC Unified Architecture Software Development Kit.
**
******************************************************************************/

#ifndef __WRITERGROUP_H__
#define __WRITERGROUP_H__

#include "pubsubgroup.h"
#include "uathread.h"
#include "uakeyvaluepair.h"
#include "uadatagramwritergrouptransportdatatype.h"
#include "uabrokerwritergrouptransportdatatype.h"
#include "uauadpwritergroupmessagedatatype.h"
#include "uajsonwritergroupmessagedatatype.h"
#include <list>

namespace PubSubBase
{

class PubSubConnection;
class DataSetWriter;
class PublishedDataSet;

/*! \addtogroup UaPubSubBaseLibrary
*  @{
*/

/** Management object for WriterGroups.
*/
class PUBSUBBASE_EXPORT WriterGroup :
    public PubSubGroup
{
    friend class PubSubConnection;
    UA_DISABLE_COPY(WriterGroup);

protected:
    WriterGroup();
    WriterGroup(
        PubSubConnection*          pParent,
        OpcUa_WriterGroupDataType* pConfigData,
        OpcUa_UInt32               configIndex,
        OpcUa_Int32                stackHandle);
    WriterGroup(
        PubSubConnection*          pParent,
        OpcUa_WriterGroupDataType* pConfigData);
    virtual ~WriterGroup();

public:
    UaStatus startUp();
    UaStatus shutDown();

    // #################################################
    // WriterGroup settings
    UaString name() const;
    void setName(const UaString& name);
    bool enabled() const;
    void setEnabled(bool bEnabled);
    OpcUa_UInt32 maxNetworkMessageSize() const;
    void setMaxNetworkMessageSize(OpcUa_UInt32 maxNetworkMessageSize);
    UaKeyValuePairs groupProperties() const;
    void setGroupProperties(const UaKeyValuePairs& groupProperties);
    OpcUa_UInt16 writerGroupId() const;
    void setWriterGroupId(OpcUa_UInt16 writerGroupId);
    OpcUa_Double publishingInterval() const;
    void setPublishingInterval(OpcUa_Double publishingInterval);
    OpcUa_Double keepAliveTime() const;
    void setKeepAliveTime(OpcUa_Double keepAliveTime);
    OpcUa_Byte priority() const;
    void setPriority(OpcUa_Byte priority);
    UaStringArray localeIds() const;
    void setLocaleIds(const UaStringArray& localeIds);
    UaString headerLayoutUri() const;
    void setHeaderLayoutUri(const UaString& headerLayoutUri);
    PubSubHeaderLayout headerLayout() const;
    // Datagram Transport Settings
    UaStatus transportSettingsDatagram(UaDatagramWriterGroupTransportDataType& datagramTransportSettings) const;
    UaStatus setTransportSettingsDatagram(const UaDatagramWriterGroupTransportDataType& datagramTransportSettings);
    // Broker Transport Settings
    UaStatus transportSettingsBroker(UaBrokerWriterGroupTransportDataType& brokerTransportSettings) const;
    UaStatus setTransportSettingsBroker(const UaBrokerWriterGroupTransportDataType& brokerTransportSettings);
    // UADP Message Settings
    UaStatus messageSettingsUadp(UaUadpWriterGroupMessageDataType& uadpMessageSettings) const;
    UaStatus setMessageSettingsUadp(const UaUadpWriterGroupMessageDataType& uadpMessageSettings);
    // JSON Message Settings
    UaStatus messageSettingsJson(UaJsonWriterGroupMessageDataType& jsonMessageSettings) const;
    UaStatus setMessageSettingsJson(const UaJsonWriterGroupMessageDataType& jsonMessageSettings);
    // #################################################

    // #################################################
    // Children handling
    DataSetWriter* addDataSetWriter(
        PublishedDataSet*       pPublishedDataSet);
    DataSetWriter* addDataSetWriter_UADP(
        const UaString&         name,
        bool                    enabled,
        OpcUa_UInt16            dataSetWriterId,
        PublishedDataSet*       pPublishedDataSet);
    OpcUa_UInt32 dataSetWriterCount() const;
    DataSetWriter* getDataSetWriter(OpcUa_UInt32 index);
    UaStatus removeDataSetWriter(DataSetWriter* pGroup);
    // #################################################

    UaStatus calculateNetworkMessageHeaderSize(OpcUa_UInt16& size, bool& isFixedSize);
    UaStatus calculateNetworkMessageSize(OpcUa_UInt32& size, bool& isFixedSize);

    inline PubSubConnection* pParent() const { return m_pParent; }
    inline OpcUa_UInt32 configIndex() const { return m_configIndex; }
    inline OpcUa_WriterGroupDataType* pConfigData() const { return m_pConfigData; }

    inline bool timingHandledByApplication() const { return m_timingHandledByApplication; }
    inline void setTimingHandledByApplication(bool timingHandledByApplication) { m_timingHandledByApplication = timingHandledByApplication; }
    OpcUa_StatusCode sample();
    OpcUa_StatusCode publish();

    virtual PubSubObjectType pubSubObjectType() const { return PubSubObjectType_WriterGroup; }

private:
    UaStatus createChildren();
    void initChildren();
    void setConfigRelation(OpcUa_WriterGroupDataType* pConfigData, OpcUa_UInt32 configIndex);
    UaStatus updateWriterGroupStructure();
    UaStatus reallocStructures(OpcUa_UInt32 dataSetWriterCount, const std::list<OpcUa_UInt32>& deleteIndexList);
    static OpcUa_WriterGroupDataType* createStructure();
    static UaStatus createWriterGroup_UADP(
        OpcUa_WriterGroupDataType* writerGroup,
        const UaString&            name,
        bool                       enabled,
        OpcUa_UInt16               writerGroupId,
        OpcUa_Double               publishingInterval,
        OpcUa_Double               keepAliveTime,
        PubSubHeaderLayout         headerLayout,
        TransportFacet             transportFacet = PubSubObject::TransportFacet_PubSub_UDP_UADP,
        OpcUa_Double               publishingOffset = -1,
        OpcUa_Double               samplingOffset = -1,
        OpcUa_UInt32               maxNetworkMessageSize = 1400);

private:
    PubSubConnection*          m_pParent;
    std::list<DataSetWriter*>  m_dataSetWriters;
    OpcUa_WriterGroupDataType* m_pConfigData;
    OpcUa_UInt32               m_configIndex;
    bool                       m_timingHandledByApplication;
};
/*! @} */

}

#endif // #ifndef __WRITERGROUP_H__

