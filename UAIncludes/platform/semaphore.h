/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_FRONTEND_UASEMAPHORE_H
#define UA_PLATFORM_FRONTEND_UASEMAPHORE_H

#include <platform/platform.h>
#include <psemaphore_types.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup platform_semaphore semaphore
 * @ingroup platform
 * Semaphore for managing shared resources.
 * @{
 */

int ua_sem_create(ua_sem_t *sem, int shared, unsigned int value, const char *name);
int ua_sem_destroy(ua_sem_t *sem);
int ua_sem_post(ua_sem_t *sem);
int ua_sem_wait(ua_sem_t *sem);
int ua_sem_trywait(ua_sem_t *sem);
int ua_sem_timedwait(ua_sem_t *sem, int ms_timeout);

/** @} */

UA_END_EXTERN_C

#endif /* UA_PLATFORM_FRONTEND_UASEMAPHORE_H */
