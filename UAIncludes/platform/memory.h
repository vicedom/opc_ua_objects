/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_FRONTEND_MEMORY_H
#define UA_PLATFORM_FRONTEND_MEMORY_H

#include <stdlib.h> /* size_t */
#include <platform/platform.h>
#include <platform/platform_config.h>
#include <pmemory.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup platform_memory memory
 * @ingroup platform
 * Wrapper for common memory operations.
 *
 * In case a platform doesn't support standard malloc or has
 * an optimized version of memset it can be forwarded to the
 * custom implementation.
 *
 * @{
 */

#ifdef MEMORY_ENABLE_TRACE
#define ua_malloc(size) ua_malloc2(size, __FILE__, __LINE__)
#define ua_calloc(nmemb, size) ua_calloc2(nmemb, size, __FILE__, __LINE__)
#define ua_realloc(ptr, size) ua_realloc2(ptr, size, __FILE__, __LINE__)
#define ua_free(ptr) ua_free2(ptr, __FILE__, __LINE__)
#define ua_strdup(s) ua_strdup2(s, __FILE__, __LINE__)
void *ua_malloc2(size_t size, const char *file, int line);
void *ua_calloc2(size_t nmemb, size_t size, const char *file, int line);
void *ua_realloc2(void *ptr, size_t size, const char *file, int line);
void ua_free2(void *ptr, const char *file, int line);
char *ua_strdup2(const char *s, const char *file, int line);
#else
void *ua_malloc(size_t size);
void *ua_calloc(size_t nmemb, size_t size);
void *ua_realloc(void *ptr, size_t size);
void ua_free(void *ptr);
char *ua_strdup(const char *s);
#endif

/**
 * Fill the first \c n bytes of the memory area pointed to
 * by \c s with the byte \c c.
 * @return \c s.
 */
#define ua_memset(s, c, n) ua_p_memset(s, c, n)

/**
 * Securely wipes sensitive data.
 * @param s data pointer to wipe
 * @param n number of bytes to wipe
 * This function cannot be optimized away by compilers.
 * @return \c s.
 */
#define ua_mem_clear_s(s, n) ua_p_mem_clear_s(s, n)

/**
 * Copy \c n bytes from memory area \c src to memory area \c dest.
 * The memory areas may overlap: copying takes place as though the
 * bytes in \c src are first copied into a temporary array that does
 * not overlap \c src or \c dest, and the bytes are then copied from the
 * temporary array to \c dest.
 */
#define ua_memmove(dest, src, n) ua_p_memmove(dest, src, n)

/**
 * Copy \c n bytes from the memory area \c src to \c dest.
 * The memory areas must not overlap.
 * @return \c dest.
 */
#define ua_memcpy(dest, src, n) ua_p_memcpy(dest, src, n)

/**
 * Compare the first \c n bytes of the memory areas s1 and s2.
 * @return Zero if both memory areas are equal, a positive value if
 * s1 is greater, a negative value if s2 is greater.
 */
#define ua_memcmp(s1, s2, n) ua_p_memcmp(s1, s2, n)

void *ua_mempcpy(void *dest, const void *src, size_t n);

#ifdef MEMORY_ENABLE_PLATFORM_STATISTICS
unsigned int ua_memalloc_count(void);
#endif

/** @} */

UA_END_EXTERN_C

#endif /* UA_PLATFORM_FRONTEND_MEMORY_H */
