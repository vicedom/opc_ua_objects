/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef PFILE_TYPES_H_X0DYBUHG
#define PFILE_TYPES_H_X0DYBUHG

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <time.h>

typedef HANDLE ua_file_t;
#define UA_FILE_T_INVALID INVALID_HANDLE_VALUE

#define ua_fs_dev_t     unsigned int
#define ua_fs_ino_t     unsigned int
#define ua_fs_mode_t    unsigned int
#define ua_fs_nlink_t   unsigned int
#define ua_fs_uid_t     unsigned int
#define ua_fs_gid_t     unsigned int
#define ua_fs_off_t     unsigned __int64
#define ua_fs_blksize_t unsigned int
#define ua_fs_blkcnt_t  unsigned int

/* length of ua_dir_entry name length */
#define UA_P_DNAME_LEN 256
/* system PATH_MAX limits */
#define UA_PATH_MAX MAX_PATH

/* standard file stream numbers */
extern ua_file_t UA_STDIN;
extern ua_file_t UA_STDOUT;
extern ua_file_t UA_STDERR;

#endif /* end of include guard: PFILE_TYPES_H_X0DYBUHG */
