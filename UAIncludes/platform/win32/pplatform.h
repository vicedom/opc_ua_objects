/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_H
#define UA_PLATFORM_H

#include <platform/platform_config.h>
#ifndef WIN32_LEAN_AND_MEAN
/* Always define WIN32_LEAN_AND_MEAN to prevent windows.h from including
 * winsock.h which leads to conflicts with winsock2.h */
# define WIN32_LEAN_AND_MEAN
#endif /* WIN32_LEAN_AND_MEAN */

/* Prevent definition of POSIX error codes. This would create problems with
   mapped error codes, e.g. in the socket implementation. */
#define _CRT_NO_POSIX_ERROR_CODES

/* There is a _WIN32_WINNT define which determines the available
 * windows api functions. When building on windows it is set to
 * a sensible value depending on the system it is build with.
 * Mingw does not know for which windows version to build and sets
 * a rather old default value if it is not set manually.
 *
 * For mingw64 we set this value to Vista (0x0600) as it is rather
 * unlikely somebody wants to build for 64bit XP using mingw.
 * It would be still possible by setting _WIN32_WINNT accordingly
 * before including this header.
 */
#ifdef __MINGW64__
# ifndef _WIN32_WINNT
#  define _WIN32_WINNT 0x0600
# endif
#endif

#include <windows.h>
#include <stdlib.h> /* add _countof() */
#include <malloc.h> /* this make alloca working on Windows */
#include <assert.h>
#include <stdio.h>    /* for fprintf (trace) */
#include <math.h>

#ifdef __MINGW32__
#include <float.h>
#include <stddef.h>
#endif /* __MINGW32__ */

/*
 * MSVC++ 14.0 _MSC_VER == 1900 (Visual Studio 2015)
 * MSVC++ 12.0 _MSC_VER == 1800 (Visual Studio 2013)
 * MSVC++ 11.0 _MSC_VER == 1700 (Visual Studio 2012)
 * MSVC++ 10.0 _MSC_VER == 1600 (Visual Studio 2010)
 * MSVC++ 9.0  _MSC_VER == 1500 (Visual Studio 2008)
 * MSVC++ 8.0  _MSC_VER == 1400 (Visual Studio 2005)
 * MSVC++ 7.1  _MSC_VER == 1310 (Visual Studio 2003)
 * MSVC++ 7.0  _MSC_VER == 1300
 * MSVC++ 6.0  _MSC_VER == 1200
 * MSVC++ 5.0  _MSC_VER == 1100
 */
#ifdef _MSC_VER
/* MS compilers give this warning with flexible arrays:
 * "nonstandard extension used : zero-sized array in struct/union."
 * However this is perfectly standard ISO C99, but MS compilers still
 * lack full C99 support. The MS compiler also supports this feature
 * (they call it unsized arrays), but give this warning.
 * So we can safely disable this warning.
 */
# pragma warning( disable : 4200 )
#endif /* _MSC_VER */

#ifdef __cplusplus
# define UA_BEGIN_EXTERN_C extern "C" {
# define UA_END_EXTERN_C }
#else
# define UA_BEGIN_EXTERN_C
# define UA_END_EXTERN_C
#endif

#define UA_UNUSED(x) (void)(x)

#define UA_LITTLE_ENDIAN 1234
#define UA_BIG_ENDIAN 4321
#define UA_PDP_ENDIAN 3412
/* On Window we normally have LITTLE_ENDIAN,
 * unfortunately there is no endian.h to detect this for sure.
 * So have to changes this manually if necessary
 */
#define UA_BYTE_ORDER UA_LITTLE_ENDIAN
#define UA_FLOAT_BYTE_ORDER UA_BYTE_ORDER
#define UA_DOUBLE_BYTE_ORDER UA_BYTE_ORDER

#if defined(_MSC_VER) && !defined(__cplusplus)
# define inline __inline
#endif

#ifdef _MSC_VER
/* fix some C99 function names */
# if _MSC_VER < 1900
#  define snprintf _snprintf
#  define strtof(nptr, endptr) (float)strtod(nptr, endptr)
# endif
# ifndef strdup
#  define strdup _strdup
# endif /* strdup */
# ifndef strtoll
# define strtoll(nptr, endptr, base) _strtoi64(nptr, endptr, base)
# endif /* strtoll */
# ifndef strtoull
# define strtoull(nptr, endptr, base) _strtoui64(nptr, endptr, base)
# endif /* strtoull */
# ifndef strcasecmp
# define strcasecmp(s1, s2) _stricmp(s1, s2)
# endif /* strcasecmp */
# ifndef strncasecmp
# define strncasecmp(s1, s2, n) _strnicmp(s1, s2, n)
# endif /* strncasecmp */
#endif /* _MSC_VER */

/* Windows defines a CONTAINING_RECORD macro in Ntdef.h.
 * This is actually the same as our container_of macro.
 * Se we just copy/paste the MS macro and rename it to
 * look like the GNU container_of macro.
 */
#define container_of(pointer, type, member) ((type *)( \
   (PCHAR)(pointer) - \
   (ULONG_PTR)(&((type *)0)->member)))

/* Returns the size of an array (number of elements).
 * VS defines _countof in stdlib.h
 */
#ifdef __MINGW32__
# define countof(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))
#else
# define countof(arr) _countof(arr)
#endif

/* Visual Studio byte swap intrinsics */
#ifdef HAVE_BYTESWAP_INTRINSICS
# define ua_bswap16(x) _byteswap_ushort(x)
# define ua_bswap32(x) _byteswap_ulong(x)
# define ua_bswap64(x) _byteswap_uint64(x)
#else
# define ua_bswap16(x) ((((unsigned __int16)(x))>> 8) | (((unsigned __int16)(x)) << 8))
# define ua_bswap32(x) (((((unsigned __int32)(x)) & 0xff000000) >> 24) | \
                        ((((unsigned __int32)(x)) & 0x00ff0000) >>  8) | \
                        ((((unsigned __int32)(x)) & 0x0000ff00) <<  8) | \
                        ((((unsigned __int32)(x)) & 0x000000ff) << 24))
# define ua_bswap64(x) (((((unsigned __int64)(x)) & 0xff00000000000000i64) >> 56) | \
                        ((((unsigned __int64)(x)) & 0x00ff000000000000i64) >> 40) | \
                        ((((unsigned __int64)(x)) & 0x0000ff0000000000i64) >> 24) | \
                        ((((unsigned __int64)(x)) & 0x000000ff00000000i64) >>  8) | \
                        ((((unsigned __int64)(x)) & 0x00000000ff000000i64) <<  8) | \
                        ((((unsigned __int64)(x)) & 0x0000000000ff0000i64) << 24) | \
                        ((((unsigned __int64)(x)) & 0x000000000000ff00i64) << 40) | \
                        ((((unsigned __int64)(x)) & 0x00000000000000ffi64) << 56))
#endif

/* UA byte swap macros - convert host byte order to little endian */
#if UA_BYTE_ORDER == UA_LITTLE_ENDIAN
# define ua_htole16(x) (x)
# define ua_htole32(x) (x)
# define ua_htole64(x) (x)
#else
# define ua_htole16(x) ua_bswap16(x)
# define ua_htole32(x) ua_bswap32(x)
# define ua_htole64(x) ua_bswap64(x)
#endif

#define ua_letoh16(x) ua_htole16(x)
#define ua_letoh32(x) ua_htole32(x)
#define ua_letoh64(x) ua_htole64(x)

/* portable assertion macro */
#ifndef UA_ASSERT /* could be defined already by other SDKs */
#  define UA_ASSERT(cond) assert(cond)
#endif

/* MS supports static_assert since VS2010 */
#if defined(_MSC_VER) && _MSC_VER >= 1600
#  define UA_STATIC_ASSERT(cond, msg) static_assert(cond, msg)
#else /* _MSC_VER */
#  define UA_STATIC_ASSERT(cond, msg)
#endif /* _MSC_VER */

/* The offsetof macro is defined in C89 and C99 and availabe on Windows,
 * but only starting with VS2015 it can be used as const value in static initializers.
 */
#if defined(_MSC_VER) && _MSC_VER < 1900
# define ua_offsetof(type, member) ((size_t) &(((type *)0)->member))
#else
# define ua_offsetof offsetof
#endif

/* MS does not support function attributes, but mingw does */
#ifdef __MINGW32__
#define UA_FORMAT_ARGUMENT(fmt, va) __attribute__ ((format (printf, fmt, va)))
#define UA_WEAK_SYMBOL __attribute__ ((weak))
#define UA_CONST_FUNCTION __attribute__ ((const))
#define UA_PURE_FUNCTION __attribute__ ((pure))
#define UA_NONNULL(...) __attribute__((nonnull(__VA_ARGS__)))
#define UA_HAVE_NONNULL_SUPPORT
#define UA_DEPRECATED __attribute((__deprecated__))
#define UA_WARN_UNUSED_RESULT __attribute((__warn_unused_result__))
#define UA_NORETURN __attribute((noreturn))
#else
#define UA_FORMAT_ARGUMENT(fmt, va)
#define UA_WEAK_SYMBOL
#define UA_CONST_FUNCTION
#define UA_PURE_FUNCTION
#define UA_NONNULL(...)
#define UA_DEPRECATED
#define UA_WARN_UNUSED_RESULT
#define UA_NORETURN
#endif

/* TODO:the check is not right; find another way to test if __func__ is available */
#ifndef __func__
#  define __func__ __FUNCTION__
#endif

/* This format specifier macro is just another hack for the stupid MS compiler.
 * MSVC does not support C99 and so it does not know %z. Using the macro we can
 * make the code working on both MS compilers and C99 compliant compilers.
 */
#define PRIuSIZE_T "Iu" /* size_t (unsigned) */
#define PRIiSIZE_T "Ii" /* ssize_t (signed, but also not supported at all by MS,
                           just here for consistency) */

int ua_p_platform_init(void);
int ua_p_platform_cleanup(void);
void ua_p_platform_panic(const char *msg, const char *file, int line) UA_NORETURN;

#ifdef _WIN64
typedef __int64           ssize_t;
#else
typedef int               ssize_t;
#endif

/* GCC provides MIN and MAX macros, but we define our own with UA prefix,
 * because MIN/MAX has portability issues with Euros OS.
 */
#define UA_MIN(x, y) (((x) < (y)) ? (x) : (y))
#define UA_MAX(x, y) (((x) > (y)) ? (x) : (y))

/* Visual Studio does not define some math.h macros,
 * so we need to fix this.
 * Since VS2012 it is available also in VS,
 * so we need to check the version to decice if we
 * need to define it ourselves.
 * Mingw32 always support standard C99 defines,
 * so there is no need for our defines.
 */
#if defined (_MSC_VER) && _MSC_VER < 1800
/* Check if given floating point value is finite. */
#ifndef INFINITY
# define INFINITY (DBL_MAX+DBL_MAX)
#endif
#ifndef isfinite
# define isfinite(xfp) _finite((double)xfp)
#endif

/* Check if given floating point value is NAN.*/
#ifndef NAN
# define NAN (INFINITY-INFINITY)
#endif
#ifndef isnan
# define isnan(xfp) _isnan((double)xfp)
#endif
/* alternative: #define isnan(xfp) (int)((xfp) != (xfp)) */
#endif

/* Max length of path strings. */
#ifndef PATH_MAX
# define PATH_MAX MAX_PATH
#endif

/** platform internal trace macro.
 * This is necessary to be able to trace early in PL,
 * before and trace module is loaded.
 * The more sophisticated trace lib is only usable from higher levels.
 */
#define UA_P_TRACE_INFO(msg, ...) fprintf(stdout, msg, ##__VA_ARGS__)
#define UA_P_TRACE_ERROR(msg, ...) fprintf(stderr, msg, ##__VA_ARGS__)

#endif /* end of include guard: UA_PLATFORM_H */

