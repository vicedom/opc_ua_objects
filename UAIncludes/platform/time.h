/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UA_PLATFORM_FRONTEND_TIME_H
#define UA_PLATFORM_FRONTEND_TIME_H

#include <stdint.h>
#include <time.h>
#include <platform/platform.h>

UA_BEGIN_EXTERN_C

/**
 * @defgroup platform_time time
 * @ingroup platform
 * Retrieve current time in different formats and sleep.
 * @{
 */

/**
 * Timestamp struct used by @ref ua_time_timestamp.
 * Counts seconds and microseconds since the beginning
 * of the epoch (1970) in contrast to  @ref ua_datetime
 * which counts 100 nanoseconds since 1601. Used for tracing.
 */
struct ua_timestamp {
    time_t tv_sec; /**< Seconds since the beginning of the epoch */
    uint32_t tv_usec; /**< Microseconds since the beginning of the epoch */
};

int ua_time_datetime(uint64_t *dt);
int ua_time_set_datetime(uint64_t dt);
int ua_time_build_datetime(uint64_t *dt);

int ua_time_timestamp(struct ua_timestamp *ts);
int ua_time_set_time(const struct ua_timestamp *ts);

uint64_t ua_time_tickcount_min(void);
uint64_t ua_time_tickcount_max(void);
uint64_t ua_time_tickcount(void);

void ua_time_usleep(unsigned int usec);

void ua_time_unixtime_to_filetime(const struct ua_timestamp *ts, uint64_t *ft);
void ua_time_filetime_to_unixtime(const uint64_t *ft, struct ua_timestamp *ts);

/** @} */

UA_END_EXTERN_C

#endif /* end of include guard: UA_PLATFORM_FRONTEND_TIME_H */

