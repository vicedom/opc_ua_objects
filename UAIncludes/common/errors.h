/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

/* Positive error codes */
#define UA_EASYNC                     1 /**< Operation completes asynchronously. */
#define UA_EGOOD                      0 /**< No error. All good. Be happy. */

/* Negative error codes */
#define UA_EBAD                        -1 /**< Generic fault. */
#define UA_EBADDISCONNECT              -2 /**< The connection was closed by the peer. */
#define UA_EBADSHUTDOWN                -3 /**< The connection was shut down by the peer. */
#define UA_EBADSOCKETCLOSED            -4 /**< The connection was closed locally. */
#define UA_EBADINVALIDARGUMENT         -5 /**< One or more of the supplied parameters were not valid. */
#define UA_EBADNOBUF                   -6 /**< Allocating a buffer failed. */
#define UA_EBADDECODING                -7 /**< An decoding error occured. */
#define UA_EBADENCODING                -8 /**< An encoding error occured. */
#define UA_EBADNOMEM                   -9 /**< Allocating memory failed */
#define UA_EBADNOTIMPL                -10 /**< The called function is not implemented fully or parially. */
#define UA_EBADHOSTUNKNOWN            -11 /**< The referenced host name could not be resolved. */
#define UA_EBADSYSCALL                -12 /**< An underlying system call failed. */
#define UA_EBADOPCANCELLED            -13 /**< The operation has been cancelled. */
#define UA_EBADOPTIMEOUT              -14 /**< The operation timed out. */
#define UA_EBADIOPENDING              -15 /**< There is already an operation pending. Call again after completion */
#define UA_EBADINVALIDSTATE           -16 /**< An object is not in the required state for the current operation. */
#define UA_EBADTRUNCATED              -17 /**< The result has been truncated. */
#define UA_EBADBUFTOOSMALL            -18 /**< The given destination buffer is too small. */
#define UA_EBADTOOMANYNESTEDCALLS     -19 /**< The operation would require too many iterations to succeed. */
#define UA_EBADNOTSUPPORTED           -20 /**< The requested operation is not supported. */
#define UA_EBADCERTIFICATINVALID      -21 /**< The certificate passed to the operation was not valid. */
#define UA_EBADNOTFOUND               -22 /**< A searched object could not be found. */
#define UA_EBADSIGNATURE              -23 /**< The operation failed because of a bad data signature. */
#define UA_EBADMISMATCH               -24 /**< The actual value didn't match the expected value. */
#define UA_EBADOUTOFRESOURCE          -25 /**< No more resources available. */
#define UA_EBADINUSE                  -26 /**< Resource is still in use. */
#define UA_EBADAGAIN                  -27 /**< Operation needs at last one more call to succeed. */
#define UA_EBADOUTOFRANGE             -28 /**< parameter is out of range. */
#define UA_EBADINTERNALERROR          -29 /**< Internal error. */
#define UA_EBADENCODINGLIMITSEXCEEDED -30 /**< Same as UA_SCBADENCODINGLIMITSEXCEEDED */
#define UA_EBADENCODINGTOOLARGE       -31 /**< Request or response would be too large if encoded. */
#define UA_EBADACCESSDENIED           -32 /**< Access denied. */
#define UA_EBADLOOP                   -33 /**< A loop exists in symbolic links encountered during resolution of the path argument. */
#define UA_EBADFILEEXISTS             -34 /**< The file or directory already exists. */
#define UA_EBADFILENOTFOUND           -35 /**< The specified file was not found. */
#define UA_EBADPATHNOTFOUND           -36 /**< The specified directory was not found. */
#define UA_EBADUNEXPECTED             -37 /**< For unexpected and unhandled errors to trigger general error handling. */
#define UA_EBADCOMMUNICATIONERROR     -38 /**< General error regarding the communication layer. */
#define UA_EBADSERVICERESPONSE        -39 /**< The server returned a bad statuscode in its response. */
#define UA_EBADLICENSELIMITSEXCEEDED  -40 /**< An operation failed because of limitations of the active license. */
#define UA_EBADREFEXISTS              -41 /**< The reference you tried to add already exists. */
#define UA_EBADREFISCONST             -42 /**< The reference you tried to modify is const. */
#define UA_EBADNOTHINGTODO            -43 /**< The reference you tried to modify is const. */
