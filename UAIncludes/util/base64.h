/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UTIL_BASE64_H
#define UTIL_BASE64_H

#include <platform/platform.h>

/**
 * @defgroup util_base64 base64
 * @ingroup util
 * @brief Base64 conversion.
 * @{
 */

UA_BEGIN_EXTERN_C

int util_base64_encode(const unsigned char *src, int slen, char *dst, int dlen);

int util_base64_decode(const char *src, int slen, unsigned char *dst, int dlen);
int util_base64_decode_verify(const char *src, int slen);
int util_base64_decode_unchecked(const char *src, int slen, unsigned char *dst, int dlen);

UA_END_EXTERN_C

/** @} */

#endif /* UTIL_BASE64_H */
