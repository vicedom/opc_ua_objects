/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef UTIL_STRING_H
#define UTIL_STRING_H

#include <stdint.h> /* for uint32_t */
#include <stdlib.h> /* for size_t */
#include <stdbool.h> /* for bool */
#include <platform/platform.h>

/**
 * @defgroup util_string string
 * @ingroup util
 * @brief String utility functions.
 * @{
 */

UA_BEGIN_EXTERN_C

size_t util_strlcpy(char *dst, const char *src, size_t len);
size_t util_strlcat(char *dst, const char *src, size_t len);
int util_snprintf(char *str, size_t size, const char *format, ...) UA_FORMAT_ARGUMENT(3, 4);
void util_print_aligned(char *line, size_t size, const char *left, char fillchar, const char *fmt, ...) UA_FORMAT_ARGUMENT(5, 6);

bool util_string_starts_with(const char *text, const char *prefix);
bool util_string_ends_with(const char *text, const char *suffix);

char *util_join_uint32(char *buf, size_t size, const uint32_t *array, unsigned int array_len, const char *sep);

UA_END_EXTERN_C

/** @} */

#endif /* end of include guard: UTIL_STRING_H */

