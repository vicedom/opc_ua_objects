/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef WATCHDOG_H_IKZ7KUXX
#define WATCHDOG_H_IKZ7KUXX

#include <stdint.h>
#include <stdbool.h>
#include <platform/platform.h>

/* Software based watchdog. */

struct util_watchdog {
    uint64_t last_reset; /**< last reset timestamp. */
    uint64_t interval;   /**< time interval until the watchdog bites. */
};

UA_BEGIN_EXTERN_C

void util_watchdog_init(struct util_watchdog *w, uint64_t interval);
void util_watchdog_clear(struct util_watchdog *w);
void util_watchdog_reset(struct util_watchdog *w);
bool util_watchdog_is_expired(struct util_watchdog *w);

UA_END_EXTERN_C

#endif /* end of include guard: WATCHDOG_H_IKZ7KUXX */

