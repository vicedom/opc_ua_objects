/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef FSM_H_ZYEC0FFA
#define FSM_H_ZYEC0FFA

#include <stdbool.h>
#include <util/ringqueue.h>

/* Enable dot writer to generate graphviz dot files.
 * This is only for developers and should not be enabled in production.
 */
/*#define HAVE_DOT_WRITER*/

/* Deine maximum event queue size */
#define FSM_QUEUE_SIZE 10

/* Index type to use to reference states, events and transitions.
 * For now we used unsigned char which allows up to 255 state, transitions, and evnts.
 * Small types reduce the table size and thus the memory requirements.
 * The type must always by unsigned!
 */
typedef unsigned char util_fsm_index_t;
#define UTIL_FSM_EVENT_NONE 255

struct util_fsm;
struct util_fsm_transition;

UA_BEGIN_EXTERN_C

/** Action function pointer type.
 * This callback can be used to implement the action that should be executed
 * when a transition happens.
 * @param fsm The FSM context.
 * @param transition The transition index in the table.
 */
typedef void (*fsm_action_t)(struct util_fsm *fsm, const struct util_fsm_transition *t);

struct util_fsm_transition {
    util_fsm_index_t  from;   /**< from state */
    util_fsm_index_t  to;     /**< to state */
    util_fsm_index_t  event;  /**< event which triggers this transition */
    fsm_action_t action; /**< action to be called */
#ifdef HAVE_DOT_WRITER
    const char *from_name;
    const char *to_name;
    const char *event_name;
    const char *action_name;
#endif /* HAVE_DOT_WRITER */
};

/**
 * This callback is called whenever a transition occurs.
 * This can be used to log this information or to send e.g. an OPC UA event.
 * This way an OPC UA State Machine could be realized.
 *
 * @param fsm The FSM context.
 * @param t Pointer to the transition structure which contains information
 *   about the current transition.
 */
typedef void (*fsm_transition_t)(struct util_fsm *fsm, const struct util_fsm_transition *t);

/**
 * This callback is called when no transition could be found the matches the
 * current state/event combination.
 * This can indicate an error condition or can be silently ignore,
 * depending on your FSM design.
 *
 * @param fsm The FSM context.
 * @param event The event that caused this callback.
 */
typedef void (*fsm_no_transition_t)(struct util_fsm *fsm, util_fsm_index_t event);

/** Finite state machine context.
 * This FSM implementation allows you to create FSMs dynamically at runtime
 * using util_fsm_init() and util_fsm_add_transition(), or to use built-in transition
 * tables using util_fsm_init_static().
 * With each transition you can associate an action (function pointer) which gets
 * executed when the transitions is performed. Thus this FSM implements a
 * "Mealy machine".
 * An optional callback can be registered to get notified about each state change.
 */
struct util_fsm {
    util_fsm_index_t current_state;
    util_fsm_index_t num_transitions;
    util_fsm_index_t table_size;
    struct util_fsm_transition *transition_tbl;
    util_fsm_index_t num_states;
    util_fsm_index_t *state_offset_tbl; /**< offset of each state in transition table */
    fsm_transition_t transition_cb;
    fsm_no_transition_t no_transition_cb;
    struct util_ringqueue event_queue;
    void *queue_elements[FSM_QUEUE_SIZE];
    void *object;
    bool valid; /**< Set to true when the FSM was validated successfully */
    bool free;  /**< Set to true when the tables are allocated dynamically */
};

int util_fsm_init(
    struct util_fsm *fsm,
    util_fsm_index_t num_states,
    util_fsm_index_t num_transitions);
int util_fsm_init_static(
    struct util_fsm *fsm,
    util_fsm_index_t num_states,
    const util_fsm_index_t *state_offset_tbl,
    util_fsm_index_t num_transitions,
    const struct util_fsm_transition *transition_tbl);
void util_fsm_clear(struct util_fsm *fsm);

void util_fsm_set_transition_cb(struct util_fsm *fsm, fsm_transition_t cb);
void util_fsm_set_no_transition_cb(struct util_fsm *fsm, fsm_no_transition_t cb);

#ifndef HAVE_DOT_WRITER
int util_fsm_add_transition(
    struct util_fsm *fsm,
    util_fsm_index_t from,
    util_fsm_index_t to,
    util_fsm_index_t event,
    fsm_action_t action
);
#endif /* HAVE_DOT_WRITER */

int util_fsm_validate(struct util_fsm *fsm);
int util_fsm_set_object(struct util_fsm *fsm, void *object);
void *util_fsm_get_object(const struct util_fsm *fsm);
int util_fsm_process_event(struct util_fsm *fsm);
int util_fsm_add_event(struct util_fsm *fsm, util_fsm_index_t event);

#ifdef HAVE_DOT_WRITER
int util_fsm_write_dot_file(struct util_fsm *fsm, const char *filename);
int util_fsm_add_transition_with_names(
    struct util_fsm *fsm,
    util_fsm_index_t from,
    util_fsm_index_t to,
    util_fsm_index_t event,
    fsm_action_t action,
    const char *from_name,
    const char *to_name,
    const char *event_name,
    const char *action_name
);
#define util_fsm_add_transition(fsm, from, to, event, action) util_fsm_add_transition_with_names(fsm, from, to, event, action, #from, #to, #event, #action)
#endif /* HAVE_DOT_WRITER */

UA_END_EXTERN_C

#endif /* end of include guard: FSM_H_ZYEC0FFA */

