/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UAENCODER_SERVICECONTEXT_H_
#define _UAENCODER_SERVICECONTEXT_H_

#include <inttypes.h>
#include <stdint.h>
#include <uabase/base.h>

struct ua_servicecontext {
    uint16_t encoding_id;
    uint16_t type_id;
    bool is_request;
    size_t body_size;
    decode_t decode_fct;
    encode_t encode_fct;
    clear_t clear_fct;
    service_handler_t handler_fct;
};

struct ua_servicecontext_handler {
    uint16_t type_id;
    service_handler_t handler_fct;
};

int ua_servicecontext_validate_table(void);

const struct ua_servicecontext *ua_servicecontext_find(uint16_t encoding_id);
const struct ua_servicecontext *ua_servicecontext_find_by_typeid(uint16_t type_id);

int ua_servicecontext_register_handler_fcts(const struct ua_servicecontext_handler *handler_fcts, unsigned int num_handler_fcts);

#endif /* _UAENCODER_SERVICECONTEXT_H_ */
