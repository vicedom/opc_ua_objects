/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UAENCODER_BOOLEAN_H_
#define _UAENCODER_BOOLEAN_H_

#include <platform/platform.h>
#include <stdbool.h>
#include "uint8.h"

static inline int ua_encode_boolean(struct ua_encoder_context *ctx, const bool *val)
{
    return ua_encode_uint8_value(ctx, (const uint8_t) *val);
}

static inline int ua_decode_boolean(struct ua_decoder_context *ctx, bool *val)
{
    uint8_t tmp;
    int ret = ua_decode_uint8(ctx, &tmp);

    if (ret == 0) {
        if (tmp > 0)
            *val = true;
        else
            *val = false;
    }
    return ret;
}

static inline int ua_decode_boolean_skip(struct ua_decoder_context *ctx)
{
    return ua_decode_uint8_skip(ctx);
}

#endif /* _UAENCODER_BOOLEAN_H_ */
