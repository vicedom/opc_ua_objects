/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UAENCODER_ENCODER_H_
#define _UAENCODER_ENCODER_H_

#include <uabase/base_config.h>
#include <uabase/base.h>
#include <uabase/type_table.h>

struct ua_encoder_context {
    struct ua_buffer *first; /**< 1st buffer */
    struct ua_buffer *cur;   /**< current buffer */
    unsigned int total_bytes_written; /**< bytes written from first buffer till now */
    unsigned int max_bytes_to_write; /**< maximum number of bytes to be written from first buffer till now */
    unsigned int reserved_start; /**< bytes to reserve at the beginning of a new buffer */
    unsigned int reserved_end; /**< bytes to reserve at the end of a new buffer */
    unsigned int buffer_handle; /**< handle for buffer management */
    unsigned int buffer_count; /**< number of currently allocated buffers by this context */
    unsigned int max_buffer_count; /**< maximum number of buffers this context will allocate */
};

struct ua_decoder_context {
    struct ua_buffer *first; /**< 1st buffer */
    struct ua_buffer *cur;   /**< current buffer */
    /** this field counts the call depth of nested decoder functions.
     * this way we can cancel recursive calls (e.g. inner diagnostic info)
     * or also indirect recursions (e.g. variant->array->variant->...)
     */
    unsigned int call_depth;
#ifdef ENABLE_DIAGNOSTICS
    /** The stringtable is set by the responseheader decoder.
     * The diagnosticinfo decoder then passes it to decoded diagnosticinfos.
     */
    struct ua_stringtable *string_table;
#endif
};

extern const struct ua_type_table_encoder_fcts g_encoder_fcts_ns0[UA_TYPE_TABLE_NUM_NS0_FCTS];

int ua_encoder_newbuffer(struct ua_encoder_context *ctx);
unsigned int ua_decoder_context_remaining(struct ua_decoder_context *ctx);

void ua_encoder_context_init(struct ua_encoder_context *ctx);
void ua_encoder_context_init_bytestring(struct ua_encoder_context *ctx, struct ua_bytestring *bs, struct ua_buffer *buf);
void ua_encoder_context_clear(struct ua_encoder_context *ctx);
unsigned int ua_encoder_context_stream_bytes(struct ua_encoder_context *ctx);
void ua_decoder_context_init(struct ua_decoder_context *ctx);
void ua_decoder_context_init_bytestring(struct ua_decoder_context *ctx, const struct ua_bytestring *bs, struct ua_buffer *buf);
void ua_decoder_context_clear(struct ua_decoder_context *ctx);
struct ua_buffer *ua_encoder_detach_buffers(struct ua_encoder_context *ctx);
struct ua_buffer *ua_decoder_detach_buffers(struct ua_decoder_context *ctx);

int ua_encoder_write_zeroes(struct ua_encoder_context *ctx, unsigned int len);
int ua_encoder_write_zeroes_chunked(struct ua_encoder_context *ctx, unsigned int len);

unsigned int ua_decoder_get_current_buffer_pos(struct ua_decoder_context *ctx);
int ua_decoder_set_current_buffer_pos(struct ua_decoder_context *ctx, unsigned int pos);

void ua_encoder_enable_buffer_alloc(void);
void ua_encoder_disable_buffer_alloc(void);

int ua_encoder_init(void);
void ua_encoder_cleanup(void);

#endif /* _UAENCODER_ENCODER_H_ */
