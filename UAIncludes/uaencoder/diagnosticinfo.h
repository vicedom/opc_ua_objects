/*****************************************************************************
 *                                                                           *
 * Copyright (c) 2006-2019 Unified Automation GmbH. All rights reserved.     *
 *                                                                           *
 * Software License Agreement ("SLA") Version 2.7                            *
 *                                                                           *
 * Unless explicitly acquired and licensed from Licensor under another       *
 * license, the contents of this file are subject to the Software License    *
 * Agreement ("SLA") Version 2.7, or subsequent versions as allowed by the   *
 * SLA, and You may not copy or use this file in either source code or       *
 * executable form, except in compliance with the terms and conditions of    *
 * the SLA.                                                                  *
 *                                                                           *
 * All software distributed under the SLA is provided strictly on an "AS     *
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,       *
 * AND LICENSOR HEREBY DISCLAIMS ALL SUCH WARRANTIES, INCLUDING WITHOUT      *
 * LIMITATION, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR   *
 * PURPOSE, QUIET ENJOYMENT, OR NON-INFRINGEMENT. See the SLA for specific   *
 * language governing rights and limitations under the SLA.                  *
 *                                                                           *
 * The complete license agreement can be found here:                         *
 * http://unifiedautomation.com/License/SLA/2.7/                             *
 *                                                                           *
 *****************************************************************************/

#ifndef _UAENCODER_DIAGNOSTIC_INFO_H_
#define _UAENCODER_DIAGNOSTIC_INFO_H_

#include <stdint.h>
#include <uabase/base_config.h>
#include "encoder.h"
#include "uint8.h"

/* forward declarations */
struct ua_decoder_context;
struct ua_encoder_context;

int ua_decode_diagnosticinfo_skip(struct ua_decoder_context *ctx);
int ua_decode_diagnosticinfo_array_skip(struct ua_decoder_context *ctx);

#ifdef ENABLE_DIAGNOSTICS
/* forward declarations */
struct ua_diagnosticinfo;

int ua_encode_diagnosticinfo(struct ua_encoder_context *ctx, const struct ua_diagnosticinfo *val);
int ua_decode_diagnosticinfo(struct ua_decoder_context *ctx, struct ua_diagnosticinfo *val);

#define ua_decode_diagnosticinfo_array(ctx, result, num_results) \
    ua_encoder_decode_array(ctx, (void **) result, num_results, (decode_t) ua_decode_diagnosticinfo, sizeof(struct ua_diagnosticinfo))
#else
/* if diagnosticinfo is disabled map decode to decode_skip */

static inline int ua_encode_diagnosticinfo(struct ua_encoder_context *ctx, const void *dummy)
{
    uint8_t val = 0;
    UA_UNUSED(dummy);
    return ua_encode_uint8_value(ctx, val);
}

static inline int ua_decode_diagnosticinfo(struct ua_decoder_context *ctx, void *dummy)
{
    UA_UNUSED(dummy);
    return ua_decode_diagnosticinfo_skip(ctx);
}

#define ua_decode_diagnosticinfo_array(ctx, val, num_results) ua_decode_diagnosticinfo_array_skip(ctx)
#endif /* ENABLE_DIAGNOSTICS */

#endif /* _UAENCODER_DIAGNOSTIC_INFO_H_ */

