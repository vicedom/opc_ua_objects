#ifndef OPCDODLL_H
#define OPCDODLL_H

#include <QObject>
#include <QVector>
#include <QHash>



namespace opcdodllclass {

class OpcdoDllClass : public QObject
{    
    Q_OBJECT    

public:    
    ~OpcdoDllClass() override;

    static QString errorString(quint32 errorNumber);
    static OpcdoDllClass* instance();

    static int registerPrivate(QObject* privateClass);
    static int unregisterPrivate(QObject* privateClass);
    static int privatesReferenceCount();

signals:

public slots:

private:
    explicit OpcdoDllClass(QObject* parent = nullptr);

    int registerObject(QObject* obj);
    int unregisterObject(QObject* obj);
    int objectReferenceCount();

    static void initErrorStrings();

private:
    bool m_uaPlatformLayerInitialized;
    QVector<QObject*> m_registeredObjects;

    static OpcdoDllClass* m_instance;
    static QHash<quint32, QString> m_opcdodllErrorStrings;
};

} // namespace

#endif // OPCDODLL_H
