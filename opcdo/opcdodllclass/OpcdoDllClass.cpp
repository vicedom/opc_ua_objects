#include "OpcdoDllClass.h"
#include "opcdodllerrors.h"

#include "uaplatformlayer.h"

#include <QMetaObject>
#include <QDebug>

// OpcuaSessionCallback

using namespace opcdodllclass;

OpcdoDllClass* OpcdoDllClass::m_instance = nullptr;
QHash<quint32, QString> OpcdoDllClass::m_opcdodllErrorStrings;

OpcdoDllClass* OpcdoDllClass::instance()
{
    if (!m_instance) {

        m_instance = new OpcdoDllClass;
        initErrorStrings();
    }

    return m_instance;
}

void OpcdoDllClass::initErrorStrings()
{
    if (!m_opcdodllErrorStrings.isEmpty()) {
        return;
    }

    m_opcdodllErrorStrings[OPCDOERR_SERVER_ERROR_MASK] = OPCDOERR_SERVER_ERROR_MASK_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EMPTY_SERVER_URL] = OPCDOERR_EMPTY_SERVER_URL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_ALLREADY_CONNECTED] = OPCDOERR_ALLREADY_CONNECTED_STRING;
    m_opcdodllErrorStrings[OPCDOERR_NO_SESSION_CONNECTED] = OPCDOERR_NO_SESSION_CONNECTED_STRING;
    m_opcdodllErrorStrings[OPCDOERR_CURSORTYPE_NOT_SUPPORTED] = OPCDOERR_CURSORTYPE_NOT_SUPPORTED_STRING;
    m_opcdodllErrorStrings[OPCDOERR_CONNECTION_BROKEN] = OPCDOERR_CONNECTION_BROKEN_STRING;
    m_opcdodllErrorStrings[OPCDOERR_NO_SESSION_SUBSCRIPTION] = OPCDOERR_NO_SESSION_SUBSCRIPTION_STRING;

    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_ERROR_MASK] = OPCDOERR_EXCHANGE_ERROR_MASK_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_WRITE_VALUE_INVAL] = OPCDOERR_EXCHANGE_WRITE_VALUE_INVAL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_WRITE_TYPE_INVAL] = OPCDOERR_EXCHANGE_WRITE_TYPE_INVAL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_WRITE_ARRAY_INVAL] = OPCDOERR_EXCHANGE_WRITE_ARRAY_INVAL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_WRITE_MATRIX_INVAL] = OPCDOERR_EXCHANGE_WRITE_MATRIX_INVAL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_WRITE_DIMENSION_INVAL] = OPCDOERR_EXCHANGE_WRITE_DIMENSION_INVAL_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG] = OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_READ_VALUE] = OPCDOERR_EXCHANGE_READ_VALUE_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_READ_ARRAY] = OPCDOERR_EXCHANGE_READ_ARRAY_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_READ_MATRIX] = OPCDOERR_EXCHANGE_READ_MATRIX_STRING;
    m_opcdodllErrorStrings[OPCDOERR_EXCHANGE_MONITORED_ARG_ARG] = OPCDOERR_EXCHANGE_MONITORED_ARG_ARG_STRING;

    m_opcdodllErrorStrings[OPCDOERR_PARSER_ERROR_MASK] = OPCDOERR_PARSER_ERROR_MASK_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_STRING_EMPTY] = OPCDOERR_PARSE_STRING_EMPTY_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_RANK_INVALID] = OPCDOERR_PARSE_RANK_INVALID_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_ARRAY_RBRACE] = OPCDOERR_PARSE_ARRAY_RBRACE_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_ARRAY_RBRACE_ARG] = OPCDOERR_PARSE_ARRAY_RBRACE_ARG_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_ARRAY_LBRACE] = OPCDOERR_PARSE_ARRAY_LBRACE_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_ARRAY_LBRACE_ARG] = OPCDOERR_PARSE_ARRAY_LBRACE_ARG_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_VALUE_PARSE] = OPCDOERR_PARSE_VALUE_PARSE_STRING;
    m_opcdodllErrorStrings[OPCDOERR_PARSE_VALUE_PARSE_ARG] = OPCDOERR_PARSE_VALUE_PARSE_ARG_STRING;

    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_ERROR_MASK] = OPCDOERR_COLLOCATOR_ERROR_MASK_STRING;
    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_RANK_INVALID] = OPCDOERR_COLLOCATOR_RANK_INVALID_STRING;
    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_VALUE] = OPCDOERR_COLLOCATOR_VALUE_STRING;
    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_VALUE_ARG] = OPCDOERR_COLLOCATOR_VALUE_ARG_STRING;
    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_VALUE_ARG_ARG] = OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_STRING;
    m_opcdodllErrorStrings[OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG] = OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG_STRING;
}

QString OpcdoDllClass::errorString(quint32 errorNumber)
{
    return m_opcdodllErrorStrings.value(errorNumber);
}

int OpcdoDllClass::registerPrivate(QObject* privateClass)
{
    return OpcdoDllClass::instance()->registerObject(privateClass);
}

int OpcdoDllClass::unregisterPrivate(QObject* privateClass)
{
    if (!m_instance) {

        Q_ASSERT(false);
        return 0;
    }

    int refCount = m_instance->unregisterObject(privateClass);

    if (refCount == 0) {

        m_instance->deleteLater();
        m_instance = nullptr;
    }

    return refCount;
}

int OpcdoDllClass::privatesReferenceCount()
{
    if (!m_instance) {
        return 0;
    }

    return m_instance->objectReferenceCount();
}

OpcdoDllClass::OpcdoDllClass(QObject* parent)
    : QObject(parent)
    , m_uaPlatformLayerInitialized(false)
{
    m_uaPlatformLayerInitialized = UaPlatformLayer::init() == 0;
}

OpcdoDllClass::~OpcdoDllClass()
{
#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor OpcdoDllClass::~OpcdoDllClass called";
#endif

    if (m_uaPlatformLayerInitialized) {
        UaPlatformLayer::cleanup();
    }

    qDebug() << "opcdodll can be unloaded now";
}

int OpcdoDllClass::registerObject(QObject* obj)
{
    Q_ASSERT(obj != nullptr);
    Q_ASSERT(!m_registeredObjects.contains(obj));

#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "registering dll object: <" << obj->objectName() << ">";
#endif

    m_registeredObjects.append(obj);

    return m_registeredObjects.count();
}

int OpcdoDllClass::unregisterObject(QObject* obj)
{
    Q_ASSERT(obj != nullptr);
    Q_ASSERT(m_registeredObjects.contains(obj));

    m_registeredObjects.remove(m_registeredObjects.indexOf(obj));

#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "dll object removed: <" << obj->objectName() << ">";
#endif

    return m_registeredObjects.count();
}

int OpcdoDllClass::objectReferenceCount()
{
    return m_registeredObjects.count();
}
