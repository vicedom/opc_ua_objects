#include "OpcdoEnums.h"

#include <QMetaEnum>

QString OpcdoEnums::dataTypeString(DataType type)
{
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("DataType"));
    QString typeString = metaEnum.valueToKey(type);

    return typeString;
}

QString OpcdoEnums::serverStatusString(ServerStatus status)
{
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("ServerStatus"));
    QString statusString = metaEnum.valueToKey(status);

    return statusString;
}

QString OpcdoEnums::connectServiceTypeString(ConnectServiceType type)
{
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("ConnectServiceType"));
    QString typeString = metaEnum.valueToKey(type);

    return typeString;
}

QString OpcdoEnums::cursorTypeString(CursorType type)
{
    QMetaEnum metaEnum = staticMetaObject.enumerator(staticMetaObject.indexOfEnumerator("CursorType"));
    QString typeString = metaEnum.valueToKey(type);

    return typeString;
}

QMetaType::Type OpcdoEnums::fromDataType(DataType dType)
{
    switch (dType)
    {
    case Boolean:
        return QMetaType::Bool;
    case DateTime:
        return QMetaType::QDateTime;
    case Int16:
        return QMetaType::Short;
    case Int32:
        return QMetaType::Int;
    case Int64:
        return QMetaType::LongLong;
    case Byte:
        return QMetaType::UChar;
    case SByte:
        return QMetaType::SChar;
    case UInt16:
        return QMetaType::UShort;
    case UInt32:
        return QMetaType::UInt;
    case UInt64:
        return QMetaType::ULongLong;
    case Float:
        return QMetaType::Float;
    case Double:
        return QMetaType::Double;
    case String:
        return QMetaType::QString;
    default:
        return QMetaType::UnknownType;
    }
}

OpcdoEnums::DataType OpcdoEnums::fromMetaType(QMetaType::Type mtype)
{
    switch (mtype)
    {
    case QMetaType::Bool:
        return Boolean;
    case QMetaType::QDateTime:
        return DateTime;
    case QMetaType::Short:
        return Int16;
    case QMetaType::Int:
        return Int32;
    case QMetaType::LongLong:
        return Int64;
    case QMetaType::UChar:
        return Byte;
    case QMetaType::SChar:
        return SByte;
    case QMetaType::UShort:
        return UInt16;
    case QMetaType::UInt:
        return UInt32;
    case QMetaType::ULongLong:
        return UInt64;
    case QMetaType::Float:
        return Float;
    case QMetaType::Double:
        return Double;
    case QMetaType::QString:
        return String;
    default:
        return Unknown;
    }
}
