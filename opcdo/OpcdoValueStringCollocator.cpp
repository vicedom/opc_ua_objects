#include "OpcdoValueStringCollocator.h"
#include "opcdodllerrors.h"
#include "OpcdoException.h"

#include "opcdodllclass/OpcdoDllClass.h"

#include <QMetaType>

using namespace opcdodllclass;

static bool breakListDown(const QVariantList& list, int& dimension)
{
    if (dimension < 0) {
        return false;
    }

    if (list.isEmpty()) {

        dimension = -1;
        return false;
    }

    QVariant value = list.at(0);

    if (!value.isValid() || value.isNull()) {

        dimension = -1;
        return false;
    }

    QMetaType::Type type = static_cast<QMetaType::Type>(value.type());

    if (type != QMetaType::QVariantList) {
        return true;
    }

    dimension++;
    QVariantList test = value.value<QVariantList>();

    if (!breakListDown(test, dimension)) {
        return false;
    }

    return true;
}


static int valueRank(const QVariant& value)
{
    if (!value.isValid() || value.isNull()) {
        return -1;
    }

    QMetaType::Type type = static_cast<QMetaType::Type>(value.type());

    if (type != QMetaType::QVariantList) {
        return 0;
    }

    int dimension = 1;
    QVariantList test = value.value<QVariantList>();

    breakListDown(test, dimension);
    return dimension;
}

QString OpcdoValueStringCollocator::collocate(const QVariant& value, const OpcdoEnums::DataType dtype)
{
    int rank = valueRank(value);

    if (rank < 0) {

        OpcdoException ex(OPCDOERR_COLLOCATOR_RANK_INVALID, OpcdoDllClass::errorString(OPCDOERR_COLLOCATOR_RANK_INVALID));
        ex.raise();

        return QString();
    }

    if (rank == 0) {
        return collocateScalar(value, dtype);
    }
    else if (rank == 1) {
        return collocateArray(value.value<QVariantList>(), dtype);
    }
    else if (rank == 2) {
        return collocateMatrix(value.value<QVariantList>(), dtype);
    }
    else {
        return collocateDimension(value.value<QVariantList>(), dtype, rank);
    }
}

QString OpcdoValueStringCollocator::collocateScalar(const QVariant& value, const OpcdoEnums::DataType dtype, int index, int subarray, const QString &dimdesc)
{
    QMetaType::Type mtype = OpcdoEnums::fromDataType(dtype);

    if (value.canConvert(mtype)) {
        return value.toString();
    }

    OpcdoException ex;

    if (index < 0) {

        ex.setReason(OPCDOERR_COLLOCATOR_VALUE);
        ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_COLLOCATOR_VALUE));
    }
    else {

        if (subarray < 0) {

            ex.setReason(OPCDOERR_COLLOCATOR_VALUE_ARG);
            ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_COLLOCATOR_VALUE_ARG).arg(index + 1));
        }
        else {

            if (dimdesc.isEmpty()) {

                ex.setReason(OPCDOERR_COLLOCATOR_VALUE_ARG_ARG);
                ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_COLLOCATOR_VALUE_ARG_ARG).arg(index + 1).arg(subarray + 1));
            }
            else {

                ex.setReason(OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG);
                ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG).arg(index + 1).arg(subarray + 1).arg(dimdesc));
            }
        }
    }

    ex.raise();
    return QString();
}

QString OpcdoValueStringCollocator::collocateArray(const QVariantList& list, const OpcdoEnums::DataType dtype, int subarray, const QString& dimdesc)
{
    QString collString("{");
    int i, len = list.length();

    for (i = 0; i < len; i++) {

        collString += collocateScalar(list.at(i), dtype, i, subarray, dimdesc);

        if (i < (len -1)) {
            collString += "|";
        }
    }

    collString += "}";
    return collString;
}

QString OpcdoValueStringCollocator::collocateMatrix(const QVariantList& list, const OpcdoEnums::DataType dtype, const QString& dimdesc)
{
    QString collString("{");
    int i, len = list.length();

    for (i = 0; i < len; i++) {

        collString += collocateArray(list.at(i).value<QVariantList>(), dtype, i, dimdesc);

        if (i < (len -1)) {
            collString += "|";
        }
    }

    collString += "}";
    return collString;
}

QString OpcdoValueStringCollocator::collocateDimension(const QVariantList& list, OpcdoEnums::DataType dtype, int rank, const QString& dimdesc)
{
    static QString format("[%1]");

    if (rank == 2) {
        return collocateMatrix(list, dtype, dimdesc);
    }

    QString collString("{");
    int i, len = list.length();
    QString ddesc = dimdesc + format.arg(rank + 1);

    for (i = 0; i < len; i++) {

        collString += collocateDimension(list.at(i).value<QVariantList>(), dtype, rank - 1, ddesc);

        if (i < (len -1)) {
            collString += "|";
        }
    }

    collString += "}";
    return collString;
}
