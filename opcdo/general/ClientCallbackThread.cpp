#include "ClientCallbackThread.h"

#include <QDebug>

ClientCallbackThread::ClientCallbackThread(const QString& identification, QObject* parent)
    : QThread(parent)
    , m_identification(identification)
{

}

ClientCallbackThread::~ClientCallbackThread()
{
#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor ClientCallbackThread::~ClientCallbackThread <" << m_identification << "> called";
#endif
}

