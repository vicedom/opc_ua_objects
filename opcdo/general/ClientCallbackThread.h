#ifndef CLIENTCALLBACKTHREAD_H
#define CLIENTCALLBACKTHREAD_H

#include <QThread>

class ClientCallbackThread : public QThread
{
    Q_OBJECT
public:
    explicit ClientCallbackThread(const QString& identification, QObject* parent = nullptr);
    ~ClientCallbackThread() override;

private:
    QString m_identification;
};

#endif // CLIENTCALLBACKTHREAD_H
