QT += core network

TEMPLATE = lib
DEFINES += OPCDO_LIBRARY

CONFIG += c++11 dll

UA_INCLUDE_DIR = "../UAIncludes"
UA_THIRD_PARTY_DIR = "../UAThirdParty"
PROJECT_INCLUDE_DIR = "../includes"

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $${UA_INCLUDE_DIR}/uabasecpp
INCLUDEPATH += $${UA_INCLUDE_DIR}/uaclientcpp
INCLUDEPATH += $${UA_INCLUDE_DIR}/uastack
INCLUDEPATH += $${UA_INCLUDE_DIR}/uapkicpp
INCLUDEPATH += $${UA_THIRD_PARTY_DIR}/include
INCLUDEPATH += $${PROJECT_INCLUDE_DIR}

INCLUDEPATH += general
INCLUDEPATH += opcua
INCLUDEPATH += private

MOC_DIR     = generated
OBJECTS_DIR = generated
UI_DIR      = generated
RCC_DIR     = generated

SOURCES += \
    OpcdoDatabase.cpp \
    OpcdoEnums.cpp \
    OpcdoException.cpp \
    OpcdoField.cpp \
    OpcdoRecord.cpp \
    OpcdoValueStringCollocator.cpp \
    OpcdoValueStringParser.cpp \
    general/ClientCallbackThread.cpp \
    opcdodll.cpp \
    opcdodllclass/OpcdoDllClass.cpp \
    opcua/ClientServerDataExchange.cpp \
    opcua/ClientServerDataExchangeSynchrounus.cpp \
    opcua/OpcuaSessionCallback.cpp \
    opcua/OpcuaSubscriptionCallback.cpp \
    opcua/OpcuaVariantRead.cpp \
    opcua/OpcuaVariantWrite.cpp \
    private/OpcdoDatabasePrivate.cpp \
    private/OpcdoFieldPrivate.cpp \
    private/OpcdoRecordPrivate.cpp

HEADERS += \
    ../includes/OpcdoDatabase.h \
    ../includes/OpcdoEnums.h \
    ../includes/OpcdoException.h \
    ../includes/OpcdoField.h \
    ../includes/OpcdoRecord.h \
    ../includes/OpcdoValueStringCollocator.h \
    ../includes/OpcdoValueStringParser.h \
    ../includes/opcdodll.h \
    ../includes/opcdodllerrors.h \
    general/ClientCallbackThread.h \
    opcdodllclass/OpcdoDllClass.h \
    opcua/ClientServerDataExchange.h \
    opcua/OpcuaSessionCallback.h \
    opcua/OpcuaSubscriptionCallback.h \
    opcua/OpcuaVariant.h \
    private/OpcdoDatabasePrivate.h \
    private/OpcdoFieldPrivate.h \
    private/OpcdoRecordPrivate.h


CONFIG(debug, debug|release): TARGET = $$join(TARGET,,,d)

DESTDIR = $$PWD/../lib

CONFIG(release, debug|release): DLLDESTDIR = $$PWD/../dllRelease
CONFIG(debug, debug|release): DLLDESTDIR = $$PWD/../dllDebug

LIBS += -lws2_32 -lmpr -lkernel32 -luser32 -lgdi32 -lwinspool -lshell32 -lole32 -loleaut32 -luuid -lcomdlg32 -ladvapi32

INCLUDEPATH += $$PWD/../UALibs
DEPENDPATH += $$PWD/../UALibs

CONFIG(release, debug|release): LIBS += -L$$PWD/../UALibs/ -luabasecpp
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UALibs/ -luabasecppd
CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uabasecpp.lib
CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uabasecppd.lib

CONFIG(release, debug|release): LIBS += -L$$PWD/../UALibs/ -luaclientcpp
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UALibs/ -luaclientcppd
CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uaclientcpp.lib
CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uaclientcppd.lib

CONFIG(release, debug|release): LIBS += -L$$PWD/../UALibs/ -luapkicpp
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UALibs/ -luapkicppd
CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uapkicpp.lib
CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/uapkicppd.lib

CONFIG(release, debug|release): LIBS += -L$$PWD/../UALibs/ -lxmlparsercpp
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UALibs/ -lxmlparsercppd
CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/xmlparsercpp.lib
CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../UALibs/xmlparsercppd.lib

CONFIG(release, debug|release): LIBS += -L$$PWD/../UALibs/ -luastack
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UALibs/ -luastackd

INCLUDEPATH += $$PWD/../UAThirdParty/libs
DEPENDPATH += $$PWD/../UAThirdParty/libs

CONFIG(release, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibcrypto
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibcryptod

CONFIG(release, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibssl
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibssld

CONFIG(release, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibxml2
CONFIG(debug, debug|release): LIBS += -L$$PWD/../UAThirdParty/libs/ -llibxml2d


CONFIG(release, debug|release): QMAKE_POST_LINK += ..\\scripts\\copyRelease.bat
CONFIG(debug, debug|release): QMAKE_POST_LINK += ..\\scripts\\copyDebug.bat
