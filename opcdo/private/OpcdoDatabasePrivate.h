#ifndef OPCDODATABASEPRIVATE_H
#define OPCDODATABASEPRIVATE_H

#include "OpcdoEnums.h"

#include <QObject>
#include <QPair>
#include <QHash>

namespace UaClientSdk {
    class UaSession;
    class UaSubscription;
    class SessionSecurityInfo;
    class OpcuaSessionCallback;
    class OpcuaSubscriptionCallback;
}

class OpcdoDatabase;
class OpcdoRecord;
class ClientCallbackThread;

class OpcdoDatabasePrivate : public QObject
{    
    Q_OBJECT
public:
    explicit OpcdoDatabasePrivate(OpcdoDatabase* parent);
    ~OpcdoDatabasePrivate() override;

    void setCompanyName(const QString& companyName);
    void setProductName(const QString& productName);

    int connect(const QString& url);
    int disconnect();
    OpcdoRecord* createRecord(const QString& nodeId, OpcdoEnums::CursorType cursorType);
    QString valueString(const QString& nodeId);
    QString valueDataTypeString(const QString& nodeId);
    QVariant value(const QString& nodeId, OpcdoEnums::DataType& dataType);
    bool writeValue(const QString& nodeId, const QVariant& varValue, OpcdoEnums::DataType dataType);

    bool monitorValue(const QString& nodeId, uint monitoringId);
    bool unmonitorValue(uint monitoringId);
    bool isMonitored(const QString& nodeId);
    bool isMonitored(uint monitoringId);

signals:
    void connectError(quint32 reason,
                      QString message,
                      QHash<QString, QString> errorInfo);
    void connectStatusChanged(uint clientConnectionId,
                              int serverStatus);
    void subscriptStatusChanged(uint clientSubscriptionHandle,
                                uint statusVal,
                                const QString& statusString);
    void subscriptDataChange(uint clientSubscriptionHandle,
                             uint montoringId,
                             const QDateTime& modified,
                             const QVariant& varValue);
    void subscriptDataChangeError(uint clientSubscriptionHandle,
                                  uint montoringId,
                                  uint statusVal,
                                  const QString& statusString);

private:
    bool connectSubscription(uint subscriptionId);
    void disconnectSubsciption();
    void setSubsciption(UaClientSdk::UaSubscription* subscription);
    QString findMonitoringId(uint monitoringId);
    void remonitor();
    void unmonitor();

private slots:
    void sessionCallbackAffinityRestored();
    void connectionStatusChanged(uint clientConnectionId,
                                 int serverStatus);
    void connectErrorDirect(uint clientConnectionId,
                            int serviceType,
                            uint error,
                            const QString& errorString,
                            bool clientSideError);
    void connectErrorQueued(uint clientConnectionId,
                            int serviceType,
                            uint error,
                            const QString &errorString,
                            bool clientSideError);

    void subscriptionCallbackAffinityRestored();
    void subscriptionStatusChanged(uint clientSubscriptionHandle,
                                   uint statusVal,
                                   const QString& statusString);
    void subscriptionDataChangeError(uint clientSubscriptionHandle,
                                     uint montoringId,
                                     uint statusVal,
                                     const QString& statusString);
    void subscriptionDataChange(uint clientSubscriptionHandle,
                                uint montoringId,
                                const QDateTime& modified,
                                const QVariant& varValue);

    void appAboutToQuit();


private:
    UaClientSdk::UaSession* m_session;
    UaClientSdk::UaSubscription* m_subscription;
    UaClientSdk::SessionSecurityInfo* m_sessionSecurityInfo;

    UaClientSdk::OpcuaSessionCallback* m_sessionCallback;
    ClientCallbackThread* m_sessionCallbackThread;

    UaClientSdk::OpcuaSubscriptionCallback* m_subscriptionCallback;
    ClientCallbackThread* m_subscriptionCallbackThread;

    uint m_clientConnectionId;
    int m_serverStatus;
    QString m_serverUrl;
    QString m_companyName;
    QString m_productName;
    QHash<QString, QPair<uint, uint> > m_monitoredNodes;

    bool m_doBreakNoServer;

    OpcdoDatabase* q;
    friend class OpcdoDatabase;
    friend class ClientServerDataExchange;
};

#endif // OPCDODATABASEPRIVATE_H
