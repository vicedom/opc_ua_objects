#include "OpcdoFieldPrivate.h"
#include "OpcdoField.h"
#include "OpcdoException.h"
#include "opcdodllerrors.h"
#include "opcdodllclass/OpcdoDllClass.h"

OpcdoFieldPrivate::OpcdoFieldPrivate(OpcdoField* parent, OpcdoField* parentField)
    : m_parentField(parentField)
    , m_nodeClass(OpcdoField::Invalid)
    , m_nameSpace(-1)
    , m_expandProceeded(false)
    , q(parent)
{
    Q_ASSERT(q != nullptr);
}

OpcdoFieldPrivate::~OpcdoFieldPrivate()
{
    qDeleteAll(m_childItems);
}

OpcdoField* OpcdoFieldPrivate::child(int row)
{
    if (row < 0 || row >= m_childItems.size()) {
        return nullptr;
    }

    return m_childItems.at(row);
}

int OpcdoFieldPrivate::childCount() const
{
    return m_childItems.count();
}

int OpcdoFieldPrivate::row() const
{
    if (m_parentField)
        return m_parentField->d->m_childItems.indexOf(const_cast<OpcdoField*>(q));

    return 0;
}

int OpcdoFieldPrivate::columnCount() const
{
    return 5;
}

QVariant OpcdoFieldPrivate::data(int column) const
{
    switch (column)
    {
    case 0:
        return m_nodeId;

    case 1:
        return m_nodeClass;

    case 2:
        return m_displayName;

    case 3:
        return m_nameSpace;

    case 4:
        return m_value;

    default:
        break;
    }

    return QVariant();
}

void OpcdoFieldPrivate::makeNaked()
{
    qDeleteAll(m_childItems);
    m_expandProceeded = false;
}

