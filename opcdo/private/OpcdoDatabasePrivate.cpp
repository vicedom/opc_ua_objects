#include "OpcdoDatabasePrivate.h"
#include "OpcdoDatabase.h"
#include "OpcdoRecord.h"
#include "ClientServerDataExchange.h"
#include "OpcuaSessionCallback.h"
#include "OpcuaSubscriptionCallback.h"
#include "OpcdoException.h"
#include "opcdodllerrors.h"
#include "opcdodllclass/OpcdoDllClass.h"
#include "ClientCallbackThread.h"

#include "uabase.h"
#include "uaclientsdk.h"
#include "uasession.h"
#include "uasubscription.h"

#include "uaplatformlayer.h"
#include "uadatavalue.h"
#include "uadiscovery.h"

/* low level and stack */
#include "opcua_core.h"

#if OPCUA_SUPPORT_PKI
#include "uapkicertificate.h"
#include "uapkirevocationlist.h"
#endif // OPCUA_SUPPORT_PKI

#include "uasettings.h"
#include "libtrace.h"
#include "uaeventfilter.h"
#include "uaargument.h"
#include "uacertificatedirectoryobject.h"
#include "uatrustlistobject.h"
#include <iostream>
#include <uadir.h>
#include "uaunistringlist.h"

#include <stdlib.h>

#include <QCoreApplication>
#include <QMetaEnum>

#include <QDebug>

using namespace opcdodllclass;
using namespace UaClientSdk;

OpcdoDatabasePrivate::OpcdoDatabasePrivate(OpcdoDatabase* parent)
    : QObject(parent)
    , m_session(nullptr)
    , m_subscription(nullptr)
    , m_sessionSecurityInfo(nullptr)
    , m_sessionCallback(nullptr)
    , m_sessionCallbackThread(nullptr)
    , m_subscriptionCallback(nullptr)
    , m_subscriptionCallbackThread(nullptr)
    , m_clientConnectionId(-1)
    , m_serverStatus(OpcdoEnums::Disconnected)
    , m_doBreakNoServer(false)
    , q(parent)
{
    Q_ASSERT(q != nullptr);

    setObjectName("OpcdoDatabasePrivate");

    opcdodllclass::OpcdoDllClass::registerPrivate(this);

    QObject::connect(qApp, SIGNAL(aboutToQuit()), this, SLOT(appAboutToQuit()), Qt::DirectConnection);

    setCompanyName("");
    setProductName("");

    m_sessionSecurityInfo = new SessionSecurityInfo;
    m_sessionCallbackThread = new ClientCallbackThread("session callback", this);
    m_subscriptionCallbackThread = new ClientCallbackThread("subscription callback", this);

    srand(time(0));
}

OpcdoDatabasePrivate::~OpcdoDatabasePrivate()
{
    if (m_subscriptionCallbackThread->isRunning()) {

        m_subscriptionCallbackThread->quit();
        m_subscriptionCallbackThread->wait();
    }

    delete m_subscriptionCallbackThread;
    m_subscriptionCallbackThread = nullptr;

    if (m_sessionCallbackThread->isRunning()) {

        m_sessionCallbackThread->quit();
        m_sessionCallbackThread->wait();
    }

    delete m_sessionCallbackThread;
    m_sessionCallbackThread = nullptr;

    opcdodllclass::OpcdoDllClass::unregisterPrivate(this);

#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor OpcdoDatabasePrivate::~OpcdoDatabasePrivate called";
#endif
}

void OpcdoDatabasePrivate::setCompanyName(const QString& companyName)
{
    if (!m_serverUrl.isEmpty()) {
        return;
    }

    QString name = companyName.trimmed();

    if (name.isEmpty()) {
        m_companyName = "Unknown Company";
    }
    else {
        m_companyName = name;
    }
}

void OpcdoDatabasePrivate::setProductName(const QString& productName)
{
    if (!m_serverUrl.isEmpty()) {
        return;
    }

    QString name = productName.trimmed();

    if (name.isEmpty()) {
        m_productName = "Unknown Product";
    }
    else {
        m_productName = name;
    }
}

void OpcdoDatabasePrivate::appAboutToQuit()
{
    q->disconnect();
}

void OpcdoDatabasePrivate::sessionCallbackAffinityRestored()
{
    m_sessionCallbackThread->quit();
}

void OpcdoDatabasePrivate::subscriptionCallbackAffinityRestored()
{
    m_subscriptionCallbackThread->quit();
}

void OpcdoDatabasePrivate::connectErrorDirect(uint clientConnectionId,
                                              int serviceType,
                                              uint error,
                                              const QString& errorString,
                                              bool clientSideError)
{
    QString formatX("0x%1");
    OpcdoException ex(error, errorString);

    ex.errorInfo()[OPCDOERRINFO_clientConnectionId] = QString::number(clientConnectionId);
    ex.errorInfo()[OPCDOERRINFO_error] = formatX.arg(QString::number(error, 16));
    ex.errorInfo()[OPCDOERRINFO_errorString] = errorString;
    ex.errorInfo()[OPCDOERRINFO_clientSideError] = clientSideError ? "true" : "false";

    QString typeString = OpcdoEnums::connectServiceTypeString(static_cast<OpcdoEnums::ConnectServiceType>(serviceType));
    ex.errorInfo()[OPCDOERRINFO_serviceType] = typeString;

    delete m_session;
    m_session = nullptr;

    m_sessionCallback->deleteSelf();
    m_sessionCallback = nullptr;

    ex.raise();
}

void OpcdoDatabasePrivate::connectErrorQueued(uint clientConnectionId,
                                              int serviceType,
                                              uint error,
                                              const QString& errorString,
                                              bool clientSideError)
{
    QString formatX("0x%1");
    QHash<QString, QString> errorInfo;

    errorInfo[OPCDOERRINFO_clientConnectionId] = QString::number(clientConnectionId);
    errorInfo[OPCDOERRINFO_error] = formatX.arg(QString::number(error, 16));
    errorInfo[OPCDOERRINFO_errorString] = errorString;
    errorInfo[OPCDOERRINFO_clientSideError] = clientSideError ? "true" : "false";

    QString typeString = OpcdoEnums::connectServiceTypeString(static_cast<OpcdoEnums::ConnectServiceType>(serviceType));
    errorInfo[OPCDOERRINFO_serviceType] = typeString;

    q->doConnectError(error, errorString, errorInfo);
}

void OpcdoDatabasePrivate::connectionStatusChanged(uint clientConnectionId,
                                                   int serverStatus)
{
    m_clientConnectionId = clientConnectionId;
    m_serverStatus = serverStatus;

    m_doBreakNoServer = m_serverStatus != OpcdoEnums::Connected;

    if (m_serverStatus == OpcdoEnums::Connected) {
        connectSubscription(static_cast<uint>(clientConnectionId));
    }
    else {
        disconnectSubsciption();
    }

    q->doConnectStatusChanged(clientConnectionId, serverStatus);
}

void OpcdoDatabasePrivate::subscriptionStatusChanged(uint clientSubscriptionHandle,
                                                     uint statusVal,
                                                     const QString& statusString)
{
    if (OpcUa_IsBad(statusVal)) {

        disconnectSubsciption();
        q->doSubscriptStatusError(clientSubscriptionHandle, statusVal, statusString);
    }
}

void OpcdoDatabasePrivate::subscriptionDataChangeError(uint clientSubscriptionHandle,
                                                       uint montoringId,
                                                       uint statusVal,
                                                       const QString& statusString)
{
    q->doSubscriptDataChangeError(clientSubscriptionHandle, montoringId, statusVal, statusString);
}

void OpcdoDatabasePrivate::subscriptionDataChange(uint clientSubscriptionHandle,
                                                  uint montoringId,
                                                  const QDateTime& modified,
                                                  const QVariant& varValue)
{
    q->doSubscriptDataChange(clientSubscriptionHandle, montoringId, modified, varValue);
}

int OpcdoDatabasePrivate::connect(const QString& url)
{
    QString serverUrl = url.trimmed();

    if (q->isConnected()) {

        OpcdoException ex(OPCDOERR_ALLREADY_CONNECTED, OpcdoDllClass::errorString(OPCDOERR_ALLREADY_CONNECTED));
        ex.raise();
        return OpcdoEnums::Disconnected;
    }

    if (serverUrl.isEmpty()) {

        OpcdoException ex(OPCDOERR_EMPTY_SERVER_URL, OpcdoDllClass::errorString(OPCDOERR_EMPTY_SERVER_URL));
        ex.raise();
        return OpcdoEnums::Disconnected;
    }

    UaStatus status;

    UaString sUrl(qPrintable(serverUrl));
    UaString sApplicationName(qPrintable(qApp->applicationName()));
    UaString sCompanyName(q->companyName().toUtf8().constData());
    UaString sProductName(q->productName().toUtf8().constData());

    UaString sNodeName("unknown_host");
    char szHostName[256];

    if (0 == UA_GetHostname(szHostName, 256)) {
        sNodeName = szHostName;
    }

    SessionConnectInfo sessionConnectInfo;
    sessionConnectInfo.clientConnectionId = rand();
    sessionConnectInfo.sApplicationName = sApplicationName;
    sessionConnectInfo.sApplicationUri  = UaString("urn:%1:%2:%3").arg(sNodeName).arg(sCompanyName).arg(sProductName);
    sessionConnectInfo.sProductUri      = UaString("urn:%1:%2").arg(sCompanyName).arg(sProductName);
    sessionConnectInfo.sSessionName     = sessionConnectInfo.sApplicationUri;
    sessionConnectInfo.bAutomaticReconnect = OpcUa_True;

    m_session = new UaSession();

    m_sessionCallback = new OpcuaSessionCallback;
    QObject::connect(m_sessionCallback, SIGNAL(affinityRestored()),
                     this, SLOT(sessionCallbackAffinityRestored()), Qt::DirectConnection);
    QObject::connect(m_sessionCallback, SIGNAL(connectionStatusChangedOccured(uint,int)),
                     this, SLOT(connectionStatusChanged(uint,int)), Qt::DirectConnection);
    QObject::connect(m_sessionCallback, SIGNAL(connectErrorOccured(uint,int,uint,QString,bool)),
                     this, SLOT(connectErrorDirect(uint,int,uint,QString,bool)), Qt::DirectConnection);

    m_sessionCallbackThread->start();
    m_sessionCallback->moveToThread(m_sessionCallbackThread);

    status = m_session->connect(
                sUrl,
                sessionConnectInfo,
                *m_sessionSecurityInfo,
                m_sessionCallback);

    if (status.isBad())
    {
        // an exception has allready been thrown
        // m_session no longer valid
        // m_sessionCallback no longer valid
        // m_sessionCallbackThread is quitting
        // code unreachable

        return OpcdoEnums::Disconnected;
    }

    // switch connection type due to automatic reconnection
    QObject::disconnect(m_sessionCallback, SIGNAL(connectionStatusChangedOccured(uint,int)),
                        this, SLOT(connectionStatusChanged(uint,int)));
    QObject::disconnect(m_sessionCallback, SIGNAL(connectErrorOccured(uint,int,uint,QString,bool)),
                        this, SLOT(connectErrorDirect(uint,int,uint,QString,bool)));

    QObject::connect(m_sessionCallback, SIGNAL(connectionStatusChangedOccured(uint,int)),
                     this, SLOT(connectionStatusChanged(uint,int)));
    QObject::connect(m_sessionCallback, SIGNAL(connectErrorOccured(uint,int,uint,QString,bool)),
                     this, SLOT(connectErrorQueued(uint,int,uint,QString,bool)));

    m_serverUrl = serverUrl;

//    connectSubscription(static_cast<uint>(sessionConnectInfo.clientConnectionId));

    return m_serverStatus;
}

int OpcdoDatabasePrivate::disconnect()
{
    if (m_session == nullptr) {

        Q_ASSERT(m_serverUrl.isEmpty());
        return OpcdoEnums::Disconnected;
    }

    disconnectSubsciption();
    unmonitor();

    ServiceSettings serviceSettings;

    m_session->disconnect(serviceSettings,    // Use default settings
                          OpcUa_True);

    delete m_session;
    m_session = nullptr;

    m_sessionCallback->deleteSelf();
    m_sessionCallback = nullptr;

    m_serverUrl.clear();

    return OpcdoEnums::Disconnected;
}

bool OpcdoDatabasePrivate::connectSubscription(uint subscriptionId)
{
    if (m_subscription) {
        return true;
    }

    UaStatus status;
    ServiceSettings serviceSettings;
    SubscriptionSettings subscriptionSettings;
    subscriptionSettings.publishingInterval = 100;

    m_subscriptionCallback = new OpcuaSubscriptionCallback;
    QObject::connect(m_subscriptionCallback, &OpcuaSubscriptionCallback::affinityRestored,
                     this, &OpcdoDatabasePrivate::subscriptionCallbackAffinityRestored, Qt::DirectConnection);

    UaClientSdk::UaSubscription* subscription = nullptr;

    status = m_session->createSubscription(
                serviceSettings,
                m_subscriptionCallback,
                static_cast<OpcUa_UInt32>(subscriptionId),
                subscriptionSettings,
                OpcUa_True,
                &subscription);

    if (status.isBad()) {

        delete m_subscriptionCallback;
        m_subscriptionCallback = nullptr;

        return false;
    }

    QObject::connect(m_subscriptionCallback, &OpcuaSubscriptionCallback::subscriptionStatusChangedOccured,
                     this, &OpcdoDatabasePrivate::subscriptionStatusChanged);
    QObject::connect(m_subscriptionCallback, &OpcuaSubscriptionCallback::dataChangeOccured,
                     this, &OpcdoDatabasePrivate::subscriptionDataChange);
    QObject::connect(m_subscriptionCallback, &OpcuaSubscriptionCallback::dataChangeErrorOccured,
                     this, &OpcdoDatabasePrivate::subscriptionDataChangeError);

    m_subscriptionCallbackThread->start();
    m_subscriptionCallback->moveToThread(m_subscriptionCallbackThread);

    setSubsciption(subscription);

    remonitor();

    return true;
}

void OpcdoDatabasePrivate::disconnectSubsciption()
{
    if (!m_subscription) {
        return;
    }

    UaStatus status;
    ServiceSettings serviceSettings;

    status = m_session->deleteSubscription(
                serviceSettings,
                &m_subscription);

    setSubsciption(nullptr);
    m_subscriptionCallback->deleteSelf();

    Q_UNUSED(status);
}

void OpcdoDatabasePrivate::setSubsciption(UaClientSdk::UaSubscription* subscription)
{
    if (subscription == m_subscription) {
        return;
    }

    m_subscription = subscription;
    q->doMonitoringStatusChanged();
}

OpcdoRecord* OpcdoDatabasePrivate::createRecord(const QString& nodeId, OpcdoEnums::CursorType cursorType)
{
    if (!q->isConnected()) {

        OpcdoException ex(OPCDOERR_NO_SESSION_CONNECTED, OpcdoDllClass::errorString(OPCDOERR_NO_SESSION_CONNECTED));
        ex.raise();
        return nullptr;
    }

    OpcdoRecord* record = new OpcdoRecord(q);
    OpcdoField& field = record->field();

    ClientServerDataExchange* exchange = new ClientServerDataExchange(cursorType, &field, nodeId, this);
    m_doBreakNoServer = false;

    QString sessionError;

    quint32 res = exchange->exec(sessionError);

    if (res != 0) {

        delete exchange;
        delete record;

        OpcdoException ex(res, sessionError.isEmpty() ? OpcdoDllClass::errorString(res) : sessionError);
        ex.raise();
        return nullptr;
    }

    delete exchange;

    return record;
}

QString OpcdoDatabasePrivate::valueString(const QString& nodeId)
{
    uint statusVal = 0;
    QString errorString;

    QString result = ClientServerDataExchange::readNodeValueString(m_session, nodeId, statusVal, errorString);

    if (statusVal) {

        OpcdoException ex(statusVal, errorString);
        ex.raise();
    }

    return result;
}

QString OpcdoDatabasePrivate::valueDataTypeString(const QString& nodeId)
{
    uint statusVal = 0;
    QString errorString;

    QString result = ClientServerDataExchange::readNodeValueDataTypeString(m_session, nodeId, statusVal, errorString);

    if (statusVal) {

        OpcdoException ex(statusVal, errorString);
        ex.raise();
    }

    return result;
}

QVariant OpcdoDatabasePrivate::value(const QString& nodeId, OpcdoEnums::DataType& dataType)
{
    uint statusVal = 0;
    QString errorString;

    QVariant result = ClientServerDataExchange::readNodeValueVariant(m_session, nodeId, statusVal, errorString, dataType);

    if (statusVal) {

        OpcdoException ex(statusVal, errorString);
        ex.raise();
    }

    return result;
}

bool OpcdoDatabasePrivate::writeValue(const QString& nodeId, const QVariant& varValue, OpcdoEnums::DataType dataType)
{
    uint statusVal = 0;
    QString errorString;

    bool result = ClientServerDataExchange::writeNodeValueVariant(m_session, nodeId, varValue, statusVal, errorString, dataType);

    if (statusVal) {

        OpcdoException ex(statusVal, errorString);
        ex.raise();
    }

    return result;
}

QString OpcdoDatabasePrivate::findMonitoringId(uint monitoringId)
{
    QHash<QString, QPair<uint, uint> >::const_iterator it = m_monitoredNodes.constBegin();

    while (it != m_monitoredNodes.constEnd()) {

        if (it.value().second == monitoringId) {
            return it.key();
        }

        ++it;
    }

    return QString();
}

void OpcdoDatabasePrivate::remonitor()
{
    if (!q->isConnected() || !m_subscription || m_monitoredNodes.isEmpty()) {
        return;
    }

    QString nodeId;
    uint monitoringId;
    QHash<QString, QPair<uint, uint> > monitoredNodes = m_monitoredNodes;
    m_monitoredNodes.clear();

    QHash<QString, QPair<uint, uint> >::const_iterator it = monitoredNodes.constBegin();

    while (it != monitoredNodes.constEnd()) {

        nodeId = it.key();
        monitoringId = it.value().second;

        monitorValue(nodeId, monitoringId);

        ++it;
    }
}

void OpcdoDatabasePrivate::unmonitor()
{
    if (m_subscription) {
        return;
    }

    m_monitoredNodes.clear();
}

bool OpcdoDatabasePrivate::isMonitored(const QString& nodeId)
{
    return m_monitoredNodes.contains(nodeId);
}

bool OpcdoDatabasePrivate::isMonitored(uint monitoringId)
{
    return !findMonitoringId(monitoringId).isEmpty();
}

bool OpcdoDatabasePrivate::monitorValue(const QString& nodeId, uint monitoringId)
{
    if (!q->isConnected()) {

        OpcdoException ex(OPCDOERR_NO_SESSION_CONNECTED, OpcdoDllClass::errorString(OPCDOERR_NO_SESSION_CONNECTED));
        ex.raise();
        return false;
    }

    if (!m_subscription) {

        OpcdoException ex(OPCDOERR_NO_SESSION_SUBSCRIPTION, OpcdoDllClass::errorString(OPCDOERR_NO_SESSION_SUBSCRIPTION));
        ex.raise();
        return false;
    }

    if (m_monitoredNodes.contains(nodeId)) {

        QString format = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_MONITORED_ARG_ARG);
        QPair<uint, uint> ids = m_monitoredNodes.value(nodeId);
        QString display = format.arg(nodeId).arg(ids.second);

        OpcdoException ex(OPCDOERR_EXCHANGE_MONITORED_ARG_ARG, display);
        ex.raise();
        return false;
    }

    QString testNode = findMonitoringId(monitoringId);

    if (!testNode.isEmpty()) {

        QString format = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_MONITORED_ARG_ARG);
        QString display = format.arg(monitoringId).arg(testNode);

        OpcdoException ex(OPCDOERR_EXCHANGE_MONITORED_ARG_ARG, display);
        ex.raise();
        return false;
    }

    UaStatus status;
    ServiceSettings serviceSettings;
    UaMonitoredItemCreateRequests itemsToCreate;
    UaMonitoredItemCreateResults createResults;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    itemsToCreate.create(1);
    itemsToCreate[0].ItemToMonitor.AttributeId = OpcUa_Attributes_Value;
    id.copyTo(&itemsToCreate[0].ItemToMonitor.NodeId);
    itemsToCreate[0].RequestedParameters.ClientHandle = monitoringId;
    itemsToCreate[0].RequestedParameters.SamplingInterval = 100;
    itemsToCreate[0].RequestedParameters.QueueSize = 1;
    itemsToCreate[0].RequestedParameters.DiscardOldest = OpcUa_True;
    itemsToCreate[0].MonitoringMode = OpcUa_MonitoringMode_Reporting;

    status = m_subscription->createMonitoredItems(
                serviceSettings,
                OpcUa_TimestampsToReturn_Server,
                itemsToCreate,
                createResults);

    if (status.isBad()) {

        OpcdoException ex(status.statusCode(), status.toString().toUtf8());
        ex.raise();
        return false;
    }

    if (createResults.length() < 1) {

        Q_ASSERT(false);
        return false;
    }

    status = UaStatus(createResults[0].StatusCode);

    if (status.isBad()) {

        OpcdoException ex(status.statusCode(), status.toString().toUtf8());
        ex.raise();
        return false;
    }

    uint monitoredId = createResults[0].MonitoredItemId;

    QPair<uint, uint> ids(monitoredId, monitoringId);
    m_monitoredNodes[nodeId] = ids;

    return true;
}

bool OpcdoDatabasePrivate::unmonitorValue(uint monitoringId)
{
    QString testNode = findMonitoringId(monitoringId);

    if (testNode.isEmpty()) {
        return true;
    }

    if (!m_subscription) {

        m_monitoredNodes.remove(testNode);
        return true;
    }

    QPair<uint, uint> ids = m_monitoredNodes.value(testNode);

    UaStatus status;
    ServiceSettings serviceSettings;
    UaUInt32Array monitoredItemIds;
    UaStatusCodeArray results;

    monitoredItemIds.create(1);
    monitoredItemIds[0] = ids.first;

    status = m_subscription->deleteMonitoredItems(
                serviceSettings,
                monitoredItemIds,
                results);

    if (status.isBad()) {

        OpcdoException ex(status.statusCode(), status.toString().toUtf8());
        ex.raise();
        return false;
    }

    if (results.length() < 1) {

        Q_ASSERT(false);
        return false;
    }

    status = UaStatus(results[0]);

    if (status.isBad()) {

        OpcdoException ex(status.statusCode(), status.toString().toUtf8());
        ex.raise();
        return false;
    }

    m_monitoredNodes.remove(testNode);

    return true;
}
