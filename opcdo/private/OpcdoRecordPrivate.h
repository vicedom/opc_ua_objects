#ifndef OPCDORECORDPRIVATE_H
#define OPCDORECORDPRIVATE_H

#include <QObject>

class OpcdoRecord;
class OpcdoDatabase;
class OpcdoField;

class OpcdoRecordPrivate : public QObject
{
    Q_OBJECT
public:
    explicit OpcdoRecordPrivate(OpcdoDatabase* database, OpcdoRecord* parent = nullptr);
    ~OpcdoRecordPrivate() override;

signals:

public slots:

private:
    OpcdoDatabase* m_database;
    OpcdoField* m_field;

    OpcdoRecord* q;
    friend class OpcdoRecord;
};

#endif // OPCDORECORDPRIVATE_H
