#ifndef OPCDOFIELDPRIVATE_H
#define OPCDOFIELDPRIVATE_H

#include <QVector>
#include <QVariant>

class OpcdoField;

class OpcdoFieldPrivate
{
public:
    explicit OpcdoFieldPrivate(OpcdoField* parent, OpcdoField* parentField = nullptr);
    ~OpcdoFieldPrivate();

    OpcdoField *child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;

private:
    void makeNaked();

private:
    QVector<OpcdoField*> m_childItems;
    OpcdoField* m_parentField;

    QString m_nodeId;
    int m_nodeClass;
    QString m_displayName;
    int m_nameSpace;
    QVariant m_value;   // return value

    bool m_expandProceeded;

    OpcdoField* q;
    friend class OpcdoField;
    friend class ClientServerDataExchange;
};

#endif // OPCDOFIELDPRIVATE_H
