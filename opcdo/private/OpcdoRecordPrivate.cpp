#include "OpcdoRecordPrivate.h"
#include "OpcdoField.h"
#include "OpcdoRecord.h"
#include "OpcdoDatabase.h"
#include "OpcdoException.h"
#include "opcdodllerrors.h"
#include "opcdodllclass/OpcdoDllClass.h"

OpcdoRecordPrivate::OpcdoRecordPrivate(OpcdoDatabase* database, OpcdoRecord* parent)
    : QObject(parent)
    , m_database(database)
    , m_field(new OpcdoField)
    , q(parent)
{
    Q_ASSERT(q != nullptr);

    setObjectName("OpcdoRecordPrivate");

    opcdodllclass::OpcdoDllClass::registerPrivate(this);
}

OpcdoRecordPrivate::~OpcdoRecordPrivate()
{
    delete m_field;
    opcdodllclass::OpcdoDllClass::unregisterPrivate(this);
}
