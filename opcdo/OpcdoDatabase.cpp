#include "OpcdoDatabase.h"
#include "OpcdoDatabasePrivate.h"

#include <QVariant>


OpcdoDatabase::OpcdoDatabase(QObject* parent)
    : QObject(parent)
    , d(new OpcdoDatabasePrivate(this))

{
    setCompanyName();
    setProductName();
}

OpcdoDatabase::~OpcdoDatabase()
{
    disconnect();
}

QString OpcdoDatabase::companyName() const
{
    return d->m_companyName;
}

void OpcdoDatabase::setCompanyName(const QString& companyName)
{
    d->setCompanyName(companyName);
}

QString OpcdoDatabase::productName() const
{
    return d->m_productName;
}

void OpcdoDatabase::setProductName(const QString& productName)
{
    d->setProductName(productName);
}

QString OpcdoDatabase::serverUrl() const
{
    return d->m_serverUrl;
}

bool OpcdoDatabase::isConnected() const
{
    return !d->m_serverUrl.isEmpty();
}

bool OpcdoDatabase::isOnline() const
{
    return d->m_serverStatus == OpcdoEnums::Connected;
}

bool OpcdoDatabase::canMonitor() const
{
    return d->m_subscription != nullptr;
}

quint32 OpcdoDatabase::clientConnectionId() const
{
    return d->m_clientConnectionId;
}

OpcdoEnums::ServerStatus OpcdoDatabase::connect(const QString& serverUrl)
{
    return static_cast<OpcdoEnums::ServerStatus>(d->connect(serverUrl));
}

OpcdoEnums::ServerStatus OpcdoDatabase::disconnect()
{
    return static_cast<OpcdoEnums::ServerStatus>(d->disconnect());
}

OpcdoRecord* OpcdoDatabase::createRecord(const QString& nodeId, OpcdoEnums::CursorType cursor)
{
    return d->createRecord(nodeId, cursor);
}

QString OpcdoDatabase::valueString(const QString& nodeId)
{
    QString result = d->valueString(nodeId);
    result.replace(QChar(','), QChar('|'));
    result.replace("'|'", QChar('|'));
    result.replace("{'", QChar('{'));
    result.replace("'}", QChar('}'));

    return result;
}

QString OpcdoDatabase::valueDataTypeString(const QString& nodeId)
{
    return d->valueDataTypeString(nodeId);
}

QVariant OpcdoDatabase::value(const QString& nodeId, OpcdoEnums::DataType &dataType)
{
    return d->value(nodeId, dataType);
}

bool OpcdoDatabase::writeValue(const QString& nodeId, const QVariant& varValue, OpcdoEnums::DataType dataType)
{
    return d->writeValue(nodeId, varValue, dataType);
}

bool OpcdoDatabase::monitorValue(const QString& nodeId, uint monitoringId)
{
    return d->monitorValue(nodeId, monitoringId);
}

bool OpcdoDatabase::unmonitorValue(uint monitoringId)
{
    return d->unmonitorValue(monitoringId);
}

bool OpcdoDatabase::isMonitored(const QString& nodeId)
{
    return d->isMonitored(nodeId);
}

bool OpcdoDatabase::isMonitored(uint monitoringId)
{
    return d->isMonitored(monitoringId);
}

void OpcdoDatabase::doConnectStatusChanged(uint clientConnectionId, int serverStatus)
{
    emit connectStatusChanged(clientConnectionId, static_cast<OpcdoEnums::ServerStatus>(serverStatus));
}

void OpcdoDatabase::doConnectError(quint32 reason, const QString& message, const QHash<QString, QString>& errorInfo)
{
    emit connectError(reason, message, errorInfo);
}

void OpcdoDatabase::doSubscriptStatusError(uint clientSubscriptionHandle, quint32 reason, const QString& message)
{
    emit subscriptStatusError(clientSubscriptionHandle, reason, message);
}

void OpcdoDatabase::doSubscriptDataChangeError(uint clientSubscriptionHandle, uint montoringId, uint statusVal, const QString& statusString)
{
    emit subscriptDataChangeError(clientSubscriptionHandle, montoringId, statusVal, statusString);
}

void OpcdoDatabase::doSubscriptDataChange(uint clientSubscriptionHandle, uint montoringId, const QDateTime& modified, const QVariant& varValue)
{
    emit subscriptDataChange(clientSubscriptionHandle, montoringId, modified, varValue);
}

void OpcdoDatabase::doMonitoringStatusChanged()
{
    emit monitoringStatusChanged();
}
