#include "OpcdoValueStringParser.h"
#include "opcdodllerrors.h"
#include "OpcdoException.h"
#include "opcdodllclass/OpcdoDllClass.h"

#include <QStringList>

using namespace opcdodllclass;

static QChar leftBrace('{');
static QChar rightBrace('}');
static QChar pipe('|');
static QString matrixSep("}|{");
static QString dimFormat("[%1]");

static int numberLeftBraces(const QString& normalized)
{
    int i, k = 0, len = normalized.length();

    for (i = 0; i < len; i++) {

        if (normalized.at(i) != leftBrace) {
            break;
        }

        k++;
    }

    return k;
}

static int numberRightBraces(const QString& normalized)
{
    int i, k = 0, len = normalized.length();

    for (i = len - 1; i >= 0; i--) {

        if (normalized.at(i) != rightBrace) {
            break;
        }

        k++;
    }

    return k;
}

static int valueRank(const QString& normalized)
{
    int rightBraces = numberRightBraces(normalized);
    int leftBraces = numberLeftBraces(normalized);

    if (leftBraces != rightBraces) {
        return -1;
    }

    return leftBraces;
}

static QString leftBracer(int rank)
{
    if (rank < 1) {
        return QString();
    }

    return QString(rank, leftBrace);
}

static QString rightBracer(int rank)
{
    if (rank < 1) {
        return QString();
    }

    return QString(rank, rightBrace);
}

static QString valueRankSeperator(int rank)
{
    QString result = rightBracer(rank) + pipe + leftBracer(rank);
    return result;
}

QVariant OpcdoValueStringParser::parse(const QString& parse, const OpcdoEnums::DataType type)
{
    QString parseString = normalize(parse);

    if (parseString.isEmpty()) {
        return QVariant(static_cast<QVariant::Type>(OpcdoEnums::fromDataType(type))); //isNull !!! must be evaluated: valueRank unknown !!!
    }

    int rank = valueRank(parseString);

    if (rank < 0) {

        OpcdoException ex(OPCDOERR_PARSE_RANK_INVALID, OpcdoDllClass::errorString(OPCDOERR_PARSE_RANK_INVALID));
        ex.raise();

        return QVariant();
    }

    if (rank == 0) {
        return parseScalar(parseString, type);
    }    
    else {
        return parseMultiDimension(parseString, type, rank);
    }
}

QVariant OpcdoValueStringParser::parseScalar(const QString& parseString, const OpcdoEnums::DataType type, const QString &dimdesc)
{
    QVariant probe = parseString;
    QMetaType::Type mtype = OpcdoEnums::fromDataType(type);

    if (probe.canConvert(mtype) && probe.convert(mtype)) {
        return probe;
    }

    OpcdoException ex;    

    if (dimdesc.isEmpty()) {

        ex.setReason(OPCDOERR_PARSE_VALUE_PARSE);
        ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_PARSE_VALUE_PARSE));
    }
    else {

        ex.setReason(OPCDOERR_PARSE_VALUE_PARSE_ARG);
        ex.setErrorString(OpcdoDllClass::errorString(OPCDOERR_PARSE_VALUE_PARSE_ARG).arg(dimdesc));
    }

    ex.raise();
    return QVariant();
}

QVariant OpcdoValueStringParser::parseDimension(const QString& parseString, OpcdoEnums::DataType type, int rank, const QString& dimdesc)
{
    QString splitString = valueRankSeperator(rank - 1);

    QString startLeftBraces(rank - 1, leftBrace);
    QString endRightBraces(rank - 1, rightBrace);
    QString replaceStartLeftBraces(rank - 2, leftBrace);
    QString replaceEndRightBraces(rank - 2, rightBrace);
    QStringList parts = parseString.split(splitString);
    int i, len = parts.length();

    for (i = 0; i < len; i++) {        

        parts[i].replace(startLeftBraces, replaceStartLeftBraces);
        parts[i].replace(endRightBraces, replaceEndRightBraces);

        if (!parts.at(i).startsWith(replaceStartLeftBraces)) {
            parts[i].prepend(replaceStartLeftBraces);
        }

        if (!parts.at(i).endsWith(replaceEndRightBraces)) {
            parts[i].append(replaceEndRightBraces);
        }
    }

    QVariantList values;
    QVariant result;
    QString ddesc;

    for (i = 0; i < len; i++) {

        ddesc = dimdesc + dimFormat.arg(i);

        if (rank == 1) {
            values.append(parseScalar(parts.at(i), type, ddesc));
        }
        else {
            values.append(parseDimension(parts.at(i), type, rank - 1, ddesc));
        }
    }

    result.setValue<QVariantList>(values);
    return result;
}

QVariant OpcdoValueStringParser::parseMultiDimension(const QString& parse, OpcdoEnums::DataType type, int rank)
{
    QString parseString = parse.mid(0, parse.lastIndexOf(rightBrace)).mid(1);
    return parseDimension(parseString, type, rank);
}

QString OpcdoValueStringParser::normalize(const QString& inputString)
{
    QString result;
    QString input = inputString.trimmed();

    if (input.isEmpty()) {
        return result;
    }

    QStringList parts = input.split(pipe);
    int i, len = parts.length();

    for (i = 0; i < len; i++) {
        parts[i] = parts.at(i).trimmed();
    }

    input = parts.join(pipe);
    parts = input.split(leftBrace);
    len = parts.length();

    for (i = 0; i < len; i++) {
        parts[i] = parts.at(i).trimmed();
    }

    input = parts.join(leftBrace);

    parts = input.split(rightBrace);
    len = parts.length();

    for (i = 0; i < len; i++) {
        parts[i] = parts.at(i).trimmed();
    }

    result = parts.join(rightBrace);

    return result;
}

