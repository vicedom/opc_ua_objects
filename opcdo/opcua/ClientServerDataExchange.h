#ifndef CLIENTSERVERDATAEXCHANGE_H
#define CLIENTSERVERDATAEXCHANGE_H

#include "OpcdoField.h"
#include "OpcdoFieldPrivate.h"
#include "OpcdoDatabase.h"
#include "OpcdoDatabasePrivate.h"

#include <QObject>

namespace UaClientSdk {
    class UaSession;
    class ServiceSettings;
}

// low level api
class ClientServerDataExchange : public QObject
{
    Q_OBJECT
public:
    explicit ClientServerDataExchange(OpcdoEnums::CursorType cursorType,
                                      OpcdoField* root,
                                      const QString& nodeId,
                                      OpcdoDatabasePrivate* parent);
    ~ClientServerDataExchange() override;

    quint32 exec(QString& sessionError); // synchronous
    //void run(); // asynchronous


    static QString readNodeValueString(UaClientSdk::UaSession* session,
                                       const QString& nodeId,
                                       uint& statusVal,
                                       QString& errorString,
                                       UaClientSdk::ServiceSettings* settings = nullptr);

    static QVariant readNodeValueVariant(UaClientSdk::UaSession* session,
                                         const QString& nodeId,
                                         uint& statusVal,
                                         QString& errorString,
                                         OpcdoEnums::DataType& dataType,
                                         UaClientSdk::ServiceSettings* settings = nullptr);

    static QString readNodeValueDataTypeString(UaClientSdk::UaSession* session,
                                               const QString& nodeId,
                                               uint& statusVal,
                                               QString& errorString,
                                               UaClientSdk::ServiceSettings* settings = nullptr);
    static QVariant readNodeValueDataTypeVariant(UaClientSdk::UaSession* session,
                                                 const QString& nodeId,
                                                 uint& statusVal,
                                                 QString& errorString,
                                                 UaClientSdk::ServiceSettings* settings = nullptr);



    static bool writeNodeValueVariant(UaClientSdk::UaSession* session,
                                      const QString& nodeId,
                                      const QVariant& varValue,
                                      uint& statusVal,
                                      QString& errorString,
                                      const OpcdoEnums::DataType dataType,
                                      UaClientSdk::ServiceSettings* settings = nullptr);

private:
    static bool browseNodeTreeChild(UaClientSdk::UaSession* session,
                                    const QString& nodeId,
                                    OpcdoFieldPrivate* field,
                                    bool deep,
                                    bool readValue,
                                    bool& doBreakConnectionBroken,
                                    uint& statusVal,
                                    QString& errorString,
                                    UaClientSdk::ServiceSettings* settings,
                                    int level);

    static void exploreNode(UaClientSdk::UaSession* session,
                            const QString& nodeId,
                            OpcdoFieldPrivate* root,
                            bool deep,
                            bool readValue,
                            bool& doBreakConnectionBroken,
                            uint& status,
                            QString& statusString);

    static QString readNodeValueAttributeString(UaClientSdk::UaSession* session,
                                                const QString& nodeId,
                                                uint attribute,
                                                uint& statusVal,
                                                QString& errorString,
                                                UaClientSdk::ServiceSettings* settings);

    static QVariant readNodeValueAttributeVariant(UaClientSdk::UaSession* session,
                                                const QString& nodeId,
                                                uint attribute,
                                                uint& statusVal,
                                                QString& errorString,
                                                OpcdoEnums::DataType& dtype,
                                                UaClientSdk::ServiceSettings* settings);

    static bool writeNodeValueVariantVariantMatrix(UaClientSdk::UaSession* session,
                                                   const QString& nodeId,
                                                   const QVariantList& varList,
                                                   uint& statusVal,
                                                   QString& errorString,
                                                   const OpcdoEnums::DataType dataType,
                                                   UaClientSdk::ServiceSettings* settings);

    static bool writeNodeValueVariantVariantList(UaClientSdk::UaSession* session,
                                                 const QString& nodeId,
                                                 const QVariantList& varList,
                                                 uint& statusVal,
                                                 QString& errorString,
                                                 const OpcdoEnums::DataType dataType,
                                                 UaClientSdk::ServiceSettings* settings);

    static bool writeNodeValueVariantVariant(UaClientSdk::UaSession* session,
                                             const QString& nodeId,
                                             const QVariant& varValue,
                                             uint& statusVal,
                                             QString& errorString,
                                             const OpcdoEnums::DataType dataType,
                                             UaClientSdk::ServiceSettings* settings);

private:
    OpcdoDatabasePrivate* m_dbPrivate;
    QString m_nodeId;
    OpcdoField* m_root;
    OpcdoEnums::CursorType m_cursorType;
};

#endif // CLIENTSERVERDATAEXCHANGE_H
