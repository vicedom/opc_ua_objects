#include "OpcuaSubscriptionCallback.h"
#include "opcdodllerrors.h"
#include "opcdodllclass/OpcdoDllClass.h"
#include "OpcdoEnums.h"
#include "OpcuaVariant.h"

#include <QDateTime>
#include <QCoreApplication>
#include <QThread>
#include <QAbstractEventDispatcher>

#include <QDebug>

using namespace UaClientSdk;
using namespace opcdodllclass;

OpcuaSubscriptionCallback::OpcuaSubscriptionCallback(QObject *parent)
    : QObject(parent)
{
    connect(this, &OpcuaSubscriptionCallback::internalDelete, this, &OpcuaSubscriptionCallback::deleteInternal);
}

OpcuaSubscriptionCallback::~OpcuaSubscriptionCallback()
{
#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor OpcuaSubscriptionCallback::~OpcuaSubscriptionCallback called";
#endif
}

void OpcuaSubscriptionCallback::deleteSelf()
{
    emit internalDelete();
}

void OpcuaSubscriptionCallback::deleteInternal()
{
    QThread* mainThread = qApp->thread();
    QThread* ownThread = thread();

    if (ownThread != mainThread) {

        moveToThread(mainThread);
        emit affinityRestored();
    }

    deleteLater();
}

void OpcuaSubscriptionCallback::subscriptionStatusChanged(OpcUa_UInt32 clientSubscriptionHandle,
                                                          const UaStatus& status)
{
    uint statusVal = status.statusCode();
    QString statusString = status.toString().toUtf8();

    emit subscriptionStatusChangedOccured(static_cast<uint>(clientSubscriptionHandle),
                                          statusVal,
                                          statusString);
}

void OpcuaSubscriptionCallback::dataChange(OpcUa_UInt32 clientSubscriptionHandle,
                                           const UaDataNotifications& dataNotifications,
                                           const UaDiagnosticInfos& /*diagnosticInfos*/)
{
    uint subscriptionHandle = static_cast<uint>(clientSubscriptionHandle);
    uint monitoringId;
    uint statusVal;
    QString errorString;
    uint i, len = static_cast<uint>(dataNotifications.length());

    for (i = 0; i < len; i++) {

        const OpcUa_MonitoredItemNotification& notification = dataNotifications[i];
        monitoringId = static_cast<uint>(notification.ClientHandle);
        OpcUa_DataValue val = notification.Value;
        UaStatus status(val.StatusCode);

        if (status.isBad()) {

            statusVal = status.statusCode();
            errorString = status.toString().toUtf8();

            emit dataChangeErrorOccured(subscriptionHandle,
                                        monitoringId,
                                        statusVal,
                                        errorString);
        }
        else {

            QVariant result;
            OpcdoEnums::DataType dtype = OpcdoEnums::Unknown;

            if (val.Value.ArrayType == OpcUa_VariantArrayType_Scalar) {

                result = toQVariant(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_VALUE;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_VALUE);
                    emit dataChangeErrorOccured(subscriptionHandle,
                                                monitoringId,
                                                statusVal,
                                                errorString);
                    continue;
                }
            }
            else if (val.Value.ArrayType == OpcUa_VariantArrayType_Array) {

                QVariantList list = toQVariantArray(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_ARRAY;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_ARRAY);
                    emit dataChangeErrorOccured(subscriptionHandle,
                                                monitoringId,
                                                statusVal,
                                                errorString);
                    continue;
                }

                result.setValue<QVariantList>(list);
            }
            else {

                QVariantList list = toQVariantMatrix(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_MATRIX;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_MATRIX);
                    emit dataChangeErrorOccured(subscriptionHandle,
                                                monitoringId,
                                                statusVal,
                                                errorString);
                    continue;
                }

                result.setValue<QVariantList>(list);
            }

            UaDateTime uadt = val.ServerTimestamp;
            UaString string = uadt.toString();
            QString utf8String = string.toUtf8();
            QDateTime qdt = QDateTime::fromString(utf8String, Qt::ISODateWithMs);

            emit dataChangeOccured(subscriptionHandle,
                                   monitoringId,
                                   qdt,
                                   result);
        }
    }
}

void OpcuaSubscriptionCallback::newEvents(OpcUa_UInt32 /*clientSubscriptionHandle*/,
                                          UaEventFieldLists& /*eventFieldList*/)
{

}
