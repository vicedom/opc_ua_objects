#include "OpcuaVariant.h"

#include "uaclientsdk.h"

#include <QDateTime>
#include <QMetaType>
#include <QVector>

static QVariantList q_variantMatrix(const QVector<int>& baseSizes, OpcUa_VariantArrayUnion data, int dim, qulonglong& pos, OpcdoEnums::DataType& dtype);
static QVariantList q_variantMatrixData(int baseSize, OpcUa_VariantArrayUnion data, qulonglong& pos, OpcdoEnums::DataType& dtype);
static OpcUa_Boolean* q_variantMatrixBoolean(QVariantList& list, OpcUa_Boolean* arrayPtr, int len);
static OpcUa_DateTime* q_variantMatrixDateTime(QVariantList& list, OpcUa_DateTime* arrayPtr, int len);
static OpcUa_Int16* q_variantMatrixInt16(QVariantList& list, OpcUa_Int16* arrayPtr, int len);
static OpcUa_Int32* q_variantMatrixInt32(QVariantList& list, OpcUa_Int32* arrayPtr, int len);
static OpcUa_Int64* q_variantMatrixInt64(QVariantList& list, OpcUa_Int64* arrayPtr, int len);
static OpcUa_Byte* q_variantMatrixByte(QVariantList& list, OpcUa_Byte* arrayPtr, int len);
static OpcUa_SByte* q_variantMatrixSByte(QVariantList& list, OpcUa_SByte* arrayPtr, int len);
static OpcUa_UInt16* q_variantMatrixUInt16(QVariantList& list, OpcUa_UInt16* arrayPtr, int len);
static OpcUa_UInt32* q_variantMatrixUInt32(QVariantList& list, OpcUa_UInt32* arrayPtr, int len);
static OpcUa_UInt64* q_variantMatrixUInt64(QVariantList& list, OpcUa_UInt64* arrayPtr, int len);
static OpcUa_Float* q_variantMatrixFloat(QVariantList& list, OpcUa_Float* arrayPtr, int len);
static OpcUa_Double* q_variantMatrixDouble(QVariantList& list, OpcUa_Double* arrayPtr, int len);
static OpcUa_String* q_variantMatrixString(QVariantList& list, OpcUa_String* arrayPtr, int len);

OpcUa_VariantArrayUnion arrayValueFromDataType(int len, const OpcdoEnums::DataType dtype)
{
    OpcUa_VariantArrayUnion u;

    switch (dtype) {
    case OpcdoEnums::Boolean:
        u.BooleanArray = (OpcUa_Boolean*)calloc(len, sizeof(OpcUa_Boolean)); break;
    case OpcdoEnums::DateTime:
        u.DateTimeArray = (OpcUa_DateTime*)calloc(len, sizeof(OpcUa_DateTime)); break;
    case OpcdoEnums::Int16:
        u.Int16Array = (OpcUa_Int16*)calloc(len, sizeof(OpcUa_Int16)); break;
    case OpcdoEnums::Int32:
        u.Int32Array = (OpcUa_Int32*)calloc(len, sizeof(OpcUa_Int32)); break;
    case OpcdoEnums::Int64:
        u.Int64Array = (OpcUa_Int64*)calloc(len, sizeof(OpcUa_Int64)); break;
    case OpcdoEnums::Byte:
        u.ByteArray = (OpcUa_Byte*)calloc(len, sizeof(OpcUa_Byte)); break;
    case OpcdoEnums::SByte:
        u.SByteArray = (OpcUa_SByte*)calloc(len, sizeof(OpcUa_SByte)); break;
    case OpcdoEnums::UInt16:
        u.UInt16Array = (OpcUa_UInt16*)calloc(len, sizeof(OpcUa_UInt16)); break;
    case OpcdoEnums::UInt32:
        u.UInt32Array = (OpcUa_UInt32*)calloc(len, sizeof(OpcUa_UInt32)); break;
    case OpcdoEnums::UInt64:
        u.UInt64Array = (OpcUa_UInt64*)calloc(len, sizeof(OpcUa_UInt64)); break;
    case OpcdoEnums::Float:
        u.FloatArray = (OpcUa_Float*)calloc(len, sizeof(OpcUa_Float)); break;
    case OpcdoEnums::Double:
        u.DoubleArray = (OpcUa_Double*)calloc(len, sizeof(OpcUa_Double)); break;
    case OpcdoEnums::String:
        u.StringArray = (OpcUa_String*)calloc(len, sizeof(OpcUa_String)); break;
    default:
        u.Array = nullptr;
        break;
    }

    return u;
}

QVariant toQVariant(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype)
{
    dtype = OpcdoEnums::Unknown;

    if (value.ArrayType != OpcUa_VariantArrayType_Scalar) {
        return QVariant();
    }

    dtype = static_cast<OpcdoEnums::DataType>(static_cast<int>(value.Datatype));
    QVariant result;

    switch (dtype) {
    case OpcdoEnums::Boolean:
    {
        bool set = value.Value.Boolean ? true : false;
        result.setValue<bool>(set);
    }
        break;
    case OpcdoEnums::DateTime:
    {
        QString utf8String = UaDateTime(value.Value.DateTime).toString().toUtf8();
        result = QVariant(QDateTime::fromString(utf8String, Qt::ISODate));
    }
        break;
    case OpcdoEnums::Int16:
        result.setValue<int>(static_cast<int>(value.Value.Int16)); break;
    case OpcdoEnums::Int32:
        result.setValue<int>(static_cast<int>(value.Value.Int32)); break;
    case OpcdoEnums::Int64:
        result.setValue<qlonglong>(static_cast<qlonglong>(value.Value.Int64)); break;
    case OpcdoEnums::Byte:
        result.setValue<uint>(static_cast<uint>(value.Value.Byte)); break;
    case OpcdoEnums::SByte:
        result.setValue<int>(static_cast<int>(value.Value.SByte)); break;
    case OpcdoEnums::UInt16:
        result.setValue<uint>(static_cast<uint>(value.Value.UInt16)); break;
    case OpcdoEnums::UInt32:
        result.setValue<uint>(static_cast<uint>(value.Value.UInt32)); break;
    case OpcdoEnums::UInt64:
        result.setValue<qulonglong>(static_cast<qulonglong>(value.Value.UInt64)); break;
    case OpcdoEnums::Float:
        result.setValue<float>(static_cast<float>(value.Value.Float)); break;
    case OpcdoEnums::Double:
        result.setValue<double>(static_cast<double>(value.Value.Double)); break;
    case OpcdoEnums::String:
    {
        QString utf8String = UaString(value.Value.String).toUtf8();
        result.setValue<QString>(utf8String);
    }
        break;
    default:
        dtype = OpcdoEnums::Unknown;
        break;
    }

    return result;
}

QVariantList toQVariantArray(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype)
{
    dtype = OpcdoEnums::Unknown;

    if (value.ArrayType != OpcUa_VariantArrayType_Array) {
        return QVariantList();
    }

    dtype = static_cast<OpcdoEnums::DataType>(static_cast<int>(value.Datatype));
    QVariantList result;
    int i, len = value.Value.Array.Length;

    for (i = 0; i < len; i++) {

        QVariant varValue;

        switch (dtype) {
        case OpcdoEnums::Boolean:
        {
            bool set = value.Value.Array.Value.BooleanArray[i] ? true : false;
            varValue.setValue<bool>(set);
        }
            break;
        case OpcdoEnums::DateTime:
        {
            QString utf8String = UaDateTime(value.Value.Array.Value.DateTimeArray[i]).toString().toUtf8();
            varValue = QVariant(QDateTime::fromString(utf8String, Qt::ISODate));
        }
        case OpcdoEnums::Int16:
            varValue.setValue<int>(static_cast<int>(value.Value.Array.Value.Int16Array[i])); break;
        case OpcdoEnums::Int32:
            varValue.setValue<int>(static_cast<int>(value.Value.Array.Value.Int32Array[i])); break;
        case OpcdoEnums::Int64:
            varValue.setValue<qlonglong>(static_cast<qlonglong>(value.Value.Array.Value.Int64Array[i])); break;
        case OpcdoEnums::Byte:
            varValue.setValue<uint>(static_cast<uint>(value.Value.Array.Value.ByteArray[i])); break;
        case OpcdoEnums::SByte:
            varValue.setValue<int>(static_cast<int>(value.Value.Array.Value.SByteArray[i])); break;
        case OpcdoEnums::UInt16:
            varValue.setValue<uint>(static_cast<uint>(value.Value.Array.Value.UInt16Array[i])); break;
        case OpcdoEnums::UInt32:
            varValue.setValue<uint>(static_cast<uint>(value.Value.Array.Value.UInt32Array[i])); break;
        case OpcdoEnums::UInt64:
            varValue.setValue<qulonglong>(static_cast<qulonglong>(value.Value.Array.Value.UInt64Array[i])); break;
        case OpcdoEnums::Float:
            varValue.setValue<float>(static_cast<float>(value.Value.Array.Value.FloatArray[i])); break;
        case OpcdoEnums::Double:
            varValue.setValue<double>(static_cast<double>(value.Value.Array.Value.DoubleArray[i])); break;
        case OpcdoEnums::String:
        {
            QString utf8String = UaString(value.Value.Array.Value.StringArray[i]).toUtf8();
            varValue.setValue<QString>(utf8String);
        }
            break;
        default:

            dtype = OpcdoEnums::Unknown;
            break;
        }

        result.append(varValue);
    }

    return result;
}

QVariantList toQVariantMatrix(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype)
{
    dtype = OpcdoEnums::Unknown;

    if (value.ArrayType < OpcUa_VariantArrayType_Matrix) {
        return toQVariantArray(value, dtype);
    }

    dtype = static_cast<OpcdoEnums::DataType>(static_cast<int>(value.Datatype));
    OpcUa_VariantMatrixValue matrix = value.Value.Matrix;
    int dimensions = static_cast<int>(matrix.NoOfDimensions);
    OpcUa_Int32* baseSizesPtr = matrix.Dimensions;
    QVector<int> baseSizes;
    int i;

    for (i = 0; i < dimensions; i++) {

        baseSizes.append(static_cast<int>(*baseSizesPtr));
        baseSizesPtr++;
    }

    OpcUa_VariantArrayUnion data = matrix.Value;
    qulonglong pos = 0;
    int dim = dimensions - 1;

    QVariantList result = q_variantMatrix(baseSizes, data, dim, pos, dtype);

    return result;
}

QVariantList q_variantMatrix(const QVector<int>& baseSizes, OpcUa_VariantArrayUnion data, int dim, qulonglong& pos, OpcdoEnums::DataType& dtype)
{
    int i, baseLength = baseSizes.at(dim);

    if (dim == 0) {
        return q_variantMatrixData(baseLength, data, pos, dtype);
    }

    QVariantList list;
    QVariant varRes;

    for (i = 0; i < baseLength; i++) {

        QVariantList res = q_variantMatrix(baseSizes, data, dim - 1, pos, dtype);

        if (dtype == OpcdoEnums::Unknown) {
            return QVariantList();
        }

        varRes.setValue<QVariantList>(res);
        list.append(varRes);
    }

    return list;
}

QVariantList q_variantMatrixData(int baseLength, OpcUa_VariantArrayUnion data, qulonglong& pos, OpcdoEnums::DataType& dtype)
{
    QVariantList list;

    switch (dtype)
    {
    case OpcdoEnums::Boolean:
        q_variantMatrixBoolean(list, data.BooleanArray + pos, baseLength); break;
    case OpcdoEnums::DateTime:
        q_variantMatrixDateTime(list, data.DateTimeArray + pos, baseLength); break;
    case OpcdoEnums::Int16:
        q_variantMatrixInt16(list, data.Int16Array + pos, baseLength); break;
    case OpcdoEnums::Int32:
        q_variantMatrixInt32(list, data.Int32Array + pos, baseLength); break;
    case OpcdoEnums::Int64:
        q_variantMatrixInt64(list, data.Int64Array + pos, baseLength); break;
    case OpcdoEnums::Byte:
        q_variantMatrixByte(list, data.ByteArray + pos, baseLength); break;
    case OpcdoEnums::SByte:
        q_variantMatrixSByte(list, data.SByteArray + pos, baseLength); break;
    case OpcdoEnums::UInt16:
        q_variantMatrixUInt16(list, data.UInt16Array + pos, baseLength); break;
    case OpcdoEnums::UInt32:
        q_variantMatrixUInt32(list, data.UInt32Array + pos, baseLength); break;
    case OpcdoEnums::UInt64:
        q_variantMatrixUInt64(list, data.UInt64Array + pos, baseLength); break;
    case OpcdoEnums::Float:
        q_variantMatrixFloat(list, data.FloatArray + pos, baseLength); break;
    case OpcdoEnums::Double:
        q_variantMatrixDouble(list, data.DoubleArray + pos, baseLength); break;
    case OpcdoEnums::String:
        q_variantMatrixString(list, data.StringArray + pos, baseLength); break;
    default:

        dtype = OpcdoEnums::Unknown;
        break;
    }

    pos += baseLength;
    return list;
}

OpcUa_Boolean* q_variantMatrixBoolean(QVariantList& list, OpcUa_Boolean* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    bool set;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        set = *arrayPtr ? true : false;
        varValue.setValue<bool>(set);
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_DateTime* q_variantMatrixDateTime(QVariantList& list, OpcUa_DateTime* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        QString utf8String = UaDateTime(*arrayPtr).toString().toUtf8();
        varValue = QVariant(QDateTime::fromString(utf8String, Qt::ISODate));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Int16* q_variantMatrixInt16(QVariantList& list, OpcUa_Int16* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<int>(static_cast<int>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Int32* q_variantMatrixInt32(QVariantList& list, OpcUa_Int32* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<int>(static_cast<int>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Int64* q_variantMatrixInt64(QVariantList& list, OpcUa_Int64* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<qlonglong>(static_cast<qlonglong>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Byte* q_variantMatrixByte(QVariantList& list, OpcUa_Byte* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<uint>(static_cast<uint>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_SByte* q_variantMatrixSByte(QVariantList& list, OpcUa_SByte* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<int>(static_cast<int>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_UInt16* q_variantMatrixUInt16(QVariantList& list, OpcUa_UInt16* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<int>(static_cast<int>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_UInt32* q_variantMatrixUInt32(QVariantList& list, OpcUa_UInt32* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<int>(static_cast<int>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_UInt64* q_variantMatrixUInt64(QVariantList& list, OpcUa_UInt64* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<qulonglong>(static_cast<qulonglong>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Float* q_variantMatrixFloat(QVariantList& list, OpcUa_Float* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<float>(static_cast<float>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_Double* q_variantMatrixDouble(QVariantList& list, OpcUa_Double* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        varValue.setValue<double>(static_cast<double>(*arrayPtr));
        list.append(varValue);
    }

    return arrayPtr;
}

OpcUa_String* q_variantMatrixString(QVariantList& list, OpcUa_String* arrayPtr, int len)
{
    list.clear();
    QVariant varValue;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        QString utf8String = UaString(*arrayPtr).toUtf8();
        varValue.setValue<QString>(utf8String);
        list.append(varValue);
    }

    return arrayPtr;
}

