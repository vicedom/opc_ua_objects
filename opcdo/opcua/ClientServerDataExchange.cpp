#include "ClientServerDataExchange.h"
#include "OpcdoField.h"
#include "OpcdoFieldPrivate.h"
#include "opcdodllerrors.h"

using namespace UaClientSdk;

ClientServerDataExchange::ClientServerDataExchange(OpcdoEnums::CursorType cursorType,
                                                   OpcdoField* root,
                                                   const QString &nodeId,
                                                   OpcdoDatabasePrivate* parent)
    : QObject(parent)
    , m_dbPrivate(parent)
    , m_nodeId(nodeId)
    , m_root(root)
    , m_cursorType(cursorType)
{

}

ClientServerDataExchange::~ClientServerDataExchange()
{

}

quint32 ClientServerDataExchange::exec(QString& sessionError)
{
    if (m_cursorType == OpcdoEnums::UnknownCursor || m_cursorType == OpcdoEnums::ServerSideCursor) {
        return OPCDOERR_CURSORTYPE_NOT_SUPPORTED;
    }

    OpcdoFieldPrivate* pfield = m_root->d;

    if (pfield->m_expandProceeded) {
        pfield->makeNaked();
    }

    uint status = 0;

    exploreNode(m_dbPrivate->m_session,
                m_nodeId,
                pfield,
                m_cursorType == OpcdoEnums::SnapshotCursor,
                m_cursorType > OpcdoEnums::SnapshotCursor,
                m_dbPrivate->m_doBreakNoServer,
                status,
                sessionError);

    return status;
}

