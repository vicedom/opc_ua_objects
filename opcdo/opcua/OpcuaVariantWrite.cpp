#include "OpcuaVariant.h"

#include "uaclientsdk.h"

#include <QDateTime>
#include <QMetaType>
#include <QVector>

extern OpcUa_VariantArrayUnion arrayValueFromDataType(int len, const OpcdoEnums::DataType dtype);

static OpcUa_VariantArrayValue arrayFromQVariantList(const QVariantList& varList, const OpcdoEnums::DataType dtype);
static OpcUa_VariantMatrixValue matrixFromQVariantList(const QVariantList& varList, const OpcdoEnums::DataType dtype);

static QVector<int> variantMatrixValuesInit(OpcUa_VariantMatrixValue& matrix, const QVariantList& varList, int& totalLength, const OpcdoEnums::DataType dtype);
static void breakDownDimensionInfo(const QVariantList& varList, OpcUa_Int32& noOfDimensions, QVector<int>& dimensions);

static void q_variantMatrix(const QVector<int>& baseSizes, OpcUa_VariantArrayUnion data, const QVariantList& varList, int dim, qulonglong& pos, const OpcdoEnums::DataType dtype);
static void q_variantMatrixData(const QVector<int> &baseSizes, OpcUa_VariantArrayUnion data, const QVariantList& varList, int dim, qulonglong& pos, const OpcdoEnums::DataType dtype);
static OpcUa_Boolean* q_variantMatrixBoolean(OpcUa_Boolean* arrayPtr, const QVariantList& list, int len);
static OpcUa_DateTime* q_variantMatrixDateTime(OpcUa_DateTime* arrayPtr, const QVariantList& list, int len);
static OpcUa_Int16* q_variantMatrixInt16(OpcUa_Int16* arrayPtr, const QVariantList& list, int len);
static OpcUa_Int32* q_variantMatrixInt32(OpcUa_Int32* arrayPtr, const QVariantList& list, int len);
static OpcUa_Int64* q_variantMatrixInt64(OpcUa_Int64* arrayPtr, const QVariantList& list, int len);
static OpcUa_Byte* q_variantMatrixByte(OpcUa_Byte* arrayPtr, const QVariantList& list, int len);
static OpcUa_SByte* q_variantMatrixSByte(OpcUa_SByte* arrayPtr, const QVariantList& list, int len);
static OpcUa_UInt16* q_variantMatrixUInt16(OpcUa_UInt16* arrayPtr, const QVariantList& list, int len);
static OpcUa_UInt32* q_variantMatrixUInt32(OpcUa_UInt32* arrayPtr, const QVariantList& list, int len);
static OpcUa_UInt64* q_variantMatrixUInt64(OpcUa_UInt64* arrayPtr, const QVariantList& list, int len);
static OpcUa_Float* q_variantMatrixFloat(OpcUa_Float* arrayPtr, const QVariantList& list, int len);
static OpcUa_Double* q_variantMatrixDouble(OpcUa_Double* arrayPtr, const QVariantList& list, int len);
static OpcUa_String* q_variantMatrixString(OpcUa_String* arrayPtr, const QVariantList& list, int len);

OpcUa_Variant fromQVariant(const QVariant& varValue, const OpcdoEnums::DataType dtype)
{
    OpcUa_Variant value;
    OpcUa_Variant_Initialize(&value);
    value.Datatype = static_cast<OpcUa_Byte>(dtype);

    switch (dtype) {
    case OpcdoEnums::Boolean:
        value.Value.Boolean = varValue.toBool() ? OpcUa_True : OpcUa_False; break;
    case OpcdoEnums::DateTime:
    {
        UaString utf8String = varValue.toDateTime().toString(Qt::ISODateWithMs).toUtf8().constData();
        UaDateTime udt = UaDateTime::fromString(utf8String);
        value.Value.DateTime = (OpcUa_DateTime)udt;
    }
        break;
    case OpcdoEnums::Int16:
        value.Value.Int16 = static_cast<OpcUa_Int16>(varValue.toInt()); break;
    case OpcdoEnums::Int32:
        value.Value.Int32 = static_cast<OpcUa_Int32>(varValue.toInt()); break;
    case OpcdoEnums::Int64:
        value.Value.Int64 = static_cast<OpcUa_Int64>(varValue.toLongLong()); break;
    case OpcdoEnums::Byte:
        value.Value.Byte = static_cast<OpcUa_Byte>(varValue.toUInt()); break;
    case OpcdoEnums::SByte:
        value.Value.SByte = static_cast<OpcUa_SByte>(varValue.toInt()); break;
    case OpcdoEnums::UInt16:
        value.Value.UInt16 = static_cast<OpcUa_UInt16>(varValue.toUInt()); break;
    case OpcdoEnums::UInt32:
        value.Value.UInt32 = static_cast<OpcUa_Int32>(varValue.toUInt()); break;
    case OpcdoEnums::UInt64:
        value.Value.UInt64 = static_cast<OpcUa_Int64>(varValue.toULongLong()); break;
    case OpcdoEnums::Float:
        value.Value.Float = static_cast<OpcUa_Float>(varValue.toFloat()); break;
    case OpcdoEnums::Double:
        value.Value.Double = static_cast<OpcUa_Double>(varValue.toDouble()); break;
    case OpcdoEnums::String:
    {
        OpcUa_String_Initialize(&value.Value.String);
        UaString utf8String = varValue.toString().toUtf8().constData();

        if (!utf8String.detach(&value.Value.String)) {
            utf8String.copyTo(&value.Value.String);
        }
    }
        break;
    default:

        value.Datatype = static_cast<OpcUa_Byte>(0);
        break;
    }

    return value;
}

OpcUa_Variant fromQVariantList(const QVariantList& varList, const OpcdoEnums::DataType dtype)
{
    OpcUa_Variant value;
    OpcUa_Variant_Initialize(&value);
    value.Datatype = static_cast<OpcUa_Byte>(dtype);
    value.ArrayType = OpcUa_VariantArrayType_Array;
    value.Value.Array = arrayFromQVariantList(varList, dtype);

    if (!value.Value.Array.Value.Array) {

        value.Datatype = static_cast<OpcUa_Byte>(0);
        value.ArrayType = static_cast<OpcUa_Byte>(-1);
    }

    return value;
}

OpcUa_Variant fromQVariantMatrix(const QVariantList& varList, const OpcdoEnums::DataType dtype)
{
    OpcUa_Variant value;
    OpcUa_Variant_Initialize(&value);
    value.Datatype = static_cast<OpcUa_Byte>(dtype);
    value.ArrayType = OpcUa_VariantArrayType_Matrix;
    OpcUa_VariantMatrixValue mvalue = matrixFromQVariantList(varList, dtype);
    value.Value.Matrix = mvalue;

    return value;
}

OpcUa_VariantArrayValue arrayFromQVariantList(const QVariantList& varList,
                                              const OpcdoEnums::DataType dtype)
{
    OpcUa_VariantArrayValue array;
    array.Length = varList.length();
    array.Value = arrayValueFromDataType(array.Length, dtype);

    if (!array.Value.Array) {
        return array;
    }

    int i;
    QVariant varValue;

    for (i = 0; i < array.Length; i++) {

        varValue = varList.at(i);

        OpcUa_Variant value = fromQVariant(varValue, dtype);

        switch (dtype) {
        case OpcdoEnums::Boolean:
            array.Value.BooleanArray[i] = value.Value.Boolean; break;
        case OpcdoEnums::DateTime:
            array.Value.DateTimeArray[i] = value.Value.DateTime; break;
        case OpcdoEnums::Int16:
            array.Value.Int16Array[i] = value.Value.Int16; break;
        case OpcdoEnums::Int32:
            array.Value.Int32Array[i] = value.Value.Int32; break;
        case OpcdoEnums::Int64:
            array.Value.Int64Array[i] = value.Value.Int64; break;
        case OpcdoEnums::Byte:
            array.Value.ByteArray[i] = value.Value.Byte; break;
        case OpcdoEnums::SByte:
            array.Value.SByteArray[i] = value.Value.SByte; break;
        case OpcdoEnums::UInt16:
            array.Value.UInt16Array[i] = value.Value.UInt16; break;
        case OpcdoEnums::UInt32:
            array.Value.UInt32Array[i] = value.Value.UInt32; break;
        case OpcdoEnums::UInt64:
            array.Value.UInt64Array[i] = value.Value.UInt64; break;
        case OpcdoEnums::Float:
            array.Value.FloatArray[i] = value.Value.Float; break;
        case OpcdoEnums::Double:
            array.Value.DoubleArray[i] = value.Value.Double; break;
        case OpcdoEnums::String:
            array.Value.StringArray[i] = value.Value.String; break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    return array;
}

OpcUa_VariantMatrixValue matrixFromQVariantList(const QVariantList& varList, const OpcdoEnums::DataType dtype)
{
    OpcUa_VariantMatrixValue matrix;
    int totalLength = 0;
    QVector<int> baseSizes = variantMatrixValuesInit(matrix, varList, totalLength, dtype);

    if (matrix.NoOfDimensions < OpcUa_VariantArrayType_Matrix) { // varList error
        return matrix;
    }

    if (matrix.Value.Array == nullptr) { // dtype error

        matrix.NoOfDimensions = -1;
        return matrix;
    }

    baseSizes.append(1);
    QVariant varDummy;
    varDummy.setValue<QVariantList>(varList);
    QVariantList dummyList;
    dummyList.append(varDummy);

    qulonglong pos = 0;
    int dim = baseSizes.length() - 1;

    q_variantMatrix(baseSizes, matrix.Value, dummyList, dim, pos, dtype);

    return matrix;
}

QVector<int> variantMatrixValuesInit(OpcUa_VariantMatrixValue& matrix, const QVariantList& varList, int& totalLength, const OpcdoEnums::DataType dtype)
{
    matrix.Value.Array = nullptr;
    totalLength = 1;

    OpcUa_Int32 noOfDimensions = 0;
    QVector<int> dimensions;
    QVector<int> baseSizes;

    breakDownDimensionInfo(varList, noOfDimensions, dimensions);

    matrix.NoOfDimensions = noOfDimensions;

    if (noOfDimensions < OpcUa_VariantArrayType_Matrix) {

        totalLength = 0;
        matrix.Dimensions = nullptr;
        return QVector<int>();
    }

    int i, len = dimensions.length();

    matrix.Dimensions = new OpcUa_Int32[len];

    for (i = 0; i < len; i++) {
        baseSizes.prepend(dimensions.at(i));
    }

    for (i = 0; i < len; i++) {
        matrix.Dimensions[i] = static_cast<OpcUa_Int32>(baseSizes.at(i));
    }

    for (i = 0; i < len; i++) {
        totalLength *= dimensions.at(i);
    }

    matrix.Value = arrayValueFromDataType(totalLength, dtype);

    return baseSizes;
}

void breakDownDimensionInfo(const QVariantList& varList, OpcUa_Int32& noOfDimensions, QVector<int> &dimensions)
{
    int len = varList.length();

    if (len < 1) {
        return;
    }

    noOfDimensions++;
    dimensions.append(len);

    if (varList.at(0).type() == QMetaType::QVariantList) {
        return breakDownDimensionInfo(varList.at(0).value<QVariantList>(), noOfDimensions, dimensions);
    }
}

void q_variantMatrix(const QVector<int>& baseSizes, OpcUa_VariantArrayUnion data, const QVariantList& varList, int dim, qulonglong& pos, const OpcdoEnums::DataType dtype)
{
    int i, baseLength = baseSizes.at(dim);

    for (i = 0; i < baseLength; i++) {

        QVariantList list = varList.at(i).value<QVariantList>();

        if (dim == 1) {
            q_variantMatrixData(baseSizes, data, list, dim - 1, pos, dtype);
        }
        else {
            q_variantMatrix(baseSizes, data, list, dim - 1, pos, dtype);
        }
    }
}

void q_variantMatrixData(const QVector<int>& baseSizes, OpcUa_VariantArrayUnion data, const QVariantList& varList, int dim, qulonglong& pos, const OpcdoEnums::DataType dtype)
{
    int baseLength = baseSizes.at(dim);

    switch (dtype)
    {
    case OpcdoEnums::Boolean:
        q_variantMatrixBoolean(data.BooleanArray + pos, varList, baseLength); break;
    case OpcdoEnums::DateTime:
        q_variantMatrixDateTime(data.DateTimeArray + pos, varList, baseLength); break;
    case OpcdoEnums::Int16:
        q_variantMatrixInt16(data.Int16Array + pos, varList, baseLength); break;
    case OpcdoEnums::Int32:
        q_variantMatrixInt32(data.Int32Array + pos, varList, baseLength); break;
    case OpcdoEnums::Int64:
        q_variantMatrixInt64(data.Int64Array + pos, varList, baseLength); break;
    case OpcdoEnums::Byte:
        q_variantMatrixByte(data.ByteArray + pos, varList, baseLength); break;
    case OpcdoEnums::SByte:
        q_variantMatrixSByte(data.SByteArray + pos, varList, baseLength); break;
    case OpcdoEnums::UInt16:
        q_variantMatrixUInt16(data.UInt16Array + pos, varList, baseLength); break;
    case OpcdoEnums::UInt32:
        q_variantMatrixUInt32(data.UInt32Array + pos, varList, baseLength); break;
    case OpcdoEnums::UInt64:
        q_variantMatrixUInt64(data.UInt64Array + pos, varList, baseLength); break;
    case OpcdoEnums::Float:
        q_variantMatrixFloat(data.FloatArray + pos, varList, baseLength); break;
    case OpcdoEnums::Double:
        q_variantMatrixDouble(data.DoubleArray + pos, varList, baseLength); break;
    case OpcdoEnums::String:
        q_variantMatrixString(data.StringArray + pos, varList, baseLength); break;
    default:

        // not reachable
        Q_ASSERT(false);
        break;
    }

    pos += baseLength;
}

OpcUa_Boolean* q_variantMatrixBoolean(OpcUa_Boolean* arrayPtr, const QVariantList& list, int len)
{
    OpcUa_Boolean set;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        set = list.at(i).toBool() ? OpcUa_True : OpcUa_False;
        *arrayPtr = set;
    }

    return arrayPtr;
}

OpcUa_DateTime* q_variantMatrixDateTime(OpcUa_DateTime* arrayPtr, const QVariantList& list, int len)
{
    OpcUa_Variant value;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        UaString utf8String = list.at(i).toDateTime().toString(Qt::ISODate).toUtf8().constData();
        UaDateTime::fromString(utf8String).toVariant(value);
        *arrayPtr = value.Value.DateTime;
    }

    return arrayPtr;
}

OpcUa_Int16* q_variantMatrixInt16(OpcUa_Int16* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Int16>(list.at(i).toInt());
    }

    return arrayPtr;
}

OpcUa_Int32* q_variantMatrixInt32(OpcUa_Int32* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Int32>(list.at(i).toInt());
    }

    return arrayPtr;
}

OpcUa_Int64* q_variantMatrixInt64(OpcUa_Int64* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Int64>(list.at(i).toLongLong());
    }

    return arrayPtr;
}

OpcUa_Byte* q_variantMatrixByte(OpcUa_Byte* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Byte>(list.at(i).toUInt());
    }

    return arrayPtr;
}

OpcUa_SByte* q_variantMatrixSByte(OpcUa_SByte* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_SByte>(list.at(i).toInt());
    }

    return arrayPtr;
}

OpcUa_UInt16* q_variantMatrixUInt16(OpcUa_UInt16* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_UInt16>(list.at(i).toUInt());
    }

    return arrayPtr;
}

OpcUa_UInt32* q_variantMatrixUInt32(OpcUa_UInt32* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_UInt32>(list.at(i).toUInt());
    }

    return arrayPtr;
}

OpcUa_UInt64* q_variantMatrixUInt64(OpcUa_UInt64* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_UInt64>(list.at(i).toULongLong());
    }

    return arrayPtr;
}

OpcUa_Float* q_variantMatrixFloat(OpcUa_Float* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Float>(list.at(i).toFloat());
    }

    return arrayPtr;
}

OpcUa_Double* q_variantMatrixDouble(OpcUa_Double* arrayPtr, const QVariantList& list, int len)
{
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {
        *arrayPtr = static_cast<OpcUa_Double>(list.at(i).toDouble());
    }

    return arrayPtr;
}

OpcUa_String* q_variantMatrixString(OpcUa_String* arrayPtr, const QVariantList& list, int len)
{
    OpcUa_String string;
    int i;

    for (i = 0; i < len; i++, arrayPtr++) {

        OpcUa_String_Initialize(&string);
        UaString utf8String = list.at(i).toString().toUtf8().constData();

        if (!utf8String.detach(&string)) {
            utf8String.copyTo(&string);
        }

        *arrayPtr = string;
    }

    return arrayPtr;
}

