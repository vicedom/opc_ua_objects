#ifndef OPCUASUBSCRIPTIONCALLBACK_H
#define OPCUASUBSCRIPTIONCALLBACK_H

#include "uaclientsdk.h"
#include "uasession.h"
#include <vector>
#include "uagenericunionvalue.h"

#include <QObject>

namespace UaClientSdk {

class OpcuaSubscriptionCallback : public QObject
                                , public UaSubscriptionCallback
{
    Q_OBJECT
public:
    explicit OpcuaSubscriptionCallback(QObject *parent = nullptr);
    ~OpcuaSubscriptionCallback() override;

    UaSubscriptionCallback* subscriptionCallback()
    {
        return this;
    }

    void deleteSelf();

    /** Send subscription state change. */
    void subscriptionStatusChanged(OpcUa_UInt32 clientSubscriptionHandle,
                                   const UaStatus& status) override;

    /** Send changed data. */
    void dataChange(OpcUa_UInt32 clientSubscriptionHandle,
                    const UaDataNotifications& dataNotifications,
                    const UaDiagnosticInfos& diagnosticInfos) override;

    /** Send new events. */ // is not used
    void newEvents(OpcUa_UInt32 clientSubscriptionHandle,
                   UaEventFieldLists& eventFieldList) override;

signals:
    void affinityRestored();

    void subscriptionStatusChangedOccured(uint clientSubscriptionHandle,
                                          uint statusVal,
                                          const QString& statusString);
    void dataChangeOccured(uint clientSubscriptionHandle,
                           uint montoringId,
                           const QDateTime& modified,
                           const QVariant& varValue);
    void dataChangeErrorOccured(uint clientSubscriptionHandle,
                                uint montoringId,
                                uint statusVal,
                                const QString& statusString);

    void internalDelete();

private slots:
    void deleteInternal();
};

} // namespace

#endif // OPCUASUBSCRIPTIONCALLBACK_H
