#include "ClientServerDataExchange.h"
#include "OpcdoField.h"
#include "OpcdoFieldPrivate.h"
#include "opcdodllclass/OpcdoDllClass.h"
#include "opcdodllerrors.h"
#include "OpcuaVariant.h"

#include "uasession.h"
#include "uaclientsdk.h"

#include <QCoreApplication>

using namespace UaClientSdk;
using namespace opcdodllclass;

void ClientServerDataExchange::exploreNode(UaSession* session,
                                           const QString& nodeId,
                                           OpcdoFieldPrivate* root,
                                           bool deep,
                                           bool readValue,
                                           bool& doBreakConnectionBroken,
                                           uint& status,
                                           QString& statusString)
{
    status = 0;
    statusString.clear();

    QString idNode = nodeId.trimmed();

    if (idNode.isEmpty()) {

        UaNodeId startingNode(OpcUaId_RootFolder);
        idNode = startingNode.toXmlString().toUtf8();

        root->m_nodeId = idNode;
        root->m_nameSpace = startingNode.namespaceIndex();
        root->m_nodeClass = OpcdoField::Unspecified;
        root->m_displayName.clear();
        root->m_value.clear();
    }

    ServiceSettings settings;

    browseNodeTreeChild(session,
                        idNode,
                        root,
                        deep,
                        readValue,
                        doBreakConnectionBroken,
                        status,
                        statusString,
                        &settings,
                        0);
}

// recursiv
bool ClientServerDataExchange::browseNodeTreeChild(UaSession* session,
                                                   const QString& nodeId,
                                                   OpcdoFieldPrivate* field,
                                                   bool deep,
                                                   bool readValue,
                                                   bool& doBreakConnectionBroken,
                                                   uint& statusVal,
                                                   QString& errorString,
                                                   ServiceSettings* settings,
                                                   int level)
{
    UaString itemNodeId = qPrintable(nodeId);
    UaNodeId startingNode = UaNodeId::fromXmlString(itemNodeId);

    statusVal = 0;
    errorString.clear();

    UaStatus                status;
    UaByteString            continuationPoint;
    UaReferenceDescriptions referenceDescriptions;
    BrowseContext           browseContext;

    status = session->browse(*settings,
                             startingNode,
                             browseContext,
                             continuationPoint,
                             referenceDescriptions);

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return false;
    }

    OpcUa_ReferenceDescription refDesc;
    OpcUa_UInt32 i, len = referenceDescriptions.length();

    for (i = 0; i < len; i++) {

        refDesc = referenceDescriptions[i];
        UaString displayString = refDesc.DisplayName.Text;
        int nc = refDesc.NodeClass;

        UaNodeId nodeId(refDesc.NodeId.NodeId);
        UaString idString = nodeId.toXmlString();
        int nsIdx = nodeId.namespaceIndex();

        OpcdoField* newItem = new OpcdoField(field->q);

        newItem->d->m_nodeId = idString.toUtf8();
        newItem->d->m_nameSpace = nsIdx;
        newItem->d->m_displayName = displayString.toUtf8();
        newItem->d->m_nodeClass = static_cast<OpcdoField::NodeClass>(nc);

        if (readValue) {

            uint _statusVal;
            QString _errorString;
            OpcdoEnums::DataType dataType = OpcdoEnums::Unknown;

            newItem->d->m_value = readNodeValueVariant(session, newItem->d->m_nodeId, _statusVal, _errorString, dataType, settings);
        }

        field->m_childItems.append(newItem);

        if (deep) {

            qApp->processEvents();

            if (doBreakConnectionBroken) {

                statusVal = OPCDOERR_CONNECTION_BROKEN;
                return false;
            }

            if (!browseNodeTreeChild(session,
                                     newItem->d->m_nodeId,
                                     newItem->d,
                                     deep,
                                     readValue,
                                     doBreakConnectionBroken,
                                     statusVal,
                                     errorString,
                                     settings,
                                     level + 1)) {
                return false;
            }
        }
    }

    while (continuationPoint.length() > 0) {

        status = session->browseNext(
            *settings,
            OpcUa_False,
            continuationPoint,
            referenceDescriptions);

        if (status.isBad()) {

            statusVal = status.statusCode();
            errorString = status.toString().toUtf8();
            return false;
        }

        len = referenceDescriptions.length();

        for (i = 0; i < len; i++) {

            refDesc = referenceDescriptions[i];
            UaString displayString = refDesc.DisplayName.Text;
            int nc = refDesc.NodeClass;

            UaNodeId nodeId(refDesc.NodeId.NodeId);
            UaString idString = nodeId.toXmlString();
            int nsIdx = nodeId.namespaceIndex();

            OpcdoField* newItem = new OpcdoField(field->q);

            newItem->d->m_nodeId = idString.toUtf8();
            newItem->d->m_nameSpace = nsIdx;
            newItem->d->m_displayName = displayString.toUtf8();
            newItem->d->m_nodeClass = static_cast<OpcdoField::NodeClass>(nc);

            if (readValue) {

                uint _statusVal;
                QString _errorString;
                OpcdoEnums::DataType dataType = OpcdoEnums::Unknown;

                newItem->d->m_value = readNodeValueVariant(session, newItem->d->m_nodeId, _statusVal, _errorString, dataType, settings);
            }

            field->m_childItems.append(newItem);

            if (deep) {

                qApp->processEvents();

                if (doBreakConnectionBroken) {

                    statusVal = OPCDOERR_CONNECTION_BROKEN;
                    return false;
                }

                if (!browseNodeTreeChild(session,
                                         newItem->d->m_nodeId,
                                         newItem->d,
                                         deep,
                                         readValue,
                                         doBreakConnectionBroken,
                                         statusVal,
                                         errorString,
                                         settings,
                                         level + 1)) {
                    return false;
                }
            }
        }
    }

    field->m_expandProceeded = true;

    return true;
}

QString ClientServerDataExchange::readNodeValueString(UaSession* session,
                                                       const QString& nodeId,
                                                       uint& statusVal,
                                                       QString& errorString,
                                                       ServiceSettings* settings)
{
    ServiceSettings serviceSettings;

    if (!settings) {
        settings = &serviceSettings;
    }

    return readNodeValueAttributeString(session,
                                        nodeId,
                                        OpcUa_Attributes_Value,
                                        statusVal,
                                        errorString,
                                        settings);


}

QVariant ClientServerDataExchange::readNodeValueVariant(UaClientSdk::UaSession* session,
                                                        const QString& nodeId,
                                                        uint& statusVal,
                                                        QString& errorString,
                                                        OpcdoEnums::DataType& dataType,
                                                        UaClientSdk::ServiceSettings* settings)
{
    ServiceSettings serviceSettings;

    if (!settings) {
        settings = &serviceSettings;
    }

    return readNodeValueAttributeVariant(session,
                                         nodeId,
                                         OpcUa_Attributes_Value,
                                         statusVal,
                                         errorString,
                                         dataType,
                                         settings);
}

QString ClientServerDataExchange::readNodeValueDataTypeString(UaSession* session,
                                                               const QString& nodeId,
                                                               uint& statusVal,
                                                               QString& errorString,
                                                               ServiceSettings* settings)
{
    ServiceSettings serviceSettings;

    if (!settings) {
        settings = &serviceSettings;
    }

    return readNodeValueAttributeString(session,
                                        nodeId,
                                        OpcUa_Attributes_DataType,
                                        statusVal,
                                        errorString,
                                        settings);
}

QVariant ClientServerDataExchange::readNodeValueDataTypeVariant(UaClientSdk::UaSession* session,
                                                                const QString& nodeId,
                                                                uint& statusVal,
                                                                QString& errorString,
                                                                UaClientSdk::ServiceSettings* settings)
{
    ServiceSettings serviceSettings;

    if (!settings) {
        settings = &serviceSettings;
    }

    OpcdoEnums::DataType dataType;

    return readNodeValueAttributeVariant(session,
                                         nodeId,
                                         OpcUa_Attributes_DataType,
                                         statusVal,
                                         errorString,
                                         dataType,
                                         settings);
}

QString ClientServerDataExchange::readNodeValueAttributeString(UaSession* session,
                                                                const QString& nodeId,
                                                                uint attribute,
                                                                uint& statusVal,
                                                                QString& errorString,
                                                                ServiceSettings* settings)
{
    QString result;
    UaStatus          status;
    UaReadValueIds    nodesToRead;
    UaDataValues      values;
    UaDiagnosticInfos diagnosticInfos;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    nodesToRead.create(1);
    nodesToRead[0].AttributeId = attribute;
    id.copyTo(&nodesToRead[0].NodeId);

    status = session->read(
            *settings,
            0,
            OpcUa_TimestampsToReturn_Both,
            nodesToRead,
            values,
            diagnosticInfos);

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return result;
    }

    OpcUa_DataValue val = values[0];

    if (OpcUa_IsGood(val.StatusCode)) {

        UaVariant varVal(val.Value);
        result = varVal.toString().toUtf8();
    }
    else {

        status = val.StatusCode;
        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
    }

    return result;
}

QVariant ClientServerDataExchange::readNodeValueAttributeVariant(UaClientSdk::UaSession* session,
                                                                 const QString& nodeId,
                                                                 uint attribute,
                                                                 uint& statusVal,
                                                                 QString& errorString,
                                                                 OpcdoEnums::DataType& dtype,
                                                                 UaClientSdk::ServiceSettings* settings)
{
    QVariant result;
    UaStatus          status;
    UaReadValueIds    nodesToRead;
    UaDataValues      values;
    UaDiagnosticInfos diagnosticInfos;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    nodesToRead.create(1);
    nodesToRead[0].AttributeId = attribute;
    id.copyTo(&nodesToRead[0].NodeId);

    status = session->read(
            *settings,
            0,
            OpcUa_TimestampsToReturn_Both,
            nodesToRead,
            values,
            diagnosticInfos);

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return result;
    }

    OpcUa_DataValue val = values[0];

    if (OpcUa_IsGood(val.StatusCode)) {

        if (attribute == OpcUa_Attributes_Value) {

            if (val.Value.ArrayType == OpcUa_VariantArrayType_Scalar) {

                result = toQVariant(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_VALUE;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_VALUE);
                    return QVariant();
                }
            }
            else if (val.Value.ArrayType == OpcUa_VariantArrayType_Array) {

                QVariantList list = toQVariantArray(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_ARRAY;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_ARRAY);
                    return QVariant();
                }

                result.setValue<QVariantList>(list);
            }
            else {

                QVariantList list = toQVariantMatrix(val.Value, dtype);

                if (dtype == OpcdoEnums::Unknown) {

                    statusVal = OPCDOERR_EXCHANGE_READ_MATRIX;
                    errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_MATRIX);
                    return QVariant();
                }

                result.setValue<QVariantList>(list);
            }
        }
        else if (attribute == OpcUa_Attributes_DataType) {

            UaNodeId dataTypeId(*val.Value.Value.NodeId);

            if (dataTypeId.namespaceIndex() == 0 && dataTypeId.identifierType() == OpcUa_IdentifierType_Numeric) {
                result.setValue<uint>(static_cast<uint>(dataTypeId.identifierNumeric()));
            }
            else {

                UaVariant varVal(val.Value);
                result = varVal.toFullString().toUtf8();
            }
        }
        else {
            statusVal = OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG;
            errorString = OpcdoDllClass::errorString(OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG).arg(attribute);
            return result;
        }        
    }
    else {

        status = val.StatusCode;
        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
    }

    return result;
}

bool ClientServerDataExchange::writeNodeValueVariant(UaSession* session,
                                                     const QString& nodeId,
                                                     const QVariant& varValue,
                                                     uint& statusVal,
                                                     QString& errorString,
                                                     const OpcdoEnums::DataType dataType,
                                                     UaClientSdk::ServiceSettings* settings)
{
    ServiceSettings serviceSettings;

    if (!settings) {
        settings = &serviceSettings;
    }

    if (!varValue.isValid() && dataType == OpcdoEnums::Unknown) {

        statusVal = OPCDOERR_EXCHANGE_WRITE_VALUE_INVAL;
        errorString = OpcdoDllClass::errorString(statusVal);
        return false;
    }

    QMetaType::Type type = static_cast<QMetaType::Type>(varValue.type());

    if (type == QMetaType::QVariantList) {

        QVariantList list = varValue.value<QVariantList>();
        int len = list.length();

        if (len == 0 || list.at(0).type() != QMetaType::QVariantList) {
            return writeNodeValueVariantVariantList(session, nodeId, list, statusVal, errorString, dataType, settings);
        }
        else {
            return writeNodeValueVariantVariantMatrix(session, nodeId, list, statusVal, errorString, dataType, settings);
        }
    }
    else {
        return writeNodeValueVariantVariant(session, nodeId, varValue, statusVal, errorString, dataType, settings);
    }
}


bool ClientServerDataExchange::writeNodeValueVariantVariant(UaClientSdk::UaSession* session,
                                                            const QString& nodeId,
                                                            const QVariant& varValue,
                                                            uint& statusVal,
                                                            QString& errorString,
                                                            const OpcdoEnums::DataType dataType,
                                                            UaClientSdk::ServiceSettings* settings)
{
    UaStatus            status;
    UaWriteValues       nodesToWrite;
    UaStatusCodeArray   results;
    UaDiagnosticInfos   diagnosticInfos;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    OpcdoEnums::DataType dtype = dataType;

    if (dtype == OpcdoEnums::Unknown) {

        QMetaType::Type mtype = static_cast<QMetaType::Type>(varValue.type());
        dtype = OpcdoEnums::fromMetaType(mtype);
    }

    OpcUa_Variant value = fromQVariant(varValue, dtype);

    if (value.Datatype == static_cast<OpcUa_Byte>(0)) {

        statusVal = OPCDOERR_EXCHANGE_WRITE_TYPE_INVAL;
        errorString = OpcdoDllClass::errorString(statusVal);
        return false;
    }

    nodesToWrite.create(1);
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;
    OpcUa_NodeId_CopyTo(id, &nodesToWrite[0].NodeId);    
    OpcUa_Variant_CopyTo(&value, &nodesToWrite[0].Value.Value);
    OpcUa_Variant_Clear(&value);

    status = session->write(
            *settings,
            nodesToWrite,
            results,
            diagnosticInfos);    

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return false;
    }

    if (OpcUa_IsGood(results[0])) {
        return true;
    }
    else {

        status = results[0];
        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
    }

    return false;
}

bool ClientServerDataExchange::writeNodeValueVariantVariantList(UaClientSdk::UaSession* session,
                                                                const QString& nodeId,
                                                                const QVariantList& varList,
                                                                uint& statusVal,
                                                                QString& errorString,
                                                                const OpcdoEnums::DataType dataType,
                                                                UaClientSdk::ServiceSettings* settings)
{
    UaStatus            status;
    UaWriteValues       nodesToWrite;
    UaStatusCodeArray   results;
    UaDiagnosticInfos   diagnosticInfos;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    OpcUa_Variant value = fromQVariantList(varList, dataType);

    if (value.ArrayType == static_cast<OpcUa_Byte>(-1)) {

        statusVal = OPCDOERR_EXCHANGE_WRITE_ARRAY_INVAL;
        errorString = OpcdoDllClass::errorString(statusVal);
        return false;
    }
    else if (value.ArrayType < OpcUa_VariantArrayType_Matrix) {

        statusVal = OPCDOERR_EXCHANGE_WRITE_DIMENSION_INVAL;
        errorString = OpcdoDllClass::errorString(statusVal);
        return false;
    }

    nodesToWrite.create(1);
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;
    OpcUa_NodeId_CopyTo(id, &nodesToWrite[0].NodeId);
    OpcUa_Variant_CopyTo(&value, &nodesToWrite[0].Value.Value);
    OpcUa_Variant_Clear(&value);

    status = session->write(
            *settings,
            nodesToWrite,
            results,
            diagnosticInfos);

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return false;
    }

    if (OpcUa_IsGood(results[0])) {
        return true;
    }
    else {

        status = results[0];
        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
    }

    return false;
}

bool ClientServerDataExchange::writeNodeValueVariantVariantMatrix(UaClientSdk::UaSession* session,
                                                                  const QString& nodeId,
                                                                  const QVariantList& varList,
                                                                  uint& statusVal,
                                                                  QString& errorString,
                                                                  const OpcdoEnums::DataType dataType,
                                                                  UaClientSdk::ServiceSettings* settings)
{
    UaStatus            status;
    UaWriteValues       nodesToWrite;
    UaStatusCodeArray   results;
    UaDiagnosticInfos   diagnosticInfos;
    UaNodeId id = UaNodeId::fromXmlString(qPrintable(nodeId));

    OpcUa_Variant value = fromQVariantMatrix(varList, dataType);

    if (value.ArrayType == static_cast<OpcUa_Byte>(-1)) {

        statusVal = OPCDOERR_EXCHANGE_WRITE_MATRIX_INVAL;
        errorString = OpcdoDllClass::errorString(statusVal);
        return false;
    }

    nodesToWrite.create(1);
    nodesToWrite[0].AttributeId = OpcUa_Attributes_Value;
    OpcUa_NodeId_CopyTo(id, &nodesToWrite[0].NodeId);
    OpcUa_Variant_CopyTo(&value, &nodesToWrite[0].Value.Value);
    OpcUa_Variant_Clear(&value);

    status = session->write(
            *settings,
            nodesToWrite,
            results,
            diagnosticInfos);

    if (status.isBad()) {

        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
        return false;
    }

    if (OpcUa_IsGood(results[0])) {
        return true;
    }
    else {

        status = results[0];
        statusVal = status.statusCode();
        errorString = status.toString().toUtf8();
    }

    return false;
}

