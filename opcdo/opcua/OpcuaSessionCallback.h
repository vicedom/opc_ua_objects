#ifndef OPCUACLIENTCALLBACK_H
#define OPCUACLIENTCALLBACK_H

#include "uaclientsdk.h"
#include "uasession.h"
#include <vector>
#include "uagenericunionvalue.h"

#include <QObject>

namespace UaClientSdk {

class OpcuaSessionCallback : public QObject
                           , public UaSessionCallback//
{
    Q_OBJECT
public:
    explicit OpcuaSessionCallback(QObject* parent = nullptr);
    ~OpcuaSessionCallback() override;

    UaSessionCallback* sessionCallback()
    {
        return this;
    }

    void deleteSelf();

    /** Status of the server connection. */
    void connectionStatusChanged(OpcUa_UInt32 clientConnectionId,
                                 UaClient::ServerStatus serverStatus) override;

    /** Connect errors. */
    bool connectError(OpcUa_UInt32 clientConnectionId,          //!< [in] Client defined handle of the affected session
                      UaClient::ConnectServiceType serviceType, //!< [in] The failing connect step
                      const UaStatus& error,                    //!< [in] Status code for the error situation
                      bool clientSideError) override;           //!< [in] Flag indicating if the bad status was created in the Client SDK

    /** Send read results. */ // not used yet
    void readComplete(OpcUa_UInt32 transactionId,
                      const UaStatus& result,
                      const UaDataValues& values,
                      const UaDiagnosticInfos& diagnosticInfos) override;

    /** Send write results. */ // not used yet
    void writeComplete(OpcUa_UInt32 transactionId,
                       const UaStatus& result,
                       const UaStatusCodeArray& results,
                       const UaDiagnosticInfos& diagnosticInfos) override;

signals:
    void affinityRestored();
    void connectionStatusChangedOccured(uint clientConnectionId,
                                        int serverStatus);
    void connectErrorOccured(uint clientConnectionId,
                             int serviceType,
                             uint error,
                             const QString& errorString,
                             bool clientSideError);

    void internalDelete();

private slots:
    void deleteInternal();
};

} // namespace

#endif // OPCUACLIENTCALLBACK_H
