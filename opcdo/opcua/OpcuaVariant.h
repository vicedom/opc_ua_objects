#ifndef OPCUAVARIANT_H
#define OPCUAVARIANT_H

#include "OpcdoEnums.h"

#include "uavariant.h"

#include <QVariant>

OpcUa_Variant fromQVariant(const QVariant& varValue, const OpcdoEnums::DataType dtype);
OpcUa_Variant fromQVariantList(const QVariantList& varList, const OpcdoEnums::DataType dtype);
OpcUa_Variant fromQVariantMatrix(const QVariantList& varList, const OpcdoEnums::DataType dtype);

QVariant toQVariant(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype);
QVariantList toQVariantArray(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype);
QVariantList toQVariantMatrix(const OpcUa_Variant& value, OpcdoEnums::DataType& dtype);

#endif // OPCUAVARIANT_H
