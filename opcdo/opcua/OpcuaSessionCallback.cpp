#include "OpcuaSessionCallback.h"

#include <QCoreApplication>
#include <QThread>

#include <QDebug>

using namespace UaClientSdk;

OpcuaSessionCallback::OpcuaSessionCallback(QObject* parent)
    : QObject(parent)
{
    connect(this, &OpcuaSessionCallback::internalDelete, this, &OpcuaSessionCallback::deleteInternal);
}

OpcuaSessionCallback::~OpcuaSessionCallback()
{
#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor OpcuaSessionCallback::~OpcuaSessionCallback called";
#endif
}

void OpcuaSessionCallback::deleteSelf()
{
    emit internalDelete();
}

void OpcuaSessionCallback::deleteInternal()
{
    QThread* mainThread = qApp->thread();
    QThread* ownThread = thread();

    if (ownThread != mainThread) {

        moveToThread(mainThread);
        emit affinityRestored();
    }

    deleteLater();
}


void OpcuaSessionCallback::connectionStatusChanged(OpcUa_UInt32 clientConnectionId,
                                                UaClient::ServerStatus serverStatus)
{
    emit connectionStatusChangedOccured(clientConnectionId, static_cast<int>(serverStatus));
}

bool OpcuaSessionCallback::connectError(OpcUa_UInt32 clientConnectionId,
                                     UaClient::ConnectServiceType serviceType,
                                     const UaStatus& error,
                                     bool clientSideError)
{
    uint errorVal = error.code();
    QString errorString = error.toString().toUtf8();

    emit connectErrorOccured(clientConnectionId,
                             static_cast<int>(serviceType),
                             errorVal,
                             errorString,
                             clientSideError);


    return false;
}

void OpcuaSessionCallback::readComplete(OpcUa_UInt32 transactionId,
                                     const UaStatus& result,
                                     const UaDataValues& values,
                                     const UaDiagnosticInfos& /*diagnosticInfos*/)
{
    Q_UNUSED(values);
    Q_UNUSED(result);
    Q_UNUSED(transactionId);
}

void OpcuaSessionCallback::writeComplete(OpcUa_UInt32 transactionId,
                                      const UaStatus& result,
                                      const UaStatusCodeArray& results,
                                      const UaDiagnosticInfos& /*diagnosticInfos*/)
{
    Q_UNUSED(results);
    Q_UNUSED(result);
    Q_UNUSED(transactionId);
}
