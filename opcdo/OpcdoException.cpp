#include "OpcdoException.h"

#include <QDebug>

OpcdoException::OpcdoException(quint32 errorNumber, const QString& errorString)
    : m_errorNumber(errorNumber)
    , m_errorString(errorString)
{

}

OpcdoException::~OpcdoException()
{
#ifdef _DEVELOPMENT_INTERNAL_USE
    qDebug() << "d'tor OpcdoException::~OpcdoException called";
#endif
}

void OpcdoException::raise() const
{
    qDebug() << "raising OpcdoException: reason <" << reason() << ">, message: <" << message() << ">";

    throw *this;
}

QException* OpcdoException::clone() const
{
    return new OpcdoException(*this);
}

quint32 OpcdoException::reason() const
{
    return m_errorNumber;
}

QString OpcdoException::message() const
{
    return m_errorString;
}

QHash<QString, QString> OpcdoException::info() const
{
    return m_errorInfo;
}

void OpcdoException::setReason(quint32 errorNumber)
{
    m_errorNumber = errorNumber;
}

void OpcdoException::setErrorString(const QString& errorString)
{
    m_errorString = errorString;
}
