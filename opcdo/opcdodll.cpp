#include "opcdodll.h"
#include "opcdodllclass/OpcdoDllClass.h"

using namespace opcdodllclass;

QString opcdodllErrorString(quint32 number)
{
    return OpcdoDllClass::errorString(number);
}

bool opcdodllCanUnloadNow()
{
    return OpcdoDllClass::privatesReferenceCount() < 1;
}
