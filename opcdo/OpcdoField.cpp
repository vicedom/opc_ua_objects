#include "OpcdoField.h"
#include "OpcdoFieldPrivate.h"

OpcdoField::OpcdoField(OpcdoField* parentField)
    : d(new OpcdoFieldPrivate(this, parentField))
{

}

OpcdoField::~OpcdoField()
{
    delete d;
}

OpcdoField* OpcdoField::child(int row)
{
    return d->child(row);
}

int OpcdoField::childCount() const
{
    return d->childCount();
}

int OpcdoField::columnCount() const
{
    return d->columnCount();
}

QVariant OpcdoField::data(int column) const
{
    return d->data(column);
}

int OpcdoField::row() const
{
    return d->row();
}

OpcdoField* OpcdoField::parentField()
{
    return d->m_parentField;
}
