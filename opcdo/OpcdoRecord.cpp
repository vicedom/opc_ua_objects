#include "OpcdoRecord.h"
#include "OpcdoRecordPrivate.h"
#include "OpcdoDatabase.h"
#include "OpcdoField.h"

OpcdoRecord::OpcdoRecord(OpcdoDatabase* parent)
    : QObject(parent)
    , d(new OpcdoRecordPrivate(parent, this))
{

}

OpcdoRecord::~OpcdoRecord()
{
    delete d;
}

OpcdoField& OpcdoRecord::field()
{
    return *d->m_field;
}
