#ifndef OPCDOVALUESTRINGCOLLOCATOR_H
#define OPCDOVALUESTRINGCOLLOCATOR_H

#include "opcdodll.h"
#include "OpcdoEnums.h"

#include <QString>
#include <QVariant>

class OPCDO_EXPORT OpcdoValueStringCollocator
{
public:
    static QString collocate(const QVariant& value, const OpcdoEnums::DataType dtype) /*throws*/;

private:
    static QString collocateScalar(const QVariant& value, const OpcdoEnums::DataType dtype, int index = -1, int subarray = -1, const QString& dimdesc = QString());
    static QString collocateArray(const QVariantList& list, const OpcdoEnums::DataType dtype, int subarray = -1, const QString& dimdesc = QString());
    static QString collocateMatrix(const QVariantList& list, const OpcdoEnums::DataType dtype, const QString& dimdesc = QString());
    static QString collocateDimension(const QVariantList& list, OpcdoEnums::DataType dtype, int rank, const QString& dimdesc = QString());

};

#endif // OPCDOVALUESTRINGCOLLOCATOR_H
