#ifndef OPCDODLLERRORS_H
#define OPCDODLLERRORS_H

// exception values
#define OPCDOERR_EMPTY_SERVER_URL               0x80000801
#define OPCDOERR_EMPTY_SERVER_URL_STRING                tr("connection request with empty url")

#define OPCDOERR_ALLREADY_CONNECTED             0x80000802
#define OPCDOERR_ALLREADY_CONNECTED_STRING              tr("connection already established, call disconnect first")

#define OPCDOERR_NO_SESSION_CONNECTED           0x80000803
#define OPCDOERR_NO_SESSION_CONNECTED_STRING            tr("no connection established, connect first")

#define OPCDOERR_CURSORTYPE_NOT_SUPPORTED       0x80000804
#define OPCDOERR_CURSORTYPE_NOT_SUPPORTED_STRING        tr("cursor type not supported")

#define OPCDOERR_CONNECTION_BROKEN              0x80000805
#define OPCDOERR_CONNECTION_BROKEN_STRING               tr("connection to server is broken")

#define OPCDOERR_NO_SESSION_SUBSCRIPTION        0x80000806
#define OPCDOERR_NO_SESSION_SUBSCRIPTION_STRING         tr("no subscription to server established")

#define OPCDOERR_SERVER_ERROR_MASK              0x80000000
#define OPCDOERR_SERVER_ERROR_MASK_STRING               tr("--- Server Error (0x%1) ---")


#define OPCDOERR_EXCHANGE_WRITE_VALUE_INVAL     0x90000801
#define OPCDOERR_EXCHANGE_WRITE_VALUE_INVAL_STRING      tr("invalid write value, set data type with empty values")

#define OPCDOERR_EXCHANGE_WRITE_TYPE_INVAL      0x90000801
#define OPCDOERR_EXCHANGE_WRITE_TYPE_INVAL_STRING       tr("not supported or invalid data type of write value")

#define OPCDOERR_EXCHANGE_WRITE_ARRAY_INVAL     0x90000802
#define OPCDOERR_EXCHANGE_WRITE_ARRAY_INVAL_STRING      tr("not supported or invalid array type of write data")

#define OPCDOERR_EXCHANGE_WRITE_MATRIX_INVAL    0x90000803
#define OPCDOERR_EXCHANGE_WRITE_MATRIX_INVAL_STRING     tr("not supported or invalid matrix type of write data")

#define OPCDOERR_EXCHANGE_WRITE_DIMENSION_INVAL 0x90000804
#define OPCDOERR_EXCHANGE_WRITE_DIMENSION_INVAL_STRING  tr("can not determin dimensions of matrix write data")

#define OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG     0x90000805
#define OPCDOERR_EXCHANGE_READ_ATTR_NOT_ARG_STRING      tr("attribute %1 is not supported")

#define OPCDOERR_EXCHANGE_READ_VALUE            0x90000806
#define OPCDOERR_EXCHANGE_READ_VALUE_STRING             tr("invalid read value")

#define OPCDOERR_EXCHANGE_READ_ARRAY            0x90000807
#define OPCDOERR_EXCHANGE_READ_ARRAY_STRING             tr("invalid read array value")

#define OPCDOERR_EXCHANGE_READ_MATRIX           0x90000808
#define OPCDOERR_EXCHANGE_READ_MATRIX_STRING            tr("invalid read matrix value")

#define OPCDOERR_EXCHANGE_MONITORED_ARG_ARG     0x9000080a
#define OPCDOERR_EXCHANGE_MONITORED_ARG_ARG_STRING      tr("node '%1' (%2) is already monitored")

#define OPCDOERR_EXCHANGE_ERROR_MASK            0x90000800
#define OPCDOERR_EXCHANGE_ERROR_MASK_STRING             tr("--- Data Exchange Error (0x%1) ---")


#define OPCDOERR_PARSE_STRING_EMPTY             0xb0000801
#define OPCDOERR_PARSE_STRING_EMPTY_STRING              tr("can not parse empty string")

#define OPCDOERR_PARSE_RANK_INVALID             0xb0000802
#define OPCDOERR_PARSE_RANK_INVALID_STRING              tr("syntax error: can not determin dimensions of array/matrix")

#define OPCDOERR_PARSE_ARRAY_RBRACE             0xb0000803
#define OPCDOERR_PARSE_ARRAY_RBRACE_STRING              tr("syntax error: right brace missing")

#define OPCDOERR_PARSE_ARRAY_RBRACE_ARG         0xb0000804
#define OPCDOERR_PARSE_ARRAY_RBRACE_ARG_STRING          tr("syntax error: right brace missing in dimension position '%1'")

#define OPCDOERR_PARSE_ARRAY_LBRACE             0xb0000806
#define OPCDOERR_PARSE_ARRAY_LBRACE_STRING              tr("syntax error: left brace missing")

#define OPCDOERR_PARSE_ARRAY_LBRACE_ARG         0xb0000807
#define OPCDOERR_PARSE_ARRAY_LBRACE_ARG_STRING          tr("syntax error: left brace missing in dimension position '%1'")

#define OPCDOERR_PARSE_VALUE_PARSE              0xb000080a
#define OPCDOERR_PARSE_VALUE_PARSE_STRING               tr("syntax error: can not interpret string")

#define OPCDOERR_PARSE_VALUE_PARSE_ARG          0xb000080b
#define OPCDOERR_PARSE_VALUE_PARSE_ARG_STRING           tr("syntax error: can not interpret string of dimension position '%1'")

#define OPCDOERR_PARSER_ERROR_MASK              0xb0000800
#define OPCDOERR_PARSER_ERROR_MASK_STRING               tr("--- Parser Error (0x%1) ---")


#define OPCDOERR_COLLOCATOR_RANK_INVALID        0xa0000802
#define OPCDOERR_COLLOCATOR_RANK_INVALID_STRING         tr("value error: can not evaluate dimensions of array/matrix")

#define OPCDOERR_COLLOCATOR_VALUE               0xa0000803
#define OPCDOERR_COLLOCATOR_VALUE_STRING                tr("value error: can not convert value to string")

#define OPCDOERR_COLLOCATOR_VALUE_ARG           0xa0000804
#define OPCDOERR_COLLOCATOR_VALUE_ARG_STRING            tr("value error: can not convert value #%1 to string")

#define OPCDOERR_COLLOCATOR_VALUE_ARG_ARG       0xa0000805
#define OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_STRING        tr("value error: can not convert value #%1 in subarray %2 to string")

#define OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG   0xa0000806
#define OPCDOERR_COLLOCATOR_VALUE_ARG_ARG_ARG_STRING    tr("value error: can not convert value #%1 in subarray %2 of dimension '%3' to string")

#define OPCDOERR_COLLOCATOR_ERROR_MASK          0xa0000800
#define OPCDOERR_COLLOCATOR_ERROR_MASK_STRING           tr("--- Collocation Error (0x%1) ---")


// server exception info parts
#define OPCDOERRINFO_clientConnectionId "clientConnectionId"    // uint
#define OPCDOERRINFO_serviceType "serviceType"                  // OpcdoDatabase::ConnectServiceType
#define OPCDOERRINFO_error "error"                              // uint
#define OPCDOERRINFO_errorString "errorString"                  // QString
#define OPCDOERRINFO_clientSideError "clientSideError"          // bool

#endif // OPCDODLLERRORS_H
