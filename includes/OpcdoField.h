#ifndef OPCDOFIELD_H
#define OPCDOFIELD_H

#include "opcdodll.h"

#include <QVariant>

class OpcdoFieldPrivate;

class OPCDO_EXPORT OpcdoField
{
public:
    /// An enumeration that identifies a NodeClass.
    enum NodeClass
    {
        Invalid       = 0xff,
        Unspecified   = 0x00,
        Object        = 0x01,
        Variable      = 0x02,
        Method        = 0x04,
        ObjectType    = 0x08,
        VariableType  = 0x10,
        ReferenceType = 0x20,
        DataType      = 0x40,
        View          = 0x80,
    };

public:
    explicit OpcdoField(OpcdoField* parentField = nullptr);
    ~OpcdoField();

    OpcdoField* child(int row);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    int row() const;
    OpcdoField* parentField();

private:

    OpcdoFieldPrivate* d;
    friend class OpcdoFieldPrivate;
    friend class ClientServerDataExchange;
};

#endif // OPCDOFIELD_H
