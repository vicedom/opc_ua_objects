#ifndef OPCDOENUMS_H
#define OPCDOENUMS_H

#include "opcdodll.h"

#include <QObject>
#include <QMetaType>

class OPCDO_EXPORT OpcdoEnums
{
    Q_GADGET

    Q_ENUMS(DataType)
    Q_ENUMS(ServerStatus)
    Q_ENUMS(ConnectServiceType)
    Q_ENUMS(CursorType)

public:
    /// OPC most usual data types enumeration
    enum DataType
    {
        Unknown = 0,
        Boolean = 1,
        DateTime = 13,
        Int16 = 4,
        Int32 = 6,
        Int64 = 8,
        Byte = 3,
        SByte = 2,
        UInt16 = 5,
        UInt32 = 7,
        UInt64 = 9,
        Float = 10,
        Double = 11,
        String = 12,        
    };

    static QString dataTypeString(DataType type);
    static QMetaType::Type fromDataType(DataType dType);
    static DataType fromMetaType(QMetaType::Type mtype);


    /// OPC server status enumeration used to indicate the connection status
    enum ServerStatus
    {
        Disconnected, /*!< The connection to the server is deactivated by the user of the client API. */
        Connected, /*!< The connection to the server is established and is working in normal mode. */
        ConnectionWarningWatchdogTimeout,  /*!< The monitoring of the connection to the server indicated a potential connection problem. */
        ConnectionErrorApiReconnect, /*!< The monitoring of the connection to the server detected an error and is trying to reconnect to the server. */
        ServerShutdown, /*!< The server sent a shut-down event and the client API tries a reconnect. */
        NewSessionCreated /*!< The client was not able to reuse the old session and created a new session during reconnect. This requires to redo register nodes for the new session or to read the namespace array. */
    };

    static QString serverStatusString(ServerStatus status);


    /// Service type enumeration used to indicate a connection establishment step
    enum ConnectServiceType
    {
        CertificateValidation, /*!< Certificate validation steps. */
        OpenSecureChannel, /*!< Processing of Service OpenSecureChannel. */
        CreateSession, /*!< Processing of Service CreateSession. */
        ActivateSession /*!< Processing of Service ActivateSession. */
    };

    static QString connectServiceTypeString(ConnectServiceType type);

    /// How to open an OpcdoField container from an OpcdoDatabase
    enum CursorType
    {
        UnknownCursor,
        SnapshotCursor,         // value: not available, full tree structure (time consuming)
        StaticCursor,           // value: readable
        ClientSideCursor,       // value: readable, writeable
        ServerSideCursor,       // value: readable, writeable, dynamic
    };

    static QString cursorTypeString(CursorType type);

};

#endif // OPCDOENUMS_H
