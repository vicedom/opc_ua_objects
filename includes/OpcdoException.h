#ifndef OPCDOEXCEPTION_H
#define OPCDOEXCEPTION_H

#include "opcdodll.h"

#include <QException>
#include <QString>
#include <QHash>


class OPCDO_EXPORT OpcdoException : public QException
{
public:
    OpcdoException(quint32 errorNumber = 0, const QString& errorString = QString());
    ~OpcdoException() override;

    void raise() const override;
    QException* clone() const override;

    quint32 reason() const;
    QString message() const;
    QHash<QString, QString> info() const;

    void setReason(quint32 errorNumber);
    void setErrorString(const QString& errorString);

    QHash<QString, QString>& errorInfo()
    {
        return m_errorInfo;
    }

private:
    quint32 m_errorNumber;
    QString m_errorString;
    QHash<QString, QString> m_errorInfo;
};

#endif // OPCDOEXCEPTION_H
