#ifndef OPCDO_DLL_H
#define OPCDO_DLL_H

#include <QtCore/qglobal.h>
#include <QString>

#if defined(OPCDO_LIBRARY)
#  define OPCDO_EXPORT Q_DECL_EXPORT
#else
#  define OPCDO_EXPORT Q_DECL_IMPORT
#endif

extern QString OPCDO_EXPORT opcdodllErrorString(quint32 number);
extern bool OPCDO_EXPORT opcdodllCanUnloadNow();

#endif // OPCDO_DLL_H
