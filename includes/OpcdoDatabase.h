#ifndef OPCDODATABASE_H
#define OPCDODATABASE_H

#include "OpcdoEnums.h"

#include <QHash>

class OpcdoDatabasePrivate;
class OpcdoRecord;

class OPCDO_EXPORT OpcdoDatabase : public QObject
{
    Q_OBJECT
public:
    explicit OpcdoDatabase(QObject* parent = nullptr);
    ~OpcdoDatabase() override;

    QString companyName() const;
    void setCompanyName(const QString& companyName = QString());

    QString productName() const;
    void setProductName(const QString& productName = QString());

    QString serverUrl() const;
    bool isConnected() const;
    bool isOnline() const;
    bool canMonitor() const;

    quint32 clientConnectionId() const;

    OpcdoEnums::ServerStatus connect(const QString& serverUrl) /*throws*/;
    OpcdoEnums::ServerStatus disconnect() /*throws*/;
    OpcdoRecord* createRecord(const QString& nodeId = QString(), OpcdoEnums::CursorType cursor = OpcdoEnums::ClientSideCursor) /*throws*/;
    QString valueString(const QString& nodeId) /*throws*/;
    QString valueDataTypeString(const QString& nodeId) /*throws*/;
    QVariant value(const QString& nodeId, OpcdoEnums::DataType& dataType) /*throws*/;
    bool writeValue(const QString& nodeId, const QVariant& varValue, OpcdoEnums::DataType dataType = OpcdoEnums::Unknown) /*throws*/;

    bool monitorValue(const QString& nodeId, uint monitoringId) /*throws*/;
    bool unmonitorValue(uint monitoringId) /*throws*/;
    bool isMonitored(const QString& nodeId);
    bool isMonitored(uint monitoringId);

signals:
    void connectStatusChanged(uint clientConnectionId,
                              OpcdoEnums::ServerStatus serverStatus);
    void connectError(quint32 reason,
                      const QString& message,
                      const QHash<QString, QString>& contens);
    void subscriptStatusError(uint clientSubscriptionHandle,
                              quint32 reason,
                              const QString& message);
    void subscriptDataChangeError(uint clientSubscriptionHandle,
                                  uint montoringId,
                                  uint statusVal,
                                  const QString& statusString);
    void subscriptDataChange(uint clientSubscriptionHandle,
                             uint montoringId,
                             const QDateTime& modified,
                             const QVariant& varValue);
    void monitoringStatusChanged();

private:
    void doConnectStatusChanged(uint clientConnectionId, int serverStatus);
    void doConnectError(quint32 reason, const QString& message, const QHash<QString, QString>& errorInfo);
    void doSubscriptStatusError(uint clientSubscriptionHandle, quint32 reason, const QString& message);
    void doSubscriptDataChangeError(uint clientSubscriptionHandle, uint montoringId, uint statusVal, const QString& statusString);
    void doSubscriptDataChange(uint clientSubscriptionHandle, uint montoringId, const QDateTime& modified, const QVariant& varValue);
    void doMonitoringStatusChanged();

private:
    OpcdoDatabasePrivate* d;
    friend class OpcdoDatabasePrivate;
};

#endif // OPCDODATABASE_H
