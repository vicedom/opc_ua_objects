#ifndef OPCDORECORD_H
#define OPCDORECORD_H

#include <QObject>

class OpcdoRecordPrivate;
class OpcdoDatabase;
class OpcdoDatabasePrivate;
class OpcdoField;

class OpcdoRecord : public QObject
{
    Q_OBJECT
public:
    explicit OpcdoRecord(OpcdoDatabase* parent);
    ~OpcdoRecord() override;

    OpcdoField& field();

signals:

public slots:

private:

    OpcdoRecordPrivate* d;
    friend class OpcdoRecordPrivate;
    friend class OpcdoDatabasePrivate;
};

#endif // OPCDORECORD_H
