#ifndef OPCDOVALUESTRINGPARSER_H
#define OPCDOVALUESTRINGPARSER_H

#include "OpcdoEnums.h"

#include <QVariant>

class OPCDO_EXPORT OpcdoValueStringParser
{
public:
    static QVariant parse(const QString& parseString, const OpcdoEnums::DataType type) /*throws*/;

private:
    static QVariant parseScalar(const QString& parseString, const OpcdoEnums::DataType type, const QString& dimdesc = QString());
    static QVariant parseMultiDimension(const QString& parseString, OpcdoEnums::DataType type, int rank);

    static QVariant parseDimension(const QString& parseString, OpcdoEnums::DataType type, int rank, const QString& dimdesc = QString());

    static QString normalize(const QString& input);
};

#endif // OPCDOVALUESTRINGPARSER_H
