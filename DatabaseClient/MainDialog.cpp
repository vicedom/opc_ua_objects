#include "MainDialog.h"

#include "opcdodllerrors.h"
#include "OpcdoException.h"
#include "OpcdoValueStringParser.h"
#include "OpcdoValueStringCollocator.h"

#include "ui_MainDialog.h"

#include <QCloseEvent>

MainDialog::MainDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::MainDialog)
    , m_serverUrlTextSet(false)
    , m_readIdTextSet(false)
    , m_writeIdTextSet(false)
    , m_monitorIdTextSet(false)
    , m_serverStatus(OpcdoEnums::Disconnected)
    , m_database(nullptr)
    , m_monitoredDataType(OpcdoEnums::Unknown)

{
    setWindowFlags(Qt::Window
                 | Qt::CustomizeWindowHint
                 | Qt::WindowTitleHint
                 | Qt::WindowSystemMenuHint
                 | Qt::WindowMinMaxButtonsHint
                 | Qt::WindowCloseButtonHint);

    ui->setupUi(this);
    viewConnectionMessages(ui->viewConnectionStateButton->isChecked());

    fillComboBoxWriteIdType();

#ifdef _DEVELOPMENT_INTERNAL_USE
    ui->selectServerComboBox->insertItem(0, "opc.tcp://localhost:48010");
    ui->selectServerComboBox->setCurrentIndex(0);

    ui->readIdSelectComboBox->insertItem(0, "ns=2;s=Demo.Static.Matrix.Double");
    ui->readIdSelectComboBox->setCurrentIndex(0);

    ui->writeIdSelectComboBox->insertItem(0, "ns=2;s=Demo.Static.Matrix.Double");
    ui->writeIdSelectComboBox->setCurrentIndex(0);

    ui->monitorIdSelectComboBox->insertItem(0, "ns=2;s=Demo.Dynamic.Arrays.Double");
    ui->monitorIdSelectComboBox->setCurrentIndex(0);
#endif

    enableServerStatusSet();
}

MainDialog::~MainDialog()
{
    delete ui;
}

void MainDialog::setConnectionStateText(const QString& text)
{
    if (text.isEmpty()) {
        return;
    }

    QString display = ui->connectionMessagesTextEdit->toPlainText();

    if (!display.isEmpty()) {
        display += "\n\n";
    }

    display += text;
    ui->connectionMessagesTextEdit->setPlainText(display);

    QTextCursor cursor = ui->connectionMessagesTextEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->connectionMessagesTextEdit->setTextCursor(cursor);
}

PROP_DEFINE_STRING_SET(serverUrlText, ServerUrlText)
void MainDialog::enableServerUrlTextSet()
{
    ui->connectButton->setEnabled(serverUrlTextSet());
}

PROP_DEFINE_STRING_SET(readIdText, ReadIdText)
void MainDialog::enableReadIdTextSet()
{
    bool isConnected = m_serverStatus == OpcdoEnums::Connected;

    ui->readIdSetAllButton->setEnabled(isConnected && readIdTextSet());
    ui->readSynchronousButton->setEnabled(isConnected && readIdTextSet());
}

PROP_DEFINE_STRING_SET(writeIdText, WriteIdText)
void MainDialog::enableWriteIdTextSet()
{
    bool isConnected = m_serverStatus == OpcdoEnums::Connected;

    ui->writeIdTypeComboBox->setEnabled(isConnected && writeIdTextSet());
}

PROP_DEFINE_STRING_SET(writeIdEditText, WriteIdEditText)
void MainDialog::enableWriteIdEditTextSet()
{
    bool isConnected = m_serverStatus == OpcdoEnums::Connected;

    ui->writeSynchronousButton->setEnabled(isConnected && writeIdTextSet() && writeIdEditTextSet() && writeIdType() > 0);
}

PROP_DEFINE_STRING_SET(monitorIdText, MonitorIdText)
void MainDialog::enableMonitorIdTextSet()
{
    bool isConnected = m_serverStatus == OpcdoEnums::Connected;

    ui->monitorIdSetAllButton->setEnabled(isConnected && monitorIdTextSet());
    ui->monitorStartButton->setEnabled(isConnected && monitorIdTextSet());
    ui->monitorIdValueEdit->setEnabled(isConnected && monitorIdTextSet());
}

PROP_DEFINE_TYPE(writeIdType, WriteIdType, int)
void MainDialog::enableWriteIdTypeSet()
{
    static bool lockClear = false;

    if (lockClear) {
        lockClear = false;
    }
    else {

        setConnectionStateText("");
        qApp->processEvents();
    }

    QVariant varType = ui->writeIdTypeComboBox->currentData();

    if (!varType.isValid()) {
        return;
    }

    int type = varType.toUInt();

    if (type != 200000) {
        return;
    }

    try {
        type = m_database->valueDataTypeString(writeIdText()).toInt();
    }
    catch (const OpcdoException& ex) {

        showOpcdoError(ex.reason(), ex.message(), ex.info());

        lockClear = true;
        ui->writeIdTypeComboBox->setCurrentIndex(0);
        return;
    }

    if (type < 0) {

        lockClear = true;
        ui->writeIdTypeComboBox->setCurrentIndex(0);
        return;
    }

    varType.setValue<uint>(static_cast<uint>(type));
    int idx = ui->writeIdTypeComboBox->findData(varType);

    if (idx < 0) {

        lockClear = true;
        ui->writeIdTypeComboBox->setCurrentIndex(0);
        return;
    }

    ui->writeIdTypeComboBox->setCurrentIndex(idx);
    enableWriteIdEditTextSet();
}

PROP_DEFINE_TYPE(serverStatus, ServerStatus, OpcdoEnums::ServerStatus)
void MainDialog::enableServerStatusSet()
{
    bool isConnected = m_serverStatus == OpcdoEnums::Connected;

    ui->connectButton->setText(isConnected ? tr("Disconnect") : tr("Connect"));
    ui->actualConnectionStateLabel->setText(isConnected ? tr("CONNECTED") : tr("DISCONNECTED"));

    ui->readIdSelectComboBox->setEnabled(isConnected);
    ui->writeIdSelectComboBox->setEnabled(isConnected);
    ui->monitorIdSelectComboBox->setEnabled(isConnected);
    ui->readIdValueEdit->setEnabled(isConnected);
    ui->writeIdValueEdit->setEnabled(isConnected);

    enableReadIdTextSet();
    enableWriteIdTextSet();
    enableWriteIdEditTextSet();
    enableMonitorIdTextSet();
}

PROP_DEFINE_TYPE(canMonitor, CanMonitor, bool)
void MainDialog::enableCanMonitorSet()
{
    ui->monitorStartButton->setEnabled(canMonitor());
    ui->monitorStopButton->setEnabled(canMonitor() && m_database->isMonitored(4711));

    if (!canMonitor() || !m_database->isMonitored(4711)) {
        ui->monitorIdValueEdit->setPlainText("");
    }
}

void MainDialog::showEvent(QShowEvent* ev)
{
    m_database = new OpcdoDatabase(this);
    connect(m_database, &OpcdoDatabase::connectStatusChanged, this, &MainDialog::serverStatusChanged);
    connect(m_database, &OpcdoDatabase::connectError, this, &MainDialog::serverError);
    connect(m_database, &OpcdoDatabase::subscriptStatusError, this, &MainDialog::subscriptError);
    connect(m_database, &OpcdoDatabase::subscriptDataChangeError, this, &MainDialog::subscriptDataError);
    connect(m_database, &OpcdoDatabase::subscriptDataChange, this, &MainDialog::subscriptData);
    connect(m_database, &OpcdoDatabase::monitoringStatusChanged, this, &MainDialog::monitoringStatusChange);

    m_database->setCompanyName(COMPANY_NAME);
    m_database->setProductName(PRODUCT_NAME);

    QDialog::showEvent(ev);
}

void MainDialog::reject()
{
    delete m_database;
    m_database = nullptr;

    QDialog::reject();
}

void MainDialog::closeEvent(QCloseEvent* /*ev*/)
{
    reject();
}

void MainDialog::toggleConnect()
{
    if (serverStatus() == OpcdoEnums::Connected) {
        disconnectFromServer();
    }
    else {
        connectToServer();
    }
}

void MainDialog::viewConnectionMessages(bool set)
{
    ui->viewConnectionStateButton->setText(set ? "<-" : "->");

    if (set) {

        ui->horizontalBoxLayout->addLayout(ui->verticalViewLayout);
        ui->horizontalBoxLayout->setStretch(0, 5);
        ui->horizontalBoxLayout->setStretch(1, 3);
        ui->connectionMessagesTextEdit->show();
    }
    else {
        ui->connectionMessagesTextEdit->hide();
        ui->horizontalBoxLayout->removeItem(ui->verticalViewLayout);
        ui->horizontalBoxLayout->setStretch(0, 0);
    }
}

void MainDialog::readSynchronous()
{
    ui->readIdValueEdit->setPlainText("");
    setConnectionStateText("");
    qApp->processEvents();

    QVariant fromDb;
    QString display;
    OpcdoEnums::DataType dataType = OpcdoEnums::Unknown;

    try {

        fromDb = m_database->value(readIdText(), dataType);
        display = OpcdoValueStringCollocator::collocate(fromDb, dataType);
        ui->readIdValueEdit->setPlainText(display);
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }
}

void MainDialog::readAsynchronous() {  }

void MainDialog::writeSynchronous()
{
    setConnectionStateText("");
    qApp->processEvents();

    QVariant toWrite;
    bool written = false;
    OpcdoEnums::DataType dtype = static_cast<OpcdoEnums::DataType>(ui->writeIdTypeComboBox->currentData().toInt());

    try {
        toWrite = OpcdoValueStringParser::parse(m_writeIdEditText, dtype);
        written = m_database->writeValue(m_writeIdText, toWrite, dtype);
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }
}

void MainDialog::writeAsynchronous() {  }

void MainDialog::monitor()
{
    bool subscribed = false;

    if (m_database->isMonitored(4711)) {

        stopMonitor();
        qApp->processEvents();
    }

    try {
        subscribed = m_database->monitorValue(monitorIdText(), 4711);
        m_monitoredDataType = static_cast<OpcdoEnums::DataType>(m_database->valueDataTypeString(monitorIdText()).toInt());
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }

    enableCanMonitorSet();
}

void MainDialog::subscriptData(uint clientSubscriptionHandle,
                               uint montoringId,
                               const QDateTime& modified,
                               const QVariant& varValue)
{
    Q_UNUSED(clientSubscriptionHandle);
    Q_UNUSED(montoringId);
    Q_UNUSED(modified);

    ui->monitorIdValueEdit->setPlainText("");
    qApp->processEvents();

    if (!m_database) {
        return;
    }

    QString display;

    try {
        display = OpcdoValueStringCollocator::collocate(varValue, m_monitoredDataType);
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }

    ui->monitorIdValueEdit->setPlainText(display);
}

void MainDialog::stopMonitor()
{
    bool unsubscribed = false;

    try {
        unsubscribed = m_database->unmonitorValue(4711);
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }

    enableCanMonitorSet();

    if (unsubscribed) {
        m_monitoredDataType = OpcdoEnums::Unknown;
    }
}

void MainDialog::readIdSetAll() {  }

void MainDialog::writeIdTypeIndexChanged(int index)
{
    setWriteIdType(index);
}

void MainDialog::monitorIdSetAll() {  }

void MainDialog::showOpcdoError(quint32 reason,
                                  const QString &message,
                                  const QHash<QString, QString> &contens)
{
    QStringList list;
    QString format("\t%1: %2");
    QString title;
    QString display;

    if ((reason & OPCDOERR_PARSER_ERROR_MASK) == OPCDOERR_PARSER_ERROR_MASK) {
        title = opcdodllErrorString(OPCDOERR_PARSER_ERROR_MASK);
    }
    else if ((reason & OPCDOERR_EXCHANGE_ERROR_MASK) == OPCDOERR_EXCHANGE_ERROR_MASK) {
        title = opcdodllErrorString(OPCDOERR_EXCHANGE_ERROR_MASK);
    }
    else {
        title = opcdodllErrorString(OPCDOERR_SERVER_ERROR_MASK);
    }

    list << title.arg(QString::number(reason, 16)) << "    " + message << "";

    QHash<QString, QString>::const_iterator it = contens.constBegin();
    while (it != contens.constEnd()) {

        display = format.arg(it.key()).arg(it.value());
        list << display;

        ++it;
    }

    setConnectionStateText(list.join('\n'));
}

void MainDialog::connectToServer()
{
    if (m_database->isConnected()) {
        disconnectFromServer();
    }

    Q_ASSERT(serverStatus() == OpcdoEnums::Disconnected);

    setConnectionStateText("");
    qApp->processEvents();

    try {
        m_database->connect(serverUrlText());
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }
}

void MainDialog::disconnectFromServer()
{
    if (!m_database->isConnected()) {

        Q_ASSERT(serverStatus() == OpcdoEnums::Disconnected);
        return;
    }

    try {
        m_database->disconnect();
    }
    catch (const OpcdoException& ex) {
        showOpcdoError(ex.reason(), ex.message(), ex.info());
    }
}

void MainDialog::serverStatusChanged(uint /*clientConnectionId*/,
                                     OpcdoEnums::ServerStatus serverStatus)
{
    setServerStatus(serverStatus);
}

void MainDialog::serverError(quint32 reason,
                             const QString &message,
                             const QHash<QString, QString> &contens)
{
    showOpcdoError(reason, message, contens);
}

void MainDialog::subscriptError(uint /*clientSubscriptionHandle*/,
                                quint32 reason,
                                const QString& message)
{
    showOpcdoError(reason, message);
}

void MainDialog::subscriptDataError(uint /*clientSubscriptionHandle*/,
                                    uint /*montoringId*/,
                                    uint statusVal,
                                    const QString& statusString)
{
    showOpcdoError(statusVal, statusString);
}

void MainDialog::monitoringStatusChange()
{
    setCanMonitor(m_database->canMonitor());
}


void MainDialog::fillComboBoxWriteIdType()
{
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Unknown), OpcdoEnums::Unknown);
    ui->writeIdTypeComboBox->insertSeparator(ui->writeIdTypeComboBox->count());
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Boolean), OpcdoEnums::Boolean);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::DateTime), OpcdoEnums::DateTime);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Int16), OpcdoEnums::Int16);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Int32), OpcdoEnums::Int32);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Int64), OpcdoEnums::Int64);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Byte), OpcdoEnums::Byte);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::SByte), OpcdoEnums::SByte);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::UInt16), OpcdoEnums::UInt16);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::UInt32), OpcdoEnums::UInt32);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::UInt64), OpcdoEnums::UInt64);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Float), OpcdoEnums::Float);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::Double), OpcdoEnums::Double);
    ui->writeIdTypeComboBox->addItem(OpcdoEnums::dataTypeString(OpcdoEnums::String), OpcdoEnums::String);
    ui->writeIdTypeComboBox->insertSeparator(ui->writeIdTypeComboBox->count());
    ui->writeIdTypeComboBox->addItem(tr("Auto"), 200000);

    ui->writeIdTypeComboBox->setCurrentIndex(0);
}

