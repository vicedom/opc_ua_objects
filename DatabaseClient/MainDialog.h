#ifndef MAINDIALOG_H
#define MAINDIALOG_H

#include "OpcdoDatabase.h"

#include <QDialog>
#include <QEvent>
#include <QLayout>

QT_BEGIN_NAMESPACE
namespace Ui { class MainDialog; }
QT_END_NAMESPACE

#define COMPANY_NAME "Techdrivers"
#define PRODUCT_NAME "BasicClient"

#define CLASS_NAME MainDialog
#include "PropMakros.inc"

class MainDialog : public QDialog
{
    Q_OBJECT

    Q_PROPERTY(QString serverUrlText READ serverUrlText WRITE setServerUrlText)
    Q_PROPERTY(bool serverUrlTextSet READ serverUrlTextSet WRITE setServerUrlTextSet)

    Q_PROPERTY(QString readIdText READ readIdText WRITE setReadIdText)
    Q_PROPERTY(bool readIdTextSet READ readIdTextSet WRITE setReadIdTextSet)

    Q_PROPERTY(QString writeIdText READ writeIdText WRITE setWriteIdText)
    Q_PROPERTY(bool writeIdTextSet READ writeIdTextSet WRITE setWriteIdTextSet)

    Q_PROPERTY(QString writeIdEditText READ writeIdEditText WRITE setWriteIdEditText)
    Q_PROPERTY(bool writeIdEditTextSet READ writeIdEditTextSet WRITE setWriteIdEditTextSet)

    Q_PROPERTY(QString monitorIdText READ monitorIdText WRITE setMonitorIdText)
    Q_PROPERTY(bool monitorIdTextSet READ monitorIdTextSet WRITE setMonitorIdTextSet)

    Q_PROPERTY(OpcdoEnums::ServerStatus serverStatus READ serverStatus WRITE setServerStatus)
    Q_PROPERTY(int writeIdType READ writeIdType WRITE setWriteIdType)
    Q_PROPERTY(bool canMonitor READ canMonitor WRITE setCanMonitor)

public:
    MainDialog(QWidget *parent = nullptr);
    ~MainDialog();

    void setConnectionStateText(const QString& text);

    PROP_DECLARE_STRING_SET(serverUrlText, ServerUrlText)
    PROP_DECLARE_STRING_SET(readIdText, ReadIdText)
    PROP_DECLARE_STRING_SET(writeIdText, WriteIdText)
    PROP_DECLARE_STRING_SET(writeIdEditText, WriteIdEditText)
    PROP_DECLARE_STRING_SET(monitorIdText, MonitorIdText)
    PROP_DECLARE_TYPE(serverStatus, ServerStatus, OpcdoEnums::ServerStatus)
    PROP_DECLARE_TYPE(writeIdType, WriteIdType, int)
    PROP_DECLARE_TYPE(canMonitor, CanMonitor, bool)

protected:
    void showEvent(QShowEvent* ev);
    void reject() override;
    void closeEvent(QCloseEvent* ev) override;

private:
    void fillComboBoxWriteIdType();
    void connectToServer();
    void disconnectFromServer();
    void showOpcdoError(quint32 reason,
                          const QString& message,
                          const QHash<QString, QString>& contens = QHash<QString, QString>());

private slots:
    void toggleConnect();
    void viewConnectionMessages(bool set);
    void readSynchronous();
    void readAsynchronous();
    void writeSynchronous();
    void writeAsynchronous();
    void monitor();
    void stopMonitor();
    void serverUrlTextChanged(const QString& text);
    void readIdTextChanged(const QString& text);
    void writeIdTextChanged(const QString& text);
    void writeIdEditTextChanged(const QString& text);
    void monitorIdTextChanged(const QString& text);
    void readIdSetAll();
    void writeIdTypeIndexChanged(int index);
    void monitorIdSetAll();

    void serverStatusChanged(uint clientConnectionId,
                             OpcdoEnums::ServerStatus serverStatus);
    void subscriptData(uint clientSubscriptionHandle,
                       uint montoringId,
                       const QDateTime& modified,
                       const QVariant& varValue);

    void serverError(quint32 reason,
                     const QString& message,
                     const QHash<QString, QString>& contens);
    void subscriptError(uint clientSubscriptionHandle,
                        quint32 reason,
                        const QString& message);
    void subscriptDataError(uint clientSubscriptionHandle,
                            uint montoringId,
                            uint statusVal,
                            const QString& statusString);
    void monitoringStatusChange();


private:
    Ui::MainDialog *ui;

    OpcdoDatabase* m_database;
    OpcdoEnums::DataType m_monitoredDataType;

};
#endif // MAINDIALOG_H
