#define PROP_DECLARE_TYPE(name, Name, type) \
    private: \
    type name() const \
    { \
        return m_##name; \
    } \
    void set##Name(type name); \
    void enable##Name##Set(); \
    type m_##name;

#define PROP_DEFINE_TYPE(name, Name, type) \
    void CLASS_NAME::set##Name(type name) \
    { \
        if (name == m_##name) { \
            return; \
        } \
        m_##name = name; \
        enable##Name##Set(); \
    }

//////////////////////////////////////////////////////////////////////////////

#define PROP_DECLARE_STRING_SET(name, Name) \
    private: \
    QString name() const \
    { \
        return m_##name; \
    } \
    bool name##Set() const \
    { \
        return m_##name##Set; \
    } \
    void set##Name(const QString& name); \
    void set##Name##Set(bool name##Set); \
    void enable##Name##Set(); \
    QString m_##name; \
    bool m_##name##Set;

#define PROP_DEFINE_STRING_SET(name, Name) \
    void CLASS_NAME::name##Changed(const QString& name) \
    { \
        set##Name(name); \
    } \
    void CLASS_NAME::set##Name(const QString& name) \
    { \
        QString val = name.trimmed(); \
        if (m_##name == val) { \
            return; \
        } \
        m_##name = val; \
        set##Name##Set(!m_##name.isEmpty()); \
    } \
    void CLASS_NAME::set##Name##Set(bool name##Set) \
    { \
        if (m_##name##Set == name##Set) { \
            return; \
        } \
        m_##name##Set = name##Set; \
        enable##Name##Set(); \
    }

