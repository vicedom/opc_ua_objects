QT       += core gui widgets

CONFIG += c++11

PROJECT_INCLUDE_DIR = "../includes"

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $${PROJECT_INCLUDE_DIR}

INCLUDEPATH += general

MOC_DIR     = generated
OBJECTS_DIR = generated
UI_DIR      = generated
RCC_DIR     = generated

SOURCES += \
    MainDialog.cpp \
    main.cpp

HEADERS += \
    general/PropMakros.inc \
    MainDialog.h

FORMS += \
    MainDialog.ui

RESOURCES += \
    resource.qrc

LIBS += -lws2_32 -lmpr -lkernel32 -luser32 -lgdi32 -lwinspool -lshell32 -lole32 -loleaut32 -luuid -lcomdlg32 -ladvapi32

INCLUDEPATH += $$PWD/../lib
DEPENDPATH += $$PWD/../lib

CONFIG(release, debug|release): LIBS += -L$$PWD/../lib/ -lopcdo
CONFIG(debug, debug|release): LIBS += -L$$PWD/../lib/ -lopcdod

CONFIG(release, debug|release): QMAKE_POST_LINK += ..\\scripts\\copyReleaseDll.bat
CONFIG(debug, debug|release): QMAKE_POST_LINK += ..\\scripts\\copyDebugDll.bat
